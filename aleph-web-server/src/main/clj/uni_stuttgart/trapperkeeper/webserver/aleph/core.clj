(ns uni-stuttgart.trapperkeeper.webserver.aleph.core
  (:require [taoensso.timbre :as ti]
            [aleph.http :as h]
            [schema.core :as s]))

(ti/refer-timbre)

(def ^:private shared-state (atom {}))

(defn add-shared-state
  [m]
  (:pre [m (map? m)])
  (assoc m :shared-state @shared-state))

(defn add-ring-handler!
  [handler]
  (swap! shared-state assoc-in [:handler] handler))

(defn start-server!
  [m]
  {:pre [(:shared-state m) (:handler (:shared-state m)) (:config m)]}
  (let [stop-fn (h/start-server (:handler (:shared-state m)) (:config m))]
    (assoc m :server-stop-fn stop-fn)))


(defn stop-server!
  [m]
  {:pre [(:server-stop-fn m)]}
  (.close (:server-stop-fn m)))
