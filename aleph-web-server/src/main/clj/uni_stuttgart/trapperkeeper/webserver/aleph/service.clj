(ns uni-stuttgart.trapperkeeper.webserver.aleph.service
  (:require [taoensso.timbre :as ti]
            [uni-stuttgart.trapperkeeper.webserver.aleph.core :as c]
            [puppetlabs.trapperkeeper.core :refer [defservice]]))

(ti/refer-timbre)

(defprotocol WebserverService
  (add-ring-handler [this handler] [this handler path options]))

(def default-config {:port 8080})

(defservice aleph-webserverservice
  "Provides a limited version of the WebserverService based on aleph"
  WebserverService
  [[:ConfigService get-in-config]]
  (init [this context]
        (debug "Initing aleph service!")
        (->> (get-in-config [:aleph])
             (into default-config)
             (assoc context :config)))
  (start [this context]
         (debug "Starting aleph service!")
         (-> context
             c/add-shared-state
             c/start-server!))
  (stop [this context]
        (debug "Stoping aleph service!")
        (c/stop-server! context))
  (add-ring-handler [this handler] (-> handler c/add-ring-handler!)))
