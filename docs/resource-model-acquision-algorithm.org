* Resource acquisition steps
* acquire-resources
1. resource-graph = init(resource-model)
2. resource-graph = init-acquirables(resource-graph)
3. resource-topology = make-topology-sort(resource-graph)
4. reversed-resource-topology = reverse-resource-topology(resource-topology)
5. for each resource in reversed-resource-topology (thread-safe)
   1. if all dependencies-are-satisfied(resource)
      1. precondtioned-relationship-resource-vector = collect-precondtioned-relationships(resource, resource-graph)
      2. postconditioned-relationship-resource-vector = collect-precondtioned-relationships(resource, resource-graph)
      3. initialize-using-domain-manager(resource, precondtioned-relationship-resource-vector, postconditioned-relationship-resource-vector)
      4. update-resource-state(resource,acquiring)
   2. else
      1. continue
* initialize-using-domain-manager
1. resource-instance = initialize-resource-graph(resource, precondtioned-relationship-resource-vector)
2. update-resource-graph(resource-instance)
3. initialize-post-relationships(resource-instance,postconditioned-relationship-resource-vector)
* update-resource-graph
1. update-resource-state(resource-instance)
2. acquire-resources
