(ns de.uni-stuttgart.iaas.ipsm.execution-environment-integrator.winery
  (:require [taoensso.timbre :as timbre]
            [environ.core :refer [env]]
            [clojure.java.io :as io]
            [clojure.string :as s]
            [clj-yaml.core :as yaml]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protos]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.transformations :as transformations]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
            [clj-http.client :as http]
            [clojure.data.xml :as x]
            [de.uni-stuttgart.iaas.ipsm.utils.xml :as xml-utils]))

(timbre/refer-timbre)

(defn- add-vinothek-uri
  [m]
  {:pre [(constants/property :vinothek-uri)]
   :post [(:vinothek-uri %)]}
  (assoc m :vinothek-uri
         (if (.endsWith (constants/property :vinothek-uri) "/")
           (constants/property :vinothek-uri)
           (str (constants/property :vinothek-uri) "/"))))

(defn- add-vinothek-deployment-uri
  [m]
  {:pre [(constants/property :vinothek-deployment-path)
         (:vinothek-uri m)]
   :post [(:vinothek-deployment-uri %)]}
  (assoc m :vinothek-deployment-uri
         (if (.endsWith (constants/property :vinothek-deployment-path) "/")
           (str (:vinothek-uri m) (constants/property :vinothek-deployment-path))
           (str (:vinothek-uri m) (constants/property :vinothek-deployment-path) "/"))))


(defn- add-vinothek-status-uri
  [m]
  {:pre [(constants/property :vinothek-status-path)
         (:vinothek-uri m)]
   :post [(:vinothek-status-uri %)]}
  (assoc m ::vinothek-status-uri
         (if (.endsWith (constants/property :vinothek-status-path) "/")
           (str (:vinothek-uri m) (constants/property :vinothek-status-path))
           (str (:vinothek-uri m) (constants/property :vinothek-status-path) "/"))))


(defn- add-opentosca-container-uri
  [m]
  {:pre [(constants/property :opentosca-container-uri)]
   :post [(:opentosca-container-uri %)]}
  (assoc m :opentosca-container-uri
         (if (.endsWith (constants/property :opentosca-container-uri) "/")
           (constants/property :opentosca-container-uri)
           (str (constants/property :opentosca-container-uri) "/"))))


(defn- add-opentosca-csar-upload-uri
  [m]
  {:pre [(:opentosca-container-uri m)
         (constants/property :opentosca-upload-path)]
   :post [(:opentosca-csar-upload-uri %)]}
  (assoc m :opentosca-csar-upload-uri
         (str (:opentosca-container-uri m) (constants/property :opentosca-upload-path))))

(defn- add-opentosca-csars-uri
  [m]
  {:pre [(:opentosca-container-uri m)
         (constants/property :opentosca-csars-path)]
   :post [(:opentosca-csars-path %)]}
  (assoc m :opentosca-csars-path
         (str (:opentosca-container-uri m) (constants/property :opentosca-csars-path))))

(defn- add-application-id
  [m]
  {:pre [(:opentosca-container-uri m)
         (:application-name m)]}
  (assoc :application-id (str (:opentosca-container-uri m) "CSARs/" (:application-name m) "/Content/SELFSERVICE-Metadata/")))


(defn- add-defualt-option
  [m]
  (assoc :option-id 1))

(defn- add-opentosca-host
  [m]
  {:pre [(:opentosca-container-uri m)]}
  (assoc m :opentosca-container-host (.getHost (java.net.URI (:opentosca-container-uri m)))))



(defn- add-application-file-name
  [m]
  {:pre [(:id (:target-resource m))]}
  (assoc :application-file-name (str (:id (:target-resource m)) ".csar")))

(defn- add-application-id
  [m]
  {:pre [(:id (:target-resource m))]}
  (assoc :application-file-name (javax.xml.namespace.QName/valueOf (:type (:target-resource m)))))

(defn- remove-deployable!
  [m]
  (assoc m :response
         (http/delete (str (:opentosca-csar-upload-uri m) (:application-file-name m)))))

(defn- runnable-deployed?
  [m]
  {:pre [(:opentosca-csars-path m)]}
  (->> (http/get (:opentosca-csar-control-uri m)
                 {:accept :xml})
       :body
       x/parse-str
       :content
       (filterv #(= (:xlink/title (:attrs %)) (:application-file-name m)))
       first
       (assoc m :runnable-deployed?)))

(defn- runnable-initialized?
  [m]
  {:pre [(:opentosca-csars-path m)]}
  (->> (http/get (:opentosca-service-instance-uri m)
                 {:accept :xml})
       :body
       x/parse-str
       :content
       (filterv #(or (= (:csar-id (:attrs %)) (:application-id m))
                     (= (:service-template-id (:attrs %)) (:application-id m))))
       first
       (assoc m :runnable-initialized?)))


(defn- complete-operation-with-error!
  [m]
  {:pre [(:current-obj m)
         (:callback m)]}
  (.completeProcessExecutionwithError
   (:current-obj m)
   (or (:err m) "Resource could not be engaged!!")
   (:callback m)))

(defn- create-and-pass-resource-instance!
  [m]
  {:pre [(:callback m)]}
  (debug "Creating an passing callback" m)
  (.completeProcessExecutionSuccessfully
   (:current-obj m)
   (java.net.URI.
    (or (:resource-uri m)
        "http://www.example.org/missing-uri/error/"))
   (:callback m)))


(defn- add-initialization-state
  [m]
  {:pre [(:opentosca-csar-control-uri m)
         (:application-id m)
         (:deployment-state-path m)]}
  (let [resp (http/get (str (:opentosca-csar-control-uri m)
                            (:application-id m) "/"
                            (:deployment-state-path m))
                       {:content-type "application/text"})]
    (cond-> m
      (or (= (:status resp) 200) (= (:status resp) 201)) (assoc :deployment-state (:body resp))
      (not (or (= (:status resp) 200) (= (:status resp) 201))) (assoc :deployment-state :erroneous))))



(defn- initialize-csar!
  [m]
  {:pre [(:opentosca-csar-control-uri m)]}
  (http/post (str (:opentosca-csar-control-uri m) (:application-file-name m))
             {:content-type "text/plain"
              :body (str "INVOKE_PLAN_DEPL&" (:application-id m))}))

(defn- handle-deployment-success!
  [m]
  (initialize-csar! m))

(defn- handle-initialization-failure!
  [m]
  (complete-operation-with-error! m))

(defn- handle-initialization-result!
  "Poll deployment backend and initialize if successful otherwise
  report back an error"
  [m]
  {:pre [(:timeout m)
         (:interval m)]}
  (let [timeout (+ (System/currentTimeMillis) (* (:timeout m) 1000))]
    (loop []
      (if-let [result (runnable-initialized? m)]
        (-> m
            (assoc :runnable-initialized? result)
            create-and-pass-resource-instance!) ;; initialize csar after deployment
        (do
          (Thread/sleep (* (:interval m) 1000))
          (if (< (System/currentTimeMillis) timeout)
            (recur)
            (handle-initialization-failure! m)))))))


(defn- handle-deployment-success!
  [m]
  (initialize-csar! m))

(defn- handle-deployment-failure!
  [m]
  (complete-operation-with-error! m))


;; inspired from http://mikerowecode.com/2013/02/clojure-polling-function.html
(defn- handle-deployment-result!
  "Poll deployment backend and initialize if successful otherwise
  report back an error"
  [m]
  {:pre [(:timeout m)
         (:interval m)]}
  (let [timeout (+ (System/currentTimeMillis) (* (:timeout m) 1000))]
    (loop []
      (if-let [result (runnable-deployed? m)]
        (-> m
            (assoc :runnable-deployed? result)
            handle-deployment-success!) ;; initialize csar after deployment
        (do
          (Thread/sleep (* (:interval m) 1000))
          (if (< (System/currentTimeMillis) timeout)
            (recur)
            (handle-deployment-failure! m)))))))


(defn- request-sucessful?
  "Checks if the response is 2xx"
  [m]
  {:pre [(:response m)]}
  (assoc m :request-sucessful? (and (:status (:response m)) (>= (:status (:response m)) 200) (< (:status (:response m)) 300))))

(defn- request-failed?
  "Checks if the response is 2xx"
  [m]
  {:pre [(:response m)]}
  (assoc m :request-failed? (and (:status (:response m)) (>= (:status (:response m)) 300))))


(defn- post-process-deploy-resp
  [m]
  {:pre [(:response m)]}
  (cond-> m
    ;(request-sucessful? (:response m)) poll-deployment-status!
    (request-failed? (:response m)) (-> m
                            )))

(defn- deploy-csar!
  [m]
  {:pre [(:opentosca-csar-upload-uri m)
         (:csar-input-stream m)
         (:application-file-name m)]}
  (assoc m :response
         (http/post (:opentosca-csar-upload-uri m)
                    {:multipart [{:name (:application-file-name m)
                                  :part-name "file"
                                  :content (:csar-input-stream m)}]})))







(defn- prepare-for-execution
  [m]
  (-> m
      add-opentosca-container-uri
      add-opentosca-csar-upload-uri
      add-opentosca-csars-uri
      add-opentosca-host
      add-application-file-name
      add-application-id))


(defn- init-deployable!
  [m]
  (debug "Resource will be initialized")
  (doto (prepare-for-execution m)
    deploy-csar!
    handle-deployment-result!))


(defn- release-deployable!
  [m]
  (debug "Resource will be released")
  )


(def acquire-resource-operation
  (proxy [uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.AcquireResourceOperation]
    []
    (executeOperation [target-resource-runnable-container parameters callback]
      (debug "Executing acquire operation")
      (init-deployable {:target-resource (transformations/get-map-of-jaxb-type-obj
                                          (.getTargetModel target-resource-runnable-container))
                        :target-resource-runnable (slurp (.getDeployable target-resource-runnable-container))
                        :callback callback
                        :current-obj this}))))

(def acquire-relationship-operation
  (proxy [uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.AcquireRelationshipOperation]
      []
    (executeOperation [target-resource-runnable-container parameters callback]
      (init-deployable target-resource-runnable-container parameters callback))))

(def release-operation
  (proxy [uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.ReleaseResourceOperation]
      []
    (executeOperation [target-resource-runnable-container parameters callback]
      (debug "Executing release operation")
      (release-deployable {:target-resource (transformations/get-map-of-jaxb-type-obj
                                              (.getTargetModel target-resource-runnable-container))
                            :target-resource-runnable (slurp (.getDeployable target-resource-runnable-container))
                            :callback callback
                            :current-obj this}))))

(def store-operation
  (proxy [uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.StoreResourceOperation]
      []
    (executeOperation [target-resource-runnable-container parameters callback]
      (init-deployable target-resource-runnable-container parameters callback))))


(deftype DockerExecutionEnvironmentMetadata []
  uni_stuttgart.ipsm.protocols.integration.IntegratorMetadata
  (getName [_] (constants/property :name))
  (getTargetNamespace [_] (constants/property :domain-uri))
  (getUri [_] (java.net.URI. (constants/property :uri)))
  (getPropertyDefinitions [_] "not availalbe"))


;; (defn runner
;;   [__prefix__]
;;   (integrator/provider-factory-registerExecutionEnvironmentIntegrator
;;    [acquire-resource-operation
;;     acquire-relationship-operation
;;     release-operation
;;     store-operation]
;;    (DockerExecutionEnvironmentMetadata.)))



; (def repl-test (runner nil))
