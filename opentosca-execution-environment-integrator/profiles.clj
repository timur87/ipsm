{:dev {:env {:name "WienryExecutionEnvironmentIntegrator"
             :opentosca-uri "https://192.168.59.103:2376"
             :vinothek-uri "https://192.168.59.103:2376"
             :process-counter-file-name "process-count"
             :deployable-artifact-type-name "ipsm-deployable"
             :life-cycle-interface-postfix "ipsm/lifecycle-operations/v0/"
             :docker-compose-bin-uri "https://github.com/docker/compose/releases/download/VERSION_NUM/docker-compose"
             :docker-cert-path "/Users/sungurtn/.boot2docker/certs/boot2docker-vm/"
             :domain-uri "http://www.iaas.uni-stuttgart.de/ipsm/domains/opentosca/csar-based"
             :uri "http://localhost:8213/"}
       :dependencies [[expectations "2.0.9"]]
       :plugins [[lein-expectations "0.0.7"]]}}
