(defproject uni-stuttgart.ipsm/opentosca-domain-manager "0.0.1-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.9.0-alpha13"]
                 [midje "1.8.2"]
                 [com.taoensso/timbre "4.7.4"]
                 [org.clojure/data.xml "0.0.8"]
                 [cheshire "5.5.0"]
                 [de.uni-stuttgart.iaas.ipsm/commons "0.0.2-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/protocols "0.5.3-SNAPSHOT"]
                 [clj-http "2.0.1"]
                 ;; Winery dependencies
                 [org.eclipse.winery/org.eclipse.winery.repository.client "0.1.37-SNAPSHOT"]
                 [org.eclipse.winery/org.eclipse.winery.common "0.1.37-SNAPSHOT"]
                 [org.eclipse.winery/org.eclipse.winery.model.tosca "0.1.21-SNAPSHOT"]
                 ;;; winery deps for production code
                 [net.sf.saxon/saxon-dom "8.7"]
                 [com.sun.jersey/jersey-core "1.17"]
                 [com.sun.jersey/jersey-client "1.17"]
                 [org.slf4j/jcl-over-slf4j "1.7.6"]
                 [com.fasterxml.jackson.jaxrs/jackson-jaxrs-json-provider "2.2.2"]
                 [com.fasterxml.jackson.core/jackson-databind "2.2.2"]]
  :plugins [[lein-environ "1.0.0"]]
  :source-paths ["src/main/clj"]
  :resource-paths ["src/main/resources"])
