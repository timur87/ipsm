{:dev {:env {:name "WineryDomainManager"
             :repository-url "http://localhost:8080/winery/"
             :target-ee "http://www.iaas.uni-stuttgart.de/ipsm/ee/opentosca/csar-based"
             :execution-dir "runnables/"
             :deployable-artifact-type-name "ipsm-deployable"
             :life-cycle-interface-postfix "ipsm/lifecycle-operations/v0/"
             :domain-uri "http://www.iaas.uni-stuttgart.de/ipsm/domains/winery/metadata-based"
             :uri "http://localhost:8121"}}}
