(ns de.uni-stuttgart.iaas.ipsm.integrator-client.commons
  (:require [taoensso.timbre :as timbre]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants])
  (:import [java.net URI]))

(timbre/refer-timbre)


(defn add-host
  [m]
  {:pre [(:uri m)]}
  (assoc m :host (.getHost (:uri m))))

(defn add-port
  [m]
  {:pre [(:uri m)]}
  (assoc m :port (.getPort (:uri m))))


(defn add-uri
  [m]
  {:pre [(:metadata m)]}
  (assoc m :uri (or (.getUri (:metadata m)) (URI. "http://localhost:8111"))))

(defn add-uri-path
  [m path k]
  {:pre [(:uri m)]}
  (assoc m k (str (.toString (:uri m)) path)))


(defn add-life-cycle-interface-name
  [m]
  {:pre [(:metadata m)]}
  (if (.endsWith (.getTargetNamespace (:metadata m)) "/")
    (assoc m :interface-name (str (.getTargetNamespace (:metadata m)) (constants/property :life-cycle-interface-postfix)))
    (assoc m :interface-name (str (.getTargetNamespace (:metadata m)) "/" (constants/property :life-cycle-interface-postfix)))))


(defn add-life-cycle-interface-with-custom-operations
  [m]
  {:pre [(:interface-name m) (:interface-operations m)]}
  (assoc m :life-cycle-interface
         (domain-language/interface {:name (:interface-name m)
                                     :operation (:interface-operations m)})))

(defn add-life-cycle-interface
  [m]
  {:pre [(:interface-name m)]}
  (assoc m :life-cycle-interface
         (domain-language/interface {:name (:interface-name m)
                                     :operation [(domain-language/operation {:name "engage"})
                                                 (domain-language/operation {:name "release"})
                                                 (domain-language/operation {:name "store"})]})))




(defn add-artifact-type-uri
  [m]
  (assoc m :artifact-type-uri "http://www.uni-stuttgart.de/ipsm/artifact-types/"))

(defn- create-artifact-type
  [m]
  {:pre [(:artifact-type-name m)
         (:artifact-type-uri m)]}
  (domain-language/artifact-type {:name (:artifact-type-name m)
                                  :abstract "no"
                                  :final "yes"
                                  :target-namespace (:artifact-type-uri m)}))

(defn add-artifact-type
  [m]
  (assoc m :artifact-type (create-artifact-type m)))
