(defproject de.uni-stuttgart.iaas.ipsm/integrator-client "0.0.3-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.9.0-alpha14"]
                 [com.taoensso/timbre "4.1.4"]
                 [com.novemberain/langohr "3.5.0"]
                 [de.uni-stuttgart.iaas.ipsm/commons "0.0.2-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/protocols "0.5.3-SNAPSHOT"]
                 [com.google.guava/guava "19.0"]
                 [environ "1.0.2"]]
  :plugins [[lein-environ "1.0.2"]]
  :source-paths ["src/main/clj"]
  :aot [de.uni-stuttgart.iaas.ipsm.integrator-client.core]
  :resource-paths ["src/main/resources"])
