(ns de.uni-stuttgart.iaas.ipsm.execution-environment-integrator.docker-compose
  (:require [taoensso.timbre :as timbre]
            [environ.core :refer [env]]
            [clojure.java.io :as io]
            [clojure.string :as s]
            [clj-yaml.core :as yaml]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protos]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.transformations :as transformations]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions]
            [clojure.java.shell :as shell]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
            [de.uni-stuttgart.iaas.ipsm.integrator-client.core :as integrator]
            [clj-http.client :as http]
            [de.uni-stuttgart.iaas.ipsm.utils.xml :as xml-utils])
  (:gen-class))

(timbre/refer-timbre)

(def temp-dir (constants/property :execution-path))


(def os-type (s/replace (shell/sh "uname" "-s") "\n" ""))
(def os-arch (s/replace (shell/sh "uname" "-m") "\n" ""))
(def docker-compose-dl-url (str (s/replace (constants/property :docker-compose-bin-uri)
                                           (constants/property :version-key)
                                           (constants/property :docker-compose-version))
                                "-" os-type "-" os-arch))

(defn- force-create-dir!
  [dir]
  (if (.exists dir)
    (if (.isDirectory dir)
      nil
      (do
        (io/delete-file dir)
        (.mkdir dir)))
    (.mkdir dir)))

(defn- create-execution-dir!
  [m]
  {:pre [(:execution-dir-path m)]}
  (force-create-dir! (io/file temp-dir))
  (force-create-dir! (io/file (:execution-dir-path m))))


(defn- download-docker-bin!
  [m]
  {:pre [(:executable-path m) (:executable-name m)]}
  (when-not (.exists (io/file (str (:executable-path m) (:executable-name m))))
    (debug "Downloading from the following url" docker-compose-dl-url)
    (io/copy (io/input-stream docker-compose-dl-url)
             (io/file (str (:executable-path m) (:executable-name m))))
    (shell/sh "chmod" "+x" (str (:executable-path m) (:executable-name m)))))

(defn- add-process-counter-file-name
  [m]
  (assoc m :process-counter-file-name (constants/property :process-counter-file-name)))

(defn- add-process-counter-file-path
  [m]
  {:pre [(:process-counter-file-name m) (:execution-dir-path m)]}
  (assoc m :process-counter-file-path (str (:execution-dir-path m) (:process-counter-file-name m))))

(defn- add-executable-name
  [m]
  (debug "Adding executable name" m)
  (assoc m :executable-name (constants/property :executable-name)))

(defn- add-executable-path
  [m]
  (debug "Adding executable path" m)
  (assoc m :executable-path (constants/property :executable-path)))

(defn- add-execution-dir-path
  [m]
  {:pre [(:target-resource m) (:type (:target-resource m))]}
  (debug "Adding execution path" m)
  (assoc m :execution-dir-path (if (.endsWith temp-dir "/")
                                 (str temp-dir (conversions/winery-encode (:type (:target-resource m))) "/")
                                 (str temp-dir "/" (conversions/winery-encode (:type (:target-resource m))) "/"))))

(defn- add-execution-dir-path-exists?
  [m]
  {:pre [(:execution-dir-path m)]}
  (debug "Adding execution path" m)
  (assoc m :execution-dir-path-exists? (.exists (io/file (:execution-dir-path m)))))

(defn- read-process-counter
  [m]
  {:pre [(:process-counter-file-path m)]}
  (read-string (slurp (io/file (:process-counter-file-path m)))))

(defn- increment-counter!
  [m]
  {:pre [(:process-counter-file-path m)]}
  (debug "Incrementing counter" m)
  (if (.exists (io/file (:process-counter-file-path m)))
    (spit (:process-counter-file-path m) (str (inc (read-process-counter m))))
    (spit (:process-counter-file-path m) "1")))


(defn- decrement-counter!
  [m]
  (debug "Counter will be decremented!!")
  (if (.exists (io/file (:process-counter-file-path m)))
    (spit (:process-counter-file-path m) (dec (read-process-counter m)))
    (spit (:process-counter-file-path m) "0")))



(defn- pass-resource-instance!
  [m]
  {:pre [(:callback m) (:resulting-resource m)]}
  (.onSuccess (:callback m) [] "upcoming"))

(defn- add-resulting-instance-definition
  [m]
  {:pre [(:target-resource m)]}
  (assoc m :resulting-resource (:target-resource m)))

(defn- create-and-pass-resource-instance!
  [m]
  {:pre [(:callback m)]}
  (debug "Creating an passing callback" m)
  (.completeProcessExecutionSuccessfully
   (:current-obj m)
   (java.net.URI.
    (or (:resource-uri m)
        "http://www.example.org/missing-uri/error/"))
   (:callback m)))

(defn- create-and-pass-error!
  [m]
  {:pre [(:callback m) (:current-obj m)]}
  (debug "Passing error to the callback" (or (:err (:compose-up-res m)) "Resource could not be engaged!!"))
  (.completeProcessExecutionwithError (:current-obj m) (or (:err (:compose-up-res m)) "Resource could not be engaged!!") (:callback m)))


(defn- post-process-docker-up!
  [m]
  {:pre [(:compose-up-res m) (:exit (:compose-up-res m))]}
  (debug "Post processing docker up result" m)
  (if (= (:exit (:compose-up-res m)) 0)
    (do
      (increment-counter! m)
      (create-and-pass-resource-instance! m))
    (create-and-pass-error! m)))


(defn- execute-docker-compose-up!
  [m]
  {:pre [(:execution-dir-path m) (:executable-name m)]}
  (info "Execution docker compose up!!!" m)
  (-> m
      (assoc :compose-up-res
             (if (constants/property :exec-env)
               (shell/sh (:executable-name m) "up" "-d" "--no-recreate" :dir (:execution-dir-path m)
                         :env (constants/property :exec-env))
               (shell/sh (:executable-name m) "up" "-d" "--no-recreate" :dir (:execution-dir-path m))))
      post-process-docker-up!))


(defn- execute-docker-compose-stop!
  [m]
  {:pre [(:execution-dir-path m) (:executable-name m)]}
  (info "Execution docker compose stop!!!" m)
  (-> m
      (assoc :compose-up-res
             (if (constants/property :exec-env)
               (shell/sh (:executable-name m) "stop" :dir (:execution-dir-path m)
                         :env (constants/property :exec-env))
               (shell/sh (:executable-name m) "stop"  :dir (:execution-dir-path m))))
      (debug)))


(defn- add-labels
  [m]
  {:pre [(:target-resource-runnable m)]}
  (assoc m :labels (:labels (yaml/parse-string (:target-resource-runnable m)))))

(defn- remove-labels
  [m]
  {:pre [(:target-resource-runnable m)]}
  (assoc m :target-resource-runnable
         (yaml/generate-string (dissoc (yaml/parse-string (:target-resource-runnable m)) :labels))))


(defn- remove-labels-from-stream
  [m]
  {:pre [(:target-resource-runnable m)]}
  (debug "Removing labels from stream" m)
  (-> m
      add-labels
      remove-labels))

(defn- copy-runnable-into-execution-dir!
  [m]
  (debug "Copying runnable into execution dir " m)
  (io/copy (:target-resource-runnable m) (io/file (:execution-dir-path m) "docker-compose.yml")))


(defn- add-resource-uri-label-key
  [m]
  (assoc m :resource-uri-label-key
         (or (if (keyword? (constants/property :resource-label-key))
               (constants/property :resource-label-key)
               (keyword? (constants/property :resource-label-key)))
             (do
               (warn ":resource-label-key is not specififed (:de.uni-stuttgart.ipsm.resource-uri) default will be used")
               :de.uni-stuttgart.ipsm.resource-uri))))

(defn- add-resource-uri-from-docker-compose-labels
  [m]
  {:pre [(:target-resource-runnable m)
         (:resource-uri-label-key m)]}
  {:pre [(:labels m)]}
  (let [label-key (:resource-uri-label-key m)]
    (->> m
       :labels
       label-key
       (assoc m :resource-uri))))


(defn- prepare-for-cmd-exec
  [m]
  (-> m
      add-executable-name
      add-executable-path
      add-execution-dir-path
      add-process-counter-file-name
      remove-labels-from-stream
      add-resource-uri-label-key
      add-resource-uri-from-docker-compose-labels
      add-process-counter-file-path))

(defn- prepare-docker-compose!
  [m]
  (doto (-> m
            prepare-for-cmd-exec)
    download-docker-bin!
    create-execution-dir!
    copy-runnable-into-execution-dir!))

(defn- apply-and-add-into-map-if-not-nil
  [m key val desired-fn]
  (if val
    (assoc m key (desired-fn val))
    m))

(defn- init-deployable
  [m]
  (debug "Resource will be initialized")
  (doto (-> m
            prepare-for-cmd-exec)
    prepare-docker-compose!
    execute-docker-compose-up!))

(defn- release-or-decrement-counter!
  [m]
  (let [counter (read-process-counter m)]
    (debug "Counter is" counter)
    (if (> counter 1)
      (decrement-counter! m)
      (if (= counter 1)
        (do
          (execute-docker-compose-stop! m)
          (decrement-counter! m))))))

(defn- release-deployable
  [m]
  (debug "Resource will be released")
  (doto (-> m
            prepare-for-cmd-exec)
    prepare-docker-compose!
    release-or-decrement-counter!))

(gen-class :name "uni_stuttgart.ipsm.execution_environment_integrator.AcquireResourceOperation"
           :extends uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.AcquireResourceOperation
           :prefix "acquire-resources-")

(defn acquire-resources-getSupportedResources [_] nil)
(defn acquire-resources-getSupportedDomains [_] [(constants/property :domain-uri)])
(defn acquire-resources-executeOperation
  [this target-resource-runnable-container parameters callback]
  (debug "Executing acquire operation")
  (init-deployable {:target-resource (transformations/get-map-of-jaxb-type-obj
                                      (.getTargetModel target-resource-runnable-container))
                    :target-resource-runnable (slurp (.getDeployable target-resource-runnable-container))
                    :callback callback
                    :current-obj this}))

(gen-class :name "uni_stuttgart.ipsm.execution_environment_integrator.AcquireRelationshipOperation"
           :extends uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.AcquireRelationshipOperation
           :prefix "acquire-relationships-")

(defn acquire-relationships-getSupportedTargetRelationships [_] [])

(defn acquire-relationships-getSupportedSourceRelationships [_] [])

(defn acquire-relationships-getSupportedTargetDomains [_] [])

(defn acquire-relationships-getSupportedSourceDomains [_] [])

(defn acquire-relationships-executeOperation
  [_ target-resource-runnable-container parameters callback]
                  (init-deployable target-resource-runnable-container parameters callback))

(gen-class :name "uni_stuttgart.ipsm.execution_environment_integrator.ReleaseResourceOperation"
           :extends uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.ReleaseResourceOperation
           :prefix "release-resources-")

(defn release-resources-getSupportedResources [_] nil)

(defn release-resources-getSupportedDomains [_] [(constants/property :domain-uri)])

(defn release-resources-executeOperation
  [this target-resource-runnable-container parameters callback]
  (debug "Executing release operation")
  (release-deployable {:target-resource (transformations/get-map-of-jaxb-type-obj
                                         (.getTargetModel target-resource-runnable-container))
                       :target-resource-runnable (slurp (.getDeployable target-resource-runnable-container))
                       :callback callback
                       :current-obj this}))

(gen-class :name "uni_stuttgart.ipsm.execution_environment_integrator.ReleaseResourceOperation"
           :extends uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.StoreResourceOperation
           :prefix "store-resources-")

(defn store-resources-getSupportedResources [_] nil)

(defn store-resources-getSupportedDomains [_] [(constants/property :domain-uri)])

(defn store-resources-executeOperation
  [_ target-resource-runnable-container parameters callback]
  (init-deployable target-resource-runnable-container parameters callback))



(deftype DockerExecutionEnvironmentMetadata []
  uni_stuttgart.ipsm.protocols.integration.IntegratorMetadata
  (getName [_] (constants/property :name))
  (getTargetNamespace [_] (constants/property :domain-uri))
  (getUri [_] (java.net.URI. (constants/property :uri))))


(def acquire-resource-operation
 (proxy [uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.AcquireResourceOperation]
     []
   (getSupportedResources [] nil)
   (getSupportedDomains [] [(constants/property :domain-uri)])
   (executeOperation [target-resource-runnable-container parameters callback]
     (debug "Executing acquire operation")
     (init-deployable {:target-resource (transformations/get-map-of-jaxb-type-obj
                                         (.getTargetModel target-resource-runnable-container))
                       :target-resource-runnable (slurp (.getDeployable target-resource-runnable-container))
                       :callback callback
                       :current-obj this}))))

(def acquire-relationship-operation
 (proxy [uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.AcquireRelationshipOperation]
     []
   (getSupportedTargetRelationships [] [])
   (getSupportedSourceRelationships [] [])
   (getSupportedTargetDomains [] [])
   (getSupportedSourceDomains [] [])
   (executeOperation [target-resource-runnable-container parameters callback]
     (init-deployable target-resource-runnable-container parameters callback))))

(def release-operation
 (proxy [uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.ReleaseResourceOperation]
     []
   (getSupportedResources [] nil)
   (getSupportedDomains [] [(constants/property :domain-uri)])
   (executeOperation [target-resource-runnable-container parameters callback]
     (debug "Executing release operation")
     (release-deployable {:target-resource (transformations/get-map-of-jaxb-type-obj
                                             (.getTargetModel target-resource-runnable-container))
                           :target-resource-runnable (slurp (.getDeployable target-resource-runnable-container))
                           :callback callback
                           :current-obj this}))))

(def store-operation
 (proxy [uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.StoreResourceOperation]
     []
   (getSupportedResources [] nil)
   (getSupportedDomains [] [(constants/property :domain-uri)])
   (executeOperation [target-resource-runnable-container parameters callback]
     (init-deployable target-resource-runnable-container parameters callback))))

(defn run-dip
  [args&]
  (integrator/register-execution-environment-integrator! [store-operation
                                                          release-operation
                                                          acquire-resource-operation
                                                          acquire-relationship-operation]
                                                         (DockerExecutionEnvironmentMetadata.)))


;; (run-dip [])
