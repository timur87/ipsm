(def tk-version "1.5.2")

(defproject uni-stuttgart.ipsm/tosca-persistence "0.0.1-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0-alpha13"]
                 [environ "1.1.0"]
                 [puppetlabs/trapperkeeper "1.5.2"]
                 [com.taoensso/timbre "4.7.4"]
                 [de.uni-stuttgart.iaas.ipsm/commons "0.0.2-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/protocols "0.5.3-SNAPSHOT"]
                 [cheshire "5.6.3"]
                 [puppetlabs/trapperkeeper-webserver-jetty9 "1.5.11"]
                 [dire "0.5.4"]
                 ;; Winery dependencies
                 [org.eclipse.winery/org.eclipse.winery.repository.client "0.1.37-SNAPSHOT"]
                 [org.eclipse.winery/org.eclipse.winery.model.csar.toscametafile "0.0.4-SNAPSHOT"]
                 [org.eclipse.winery/org.eclipse.winery.common "0.1.37-SNAPSHOT"]
                 [org.eclipse.winery/org.eclipse.winery.model.tosca "0.1.21-SNAPSHOT"]
                 ;;; winery deps for production code
                 [net.sf.saxon/saxon-dom "8.7"]
                 [com.sun.jersey/jersey-core "1.17"]
                 [com.sun.jersey/jersey-client "1.17"]
                 [org.slf4j/jcl-over-slf4j "1.7.6"]
                 [com.fasterxml.jackson.jaxrs/jackson-jaxrs-json-provider "2.2.2"]
                 [com.fasterxml.jackson.core/jackson-databind "2.2.2"]]

  :profiles {:dev {:source-paths ["dev"]
                   :dependencies [[puppetlabs/trapperkeeper ~tk-version :classifier "test" :scope "test"]]}}

  :repl-options {:init-ns user}
  :source-paths ["src/main/clj"]
  :uberjar-name "persistence.jar"
  :resource-paths ["src/main/resources"]
  :aliases {"tk" ["trampoline" "run" "--config" "dev-resources/config.conf"]}
  :main puppetlabs.trapperkeeper.main)
