(defproject de.uni-stuttgart.iaas.ipsm/docker-execution-environment-integrator "0.0.1-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.9.0-alpha13"]
                 [com.taoensso/timbre "4.7.4"]
                 [org.clojure/data.xml "0.0.8"]
                 [clojure-saxon "0.9.4"]
                 [de.uni-stuttgart.iaas.ipsm/integrator-client "0.0.1-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/commons "0.0.2-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/protocols "0.5.3-SNAPSHOT"]
                 [environ "1.0.0"]
                 [clj-http "2.0.1"]
                 [com.github.docker-java/docker-java "1.3.0"]]
  :plugins [[lein-environ "1.0.2"]]
  :source-paths ["src/main/clj"]
  :resource-paths ["src/main/resources"]
  :ring {:handler de.uni-stuttgart.iaas.ipsm.providers.docker-compose/runner})
