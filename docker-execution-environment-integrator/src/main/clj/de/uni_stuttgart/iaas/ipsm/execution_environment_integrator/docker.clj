(ns de.uni-stuttgart.iaas.ipsm.execution-environment-integrator.docker
  (:require [taoensso.timbre :as timbre]
            [environ.core :refer [env]]
            [clojure.java.io :as io]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protos]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
            [de.uni-stuttgart.iaas.ipsm.integrator-client.execution-environment-integrator :as integrator]
            [clj-http.client :as http]
            [de.uni-stuttgart.iaas.ipsm.utils.xml :as xml-utils])
  (:import [com.github.dockerjava.api DockerClient]
           [com.github.dockerjava.core DockerClientBuilder DockerClientConfig]))

(timbre/refer-timbre)



(def docker-config (-> (DockerClientConfig/createDefaultConfigBuilder)
                       (.withVersion (constants/property :docker-version))
                       (.withUri (constants/property :docker-uri))
                       (.withDockerCertPath (constants/property :docker-cert-path))
                       (.build)))

(def docker-client (.build (DockerClientBuilder/getInstance docker-config)))



(defn- create-map
  "Creates a map out of deployable-container"
  [deployable-container entity-type]
  (-> {}
      (assoc
       :entity-data
       (domain-language/get-maps-for-owl-str (.getModel deployable-container) entity-type))
      (assoc :deployable (.getDeploymentArtifact deployable-container))))


(defn- create-maps
  [deployable-containers-v target-entity]
  (mapv
   #(create-map % target-entity)
   deployable-containers-v))

(defn- add-maps
  [m source-key target-key entity-type]
  (if (target-key m)
    (update-in m [target-key] #(create-maps (source-key m) entity-type))
    (assoc m target-key (create-maps (source-key m) entity-type))))

(defn- add-map
  [m source-key target-key entity-type]
  (if (target-key m)
    (update-in m [target-key] #(vec (concat (create-map (source-key m) entity-type))))
    (assoc m target-key (create-map (source-key m) entity-type))))


(defn- create-maps
  [m]
  (-> m
      (create-maps :resource-to-init :resource-to-init-map :resource-definition)
      (create-maps :relationships :relationship-maps :relationship-definition)
      (create-maps :related-resources :related-resources :resource-definition)))


(defn- init-deployable
  [resource-to-be-init relationships related-resources callback]
  (-> {:resource-to-init resource-to-be-init
       :relationships relationships
       :related-resources related-resources
       :callback callback}))


(deftype DockerExecutionIntegratorOperations []
  de.uni_stuttgart.iaas.ipsm.protocols.integration.ExecutionIntegratorOperations
  (initDeployable [this resource-to-be-init relationships related-resources callback] "AXS")
  (releaseResource [this resource-instance-model relationships related-resources callback])
  (storeResource [this resource-instance-model callback])
  (getProcessingState [this process-id]))


(deftype DockerExecutionEnvironmentMetadata []
  de.uni_stuttgart.iaas.ipsm.protocols.integration.IntegratorMetadata
  (getName [_] (constants/property :name))
  (getId [_] (constants/property :domain-uri))
  (getUri [_] (constants/property :uri)))


(defn runner
  [__prefix__]
  (integrator/provider-factory-registerExecutionEnvironmentIntegrator
   (DockerExecutionIntegratorOperations.)
   (DockerExecutionEnvironmentMetadata.)))


;; (def repl-test (runner nil))
