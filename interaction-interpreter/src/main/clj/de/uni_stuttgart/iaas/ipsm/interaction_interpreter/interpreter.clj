(ns de.uni-stuttgart.iaas.ipsm.interaction-interpreter.interpreter
  (:require
   [cheshire.core :refer :all]
   [clj-http.client :as client]
   [de.uni-stuttgart.iaas.ipsm.ipe.model.ontology.individual-mgt :as individual-mgt]
   [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions]
   [de.uni-stuttgart.iaas.ipsm.ipe.model.interactions :as interactions]
   [de.uni-stuttgart.iaas.ipsm.ipe.model.resources :as resources]
   [de.uni-stuttgart.iaas.ipsm.interaction-services.git.service :as git-service])
  (:import
   [javax.xml.namespace QName]))


(def interaction-services [git-service/service-instance])





;; (let [size 130
;;          file-name "crawl-data/author"]
;;      (loop [index 0]
;;        (let [resource (into {} (remove #(or (nil? (second %)) (= "" (second %))) (read-string (slurp (str file-name "-" index ".clj")))))
;;              resource-entity (resources/entity-map "temp"
;;                                                    (:name resource)
;;                                                    (or (:bio resource) "A GIT Hub Memeber")
;;                                                    "http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/user"
;;                                                    "http://www.iaas.uni-stuttgart.de/ipsm/domains/git-hub"
;;                                                    (create-properties {:email (:email resource)
;;                                                                        :login (:login resource)
;;                                                                        :url (:html_url resource)}))]
;;          (debug "this resource will be added" resource-entity)
;;          (add-resource! resource-entity))
;;        (if (< index size)
;;          (recur (inc index)))))


;; (remove #(not (nil? (:name %))) (get-resources))
;; (mapv :name (get-resources))


;; (add-resource! (let [resource (into {} (remove #(or (nil? (second %)) (= "" (second %))) (read-string (slurp "crawl-data/author-0.clj"))))]
;;    (resources/entity-map "temp"
;;                          (:name resource)
;;                          (or (:bio resource) "A GIT Hub Memeber")
;;                          "http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/user"
;;                          "http://www.iaas.uni-stuttgart.de/ipsm/capabilities/definitions/git/"
;;                          (create-properties (dissoc resource :name)))))




;; (let [size 302
;;       file-name "crawl-data/commit-page"]
;;      (loop [index 0]
;;        (let [commits-per-file (read-string (str "[" (slurp (str file-name "-" index ".clj")) "]"))]
;;          (mapv
;;           #(debug )
;;           commits-per-file))
;;        (if (< index size)
;;          (recur (inc index)))))

;; ;(create-commit-interaction (read-string (slurp "crawl-data/commit-page-0.clj")))



;; (defn create-commit-interaction
;;   [commit]
;;   (individual-mgt/create-interaction!
;;    (interactions/entity-map (:sha commit)
;;                             "Commit"
;;                             "A contributor commit to the resource"
;;                             "http://www.iaas.uni-stuttgart.de/ipsm/interaction-types/git/commit"
;;                             [{:id (conversions/create-unique-id-from-str (:name (:author commit)) "http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/user")}]
;;                             [{:id (conversions/create-unique-id-from-str "MainRepo" "http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/repo")}]
;;                             [{:id "787a315dc4bbe2ea8a6981955175e8de02a38f84bff13ad643606d09202e7e7c"}])))

;; (individual-mgt/get-resource-by-id (conversions/create-unique-id-from-str "MainRepo" "http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/repo"))

;; (print (mapv
;;         :id
;;         (individual-mgt/get-capabilities)))


;; (individual-mgt/create-resource! (resources/entity-map
;;                                   (conversions/create-unique-id-from-str "MainRepo" "http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/repo")
;;                                   "MainRepo"
;;                                   "Main JClouds Repo"
;;                                   "http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/repo"
;;                                   "http://www.iaas.uni-stuttgart.de/ipsm/domains/git/"
;;                                   (create-properties {:owner "jclouds" :repo "jclouds"})))

;; (conversions/create-unique-id-from-str "MainRepo" "http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/repo")

;; (conversions/create-unique-id-from-str (:name (individual-mgt/get-resource-by-id "6540bb689ed32e095502cbf6f039435fbef60cbb1cda0ddd621d59da67691caa")) (:type (individual-mgt/get-resource-by-id "6540bb689ed32e095502cbf6f039435fbef60cbb1cda0ddd621d59da67691caa")))
;; (individual-mgt/remove-resource-by-id! "f281995f65a5150efe94d5a2bcf1ddd58c3c49b152686eb2f6ccde405a9598e9")
;; (individual-mgt/get-resource-by-id (conversions/create-unique-id-from-str "MainRepo" "http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/repo"))
;; (individual-mgt/create-resource! (resources/entity-map
;;                                   (conversions/create-unique-id-from-str "MainRepo" "http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/user")
;;                                   "MainRepo"
;;                                   "Main JClouds Repo"
;;                                   "http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/repo"
;;                                   "http://www.iaas.uni-stuttgart.de/ipsm/capabilities/definitions/git/"
;;                                   (create-properties {:owner "jclouds" :repo "jclouds"})))




(defprotocol InteractionInterpreter
  "Defines standard operations of the interaction interpreter"
  (refresh-capability-definitions [this] "Refresh individuals with the capability definitions provided by capability plug-ins. Implementation should be idempotent"))

(defn- add-capability-to-ontology-model!
  [capability-definitions]
  (individual-mgt/add-capability-definitions! capability-definitions))

(defn- refresh-capability-definitions-using-services
  (mapv
   #(add-capability-to-ontology-model! (.get-capability-definitions %))
   interaction-services))


(defrecord interpreter-impl []
  InteractionInterpreter
  (refresh-capability-definitions [this] ()))
