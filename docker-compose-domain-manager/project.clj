(defproject uni-stuttgart.ipsm/docker-compose-domain-manager "0.0.1-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.9.0-alpha14"]
                 [com.taoensso/timbre "4.1.4"]
                 [org.clojure/data.json "0.2.6"]
                 [circleci/clj-yaml "0.5.4"]
                 [de.uni-stuttgart.iaas.ipsm/commons "0.0.2-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/protocols "0.5.3-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/integrator-client "0.0.3-SNAPSHOT"]
                 [clj-http "2.0.1"]]
  :source-paths ["src/main/clj"]
  :resource-paths ["src/main/resources"])
