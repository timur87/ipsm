(ns de.uni-stuttgart.iaas.ipsm.utils.winery
  (:require [taoensso.timbre :as timbre]
            [clojure.java.io :as io]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.transformations :as tx]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as c]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as co]
            [clojure.data.json :as j]
            [clj-http.client :as http])
  (:import [java.net URI]))


(timbre/refer-timbre)


(defn- upload-definitions-into-winery!
  [m]
  {:pre [(:winery-uri m) (:defs-str m)]}
  (debug "Posting maps" m)
  (http/post (:winery-uri m)
             {:content-type :xml
              :body (:defs-str m)}))


(defn upload-definitions-map-into-winery!
  [m]
  {:pre [(:defs m) (:defs-str m)]}
  (debug "Posting maps" m)
  (http/post (:winery-uri m)
             {:content-type :xml
              :body (tx/defs-map->str (:defs m))}))


;;TODO must be completed
(defn- upload-csar-into-winery!
  [m]
  {:pre [(:winery-uri m)
         (:csar m)]}
  (http/post (:winery-uri m)
             {:multipart [{:name "file" :content (:csar m)}
                          {:name "overwrite" :content true}]
              :body (:defs-str m)}))

(defn- imports?
  [m]
  {:pre [(:entity-data m)]
   :post [(:imports? %)]}
  (assoc m :imports? (:imports (:entity-data m))))


(defn- add-csar-name
  [m]
  {:post [(:csar-name %)]}
  (assoc m :csar-name (str (co/create-unique-id) ".csar")))


(defn- add-csar-folder-path
  [m]
  {:pre [(:csar-name m)]
   :post [(:csar-name-path %)]}
  (assoc m :csar-name-path (str "temp/" (:csar-name m))))


(defn- add-target-namespace-if-available
  [m]
  {:pre [(:entity-data m) (:entity-obj m)]}
  (if (:target-namespace (:entity-data m))
    (.setTargetNamespace (:entity-obj m) (:target-namespace (:entity-data m))))
  m)


(defn- add-definitions-str-for-types
  [m]
  (->> m
       :entity-data
       :types
       (apply tx/get-definitions-obj-with-types)
       (assoc m :entity-obj)
       add-target-namespace-if-available
       :entity-obj
       tx/get-str-of-jaxb-obj
       (assoc m :defs-str)))


(defn- prepare-for-addition
  [m]
  (-> m
      add-csar-name
      add-csar-folder-path
      add-definitions-str-for-types))


;; removes the types relevant to a domain manager data we delete them just before uploading the new ones to avoid the gap in between


(defn- post-types-into-winery!
  [m]
  (->> m
       :entity-data
       tx/get-definitions-xml-with-types
       (assoc m :defs-str)
       upload-definitions-into-winery!))


(defn- add-list-response
  [m]
  {:pre [(:winery-entity-path m)]}
  (assoc m :response
         (http/get (:winery-entity-path m)
                   {:accept :json})))


(defn- add-entity-list
  [m]
  {:pre [(:response m)]
   :post [(:entity-list %)]}
  (assoc m :entity-list (if (:body (:response m))
                          (j/read-json (:body (:response m)))
                          [])))


(defn- add-url-based-on-ns-id
  [m]
  {:pre [(:winery-entity-path m)]}
  (if-not (and (or (:namespace (:entity-data m))
                   (:target-namespace (:entity-data m)))
               (or (:id (:entity-data m))
                   (:name (:entity-data m))))
    (error "Namespace OR target-namespace = nil AND id or name = nil. This condition must evaluate to true " (:entity-data m)))
  (assoc m :url (str (:winery-entity-path m)
                     (let [qn (javax.xml.namespace.QName.
                               (or (:namespace (:entity-data m))
                                   (:target-namespace (:entity-data m)))
                               (or (:id (:entity-data m))
                                   (:name (:entity-data m))))]
                       (str (co/winery-encode (.getNamespaceURI qn))
                            "/"
                            (co/winery-encode (.getLocalPart qn))))
                     "/")))


(defn- add-urls-for-entity-list
  [m]
  {:pre [(:entity-list m)]
   :post [(:urls %)]}
  (->> m
       :entity-list
       (mapv #(-> (assoc m :entity-data %)
                  add-url-based-on-ns-id
                  :url))
       (assoc m :urls)))


(defn- add-url->types
  [m]
  {:pre [(:url m)]
   :post [(:defs %)]}
  (assoc m :defs (let [resp (http/get (:url m) {:accept :xml})]
                   (if (= (:status resp) 200)
                     (tx/get-types-from-definitions-xml (:body resp))
                     nil))))


(defn- add-urls->types
  [m]
  (assoc m :types (some->> m
                           :urls
                           (mapcat #(-> m
                                        (assoc :url %)
                                        add-url->types
                                        :defs))
                           (filterv identity))))


(defn- add-possible-entities-using-response
  [m]
  {:pre [(:response m)]
   :post [(:entity-data %)]}
  (cond-> m
    (= 200 (:status (:response m))) add-entity-list
    (= 200 (:status (:response m))) add-urls-for-entity-list
    (= 200 (:status (:response m))) add-urls->types
    (not= 200 (:status (:response m))) (assoc :entity-data [])))


(defn- add-winery-entity-type-path
  [m]
  {:pre [(:winery-uri m)
         (:entity-type (:entity-data m))
         (:winery-path-mappings m)]}
  (assoc m :winery-entity-path (str (:winery-uri m)
                                    (get (:winery-path-mappings m)
                                         (:entity-type (:entity-data m))))))



(defn get-entity-type-from-winery
  "Adds :types for a specific entity type such as :node-type. Entity
  type should be stored in {:entity-data {:entity-type :node-type}}"
  [m]
  (-> m
      add-winery-entity-type-path
      add-list-response
      add-entity-list
      add-possible-entities-using-response))


(defn- delete-entity!
  [m]
  {:pre [(:url m)]}
  (http/delete (:url m)))


(defn delete-entity-from-winery!
  [m]
  (-> m
      add-winery-entity-type-path
      add-url-based-on-ns-id
      delete-entity!))



(defn get-entity-from-winery
  [m]
  (-> m
      add-winery-entity-type-path
      add-url-based-on-ns-id
      add-url->types
      :defs
      first))

(defn- filter-namespaces-if-needed
  [m]
  {:pre [(:entity-list m)]
   :post [(:entity-list %)]}
  (if (:target-namespace m)
    (->> m
         :entity-list
         (filterv #(= (:target-namespace m) (:namespace %)))
         (assoc m :entity-list))
    m))


(defn delete-entity-type-from-winery!
  [m]
  (->> m
       add-winery-entity-type-path
       add-list-response
       add-entity-list
       filter-namespaces-if-needed
       add-urls-for-entity-list
       :urls
       (mapv #(delete-entity! (assoc m :url %))))
  nil)


(defn delete-entity-types-from-winery!
  [m]
  (if (and (:entity-types m) (vector? (:entity-types m)))
    (->> m
         :entity-types
         (mapv #(delete-entity-type-from-winery! (assoc-in m [:entity-data :entity-type] %))))
    (delete-entity-type-from-winery! m))
  nil)


(defn get-entity-types-from-winery
  [m]
  (if (and (:entity-types m) (vector? (:entity-types m)))
    (some->> m
             :entity-types
             (mapv #(-> m (assoc :entity-data {:entity-type %}) get-entity-type-from-winery :types))
             (reduce into)
             (remove empty?)
             (filterv identity)
             (assoc m :types))
    (get-entity-from-winery m)))

(defn- delete-domain-manager-relevant-types!
  [m]
  (-> m
      (assoc :target-namespace (:target-namespace (:entity-data m)))
      (assoc :entity-types [:artifact-type
                            :node-type
                            :node-type-implementation
                            :relationship-type-implementation
                            :relationship-type
                            :artifact-template])
      delete-entity-types-from-winery!))

(defn add-domain-manager-data-into-winery!
  [m]
  {:pre [(:entity-data m)]}
  (let [m (prepare-for-addition m)]
    (delete-domain-manager-relevant-types! m)
    (cond
      (or (not (:imports (:entity-data m)))
          (not (seq (:imports (:entity-data m))))) (upload-definitions-into-winery! m)
      (seq (:imports (:entity-data m))) (upload-csar-into-winery! m))))

(defn- add-winery-url->csar-stream
  [m]
  {:pre [(:csar-url m)]
   :post [(:csar-stream %)]}
  (assoc m :csar-stream (io/input-stream (:winery-uri m))))

(defn- add-winery-url->csar-url
  [m]
  {:pre [(:url m)]
   :post [(:csar-url %)]}
  (assoc m :csar-url (if (.endsWith (:url m) "/")
                       (str (:url m) "?csar")
                       (str (:url m) "/?csar"))))


(defn add-csar-stream
  [m]
  (-> m
      add-winery-entity-type-path
      add-url-based-on-ns-id
      add-winery-url->csar-url
      add-winery-url->csar-stream))
