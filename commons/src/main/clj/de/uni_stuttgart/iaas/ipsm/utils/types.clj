(ns de.uni-stuttgart.iaas.ipsm.utils.types
  (:require [taoensso.timbre :as timbre]))

(defmacro deftype-with-extensions
  "The first item of the argument list (args) contains the super type of this type."
  [new-type args & protocol-fn-pairs]
  `(let [extension-concept# (defrecord ~new-type ~args)];parent-map
     (mapv
      (fn [protocol-fn-pair#]
        (apply extend extension-concept# protocol-fn-pair#))
      ~@protocol-fn-pairs)
     extension-concept#))
