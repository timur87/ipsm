(ns de.uni-stuttgart.iaas.ipsm.utils.io
  (:require [clojure.java.io :as io]
            [clojure.data.json :as json]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions]))

(defn read-resource
  "Read a file defined under resource directory. Takes a relative file path argument and returns the string representation of the file. Suitable for small files."
  [path]
  (some-> path
          io/resource
          slurp))

(defn read-json-resource
  "Read a json file defined under resource directory. Takes a relative file path argument and returns the EDN representation of the JSON file. Suitable for small files."
  [path]
  (conversions/read-json (read-resource path)))
