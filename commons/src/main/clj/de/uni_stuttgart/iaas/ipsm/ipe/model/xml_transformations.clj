(ns de.uni-stuttgart.iaas.ipsm.ipe.model.xml-transformations)


(def xml-mappings {:definitions {:xml-tag "Definitions" :xml-type :element :type :val}
                   :intention {:xml-tag "Intention" :xml-type :element :type :val}
                   :interaction {:xml-tag "Interaction" :xml-type :element :type :val}
                   :action {:xml-tag"Action" :xml-type :element :type :val}
                   :ability {:xml-tag "Ability" :xml-type :element :type :val}
                   :actor {:xml-tag "Actor" :xml-type :element :type :val}
                   :basic-concept {:xml-tag "BasicConcept" :xml-type :element :type :val}
                   :capability {:xml-tag "Capability" :xml-type :element :type :val}
                   :capability-definition {:xml-tag "CapabilityDefinition" :xml-type :element :type :val}
                   :concept-with-properties {:xml-tag "ConceptWithProperties" :xml-type :element :type :val}
                   :content {:xml-tag "Content" :xml-type :element :type :val}
                   :context {:xml-tag "Context" :xml-type :element :type :val}
                   :domain-concept {:xml-tag "DomainConcept" :xml-type :element :type :val}
                   :functional-definition {:xml-tag "FunctionalDefinition" :xml-type :element :type :val}
                   :identifiable-concept {:xml-tag "IdentifiableConcept" :xml-type :element :type :val}
                   :informal-process {:xml-tag "InformalProcess" :xml-type :element :type :val}
                   :key-value-property {:xml-tag "KeyValueProperty" :xml-type :element :type :val}
                   :xml-typed-property {:xml-tag "TypedProperty" :xml-type :element :type :val}
                   :non-functional-property {:xml-tag "NonFunctionalDefinition" :xml-type :element :type :val}
                   :property {:xml-tag "Property" :xml-type :element :type :val}
                   :relationship-definition {:xml-tag "RelationshipDefinition" :xml-type :element :type :val}
                   :preconditioner-relationship-definition {:xml-tag "PreConditionerRelationshipDefinition" :xml-type :element :type :val}
                   :postconditioner-relationship-definition {:xml-tag "PostConditionerRelationshipDefinition" :xml-type :element :type :val}
                   :instantiable-relationship-definition {:xml-tag "InstantiableRelationshipDefinition" :xml-type :element :type :val}
                   :resource-definition {:xml-tag "ResourceDefinition" :xml-type :element :type :val}
                   :resource-instance {:xml-tag "ResourceInstance" :xml-type :element :type :val}
                   :relationship-instance {:xml-tag "RelationshipDefinition" :xml-type :element :type :val}
                   :informal-process-instance {:xml-tag "InformalProcessInstance" :xml-type :element :type :val}
                   :instance-descriptor {:xml-tag "InstanceDescriptor" :xml-type :element :type :val}
                   :resource-provider {:xml-tag "ResourceProvider" :xml-type :element :type :val}
                   :source-resource  {:xml-tag "SourceResource" :xml-type :element :type :val}
                   :target-resource {:xml-tag "TargetResource" :xml-type :element :type :val}
                   :xml-typed-concept {:xml-tag "TypedConcept" :xml-type :element :type :val}
                   :used-capability {:xml-tag "UsedCapability" :xml-type :element :type :val}
                   :id {:xml-tag "Id" :xml-type :attribute}
                   :name {:xml-tag "Name" :xml-type :attribute}
                   :description {:xml-tag "Description" :xml-type :element :type :val}
                   :xml-type {:xml-tag "Type" :xml-type :attribute}
                   :runnable-uri {:xml-tag "RunnableUri" :xml-type :attribute}
                   :runnable-name {:xml-tag "RunnableName" :xml-type :attribute}
                   :domain-uri {:xml-tag "domain" :xml-type :attribute}
                   :deployment-descriptor {:xml-tag "DeploymentDescriptor" :xml-type :element :type :val}
                   :key {:xml-tag "key"  :xml-type :element :type :val}
                   :value {:xml-tag "value" :xml-type :element :type :val}
                   :end-time {:xml-tag "EndTime" :xml-type :element :type :val}
                   :start-time {:xml-tag "StartTime" :xml-type :element :type :val}
                   :instance-location {:xml-tag "InstanceLocation" :xml-type :attribute}
                   :instance-state {:xml-tag "InstanceState" :xml-type :attribute}
                   :source-model {:xml-tag "SourceModel" :xml-type :attribute}
                   :initial-context {:xml-tag "InitialContext" :xml-type :element :type :val}
                   :resources {:xml-tag "Resources" :xml-type :element :type :vector}
                   :source-resources {:xml-tag "SourceResources" :xml-type :element :type :vector}
                   :target-resources {:xml-tag "TargetResources" :xml-type :element :type :vector}
                   :valid-sources {:xml-tag "ValidSources" :xml-type :element :type :vector}
                   :valid-targets {:xml-tag "ValidTargets" :xml-type :element :type :vector}
                   :relationships {:xml-tag "Relationships" :xml-type :element :type :vector}
                   :final-context {:xml-tag "FinalContext" :xml-type :element :type :val}
                   :main-intention {:xml-tag "MainIntention" :xml-type :element :type :val}
                   :required-capabilities {:xml-tag "RequiredCapabilities" :xml-type :element :type :vector}
                   :sub-intentions {:xml-tag "Intentions" :xml-type :element :type :vector}
                   :execution-environment {:xml-tag "executionEnvironment" :xml-type :attribute}
                   :properties {:xml-tag "Property" :xml-type :element :type :vector}
                   :informal-processes {:xml-tag "InformalProcesses" :xml-type :element :type :vector}
                   :capabilities {:xml-tag "Capabilities" :xml-type :element :type :vector}
                   :intentions {:xml-tag "Capabilities" :xml-type :element :type :vector}
                   :resource-definitions {:xml-tag "ResourceDefinitions" :xml-type :element :type :vector}
                   :relationship-definitions {:xml-tag "RelationshipDefinitions" :xml-type :element :type :vector}
                   :resource-instances {:xml-tag "ResourceInstances" :xml-type :element :type :vector}
                   :relationship-instances {:xml-tag "RelationshipInstances" :xml-type :element :type :vector}
                   :informal-process-instances {:xml-tag "InformalProcessInstances" :xml-type :element :type :vector}})
