(ns de.uni-stuttgart.iaas.ipsm.utils.constants
  (:require [clojure.data.json :as json]
           [clojure.java.io :as io]
           [clojure.string :as str]
           [nomad :refer [defconfig]]
           [environ.core :refer [env]]
           [taoensso.timbre :as timbre]
           [de.uni-stuttgart.iaas.ipsm.utils.io :as io-utils]
           [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions]))

(timbre/refer-timbre)

;; html

(def node-type-selection-text "List of Node Types which satisfy the capability.")
(def requirement-type-selection-text "List of Requirement Types which satisfy the capability.")



(def deployable-media-type "application/de.uni-stuttgart.iaas.ipsm.deployable")
(def dipea-media-type "application/de.uni-stuttgart.iaas.ipsm.dipea")
(def dipea-suffix ".dipea")

(def redirect-path "/login-success")
(def modeler-path "/ipe-modeler")

;;properties file fields
(def hadl2xsl "hadl-2-ipe-xsl")
(def branding "branding")

(def ipe-models-collection "ipe-models")
(def users-collection "users")

(def hadl-import-states {:init "init"
                         :on-going "on-going"})

(def config-file-name (or (:confi-file-name env) "config.edn"))

(defn load-props
  [file-name]
  (let [config-file (io/resource file-name)]
    (if config-file
      (read-string (slurp config-file))
      nil)))

(defconfig properties-file (io/resource config-file-name))


(defn property
  [prop-name]
  (if (or (get env (keyword prop-name)) (and properties-file (get (properties-file) prop-name)))
    (or (get env (keyword prop-name)) (and properties-file (get (properties-file) prop-name)))
    (warn "Missing property!!!!" prop-name)))

(defn get-server-config
  "returns http://hostname:port"
  []
  (str (property "host-url")  (if (= "80" (property "host-port")) "" (str ":" (property "host-port")))))

(def ds-name "coAct")
(def product-ns "http://www.uni-stuttgart.de/iaas/ipsm/coact")
(def opentosca-ns "http://www.uni-stuttgart.de/opentosca")
;; Name of the file which contains downloads contents
(def downloads-file-name "downloads.clj")

(def available-ns (property "namespaceMap"))

(defn resolve-uri-of-qname
  "Resolves URI of a prefix based on the given String prefix:localname"
  [qname]
  (get
    available-ns
    (keyword (first (str/split "ipsm:test" #":")))))

(defn resolve-uri-of-prefix
  "Resolves URI of a prefix based on the given String prefix"
  [prefix]
  (get available-ns (keyword prefix)))


(def id-tag "id")
(def ipe-models-tag "ipe-models")

(def error-codes
  {:no-resource-service {:path "no-resource-service"
                         :description "Resource service is not available"}})

(def success-status "success")
(def incomplete-status "incomplete")
(def hadl "hadl")
(def hadl-import "hadl-import")
(def hadl-model "hadl-model")
;; import post parameters
(def hadl-import-status-finalize "finalize")
(def hadl-import-status-cancel "cancel")

(def hadl-step-type "step-type")
(def hadl-resource-step "resource")
(def hadl-relationship-step "relationship")

;; winery properties

(def winery-host (property "winery-url"))
(def repository-url (property "service-url"))
(def modeler-host (property "resource-modeler-url"))

;; xpath query definitions, namespace dexLfinitions are found in constants

(defn distinct-values-query
  "get the distinct values query for the given query"
  [query]
  (str "distinct-values(" query ")"))

(def resource-query "//ipsm:Resource") ;; get all resources

(def capabilities-query (str resource-query "//ipsm:Capability/@ipsm:type"))

(def requirements-query (str resource-query "//ipsm:Requirement/@ipsm:requiredCapability"))

(def root-element "/ipsm:IPEModel")

(def root-element ".")

(def distinct-capabilities-query (distinct-values-query capabilities-query)) ;; get different capabilities each should be a QName

(def distinct-requirements-query (distinct-values-query requirements-query)) ;; get different capabilities each should be a QName


(defn create-dipea-file-name-for-ipe-process
  [process-map]
  (str (:id (:entity-map process-map)) dipea-suffix))
