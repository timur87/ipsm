(ns de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language
  (:require [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conv]
            [uni-stuttgart.jaxb-conversions.core :as j]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.tosca :as to]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.schema :as ds]
            [clojure.spec :as sc]
            [taoensso.timbre :as timbre]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protos]))



(timbre/refer-timbre)

(gen-class
 :name de.uni_stuttgart.iaas.ipsm.ipe.model.DomainLanguage
 :prefix provide-)

(defn- create-standard-entity
  [entity-type entity-map]
  {:entity-type entity-type :entity-map entity-map})

(defn- add-class-type
  [m]
  {:pre [(:entity-map m) (:entity-type m) (:schema m)]}
  (-> m
      (assoc-in [:entity-map :entity-type]
                (:entity-type m))))

(defn validate-entity-map
  [m]
  {:pre [(:entity-map m) (:schema m)]}
  (if (sc/valid? (:schema m) (:entity-map m))
    (:entity-map m)
    (do (error "Invalid type" (:schema m) (:entity-map m))
        (error (sc/explain-str (:schema m) (:entity-map m)))
        nil)))

(defn add-data-type-and-validate
  [m]
  {:pre [(:entity-type m) (:schema m) (:entity-map m)]}
  (-> m
      add-class-type
      validate-entity-map))

(defn resource-definition
  "A resource definition adressing TOSCA NodeTypes"
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :node-type :schema ::ds/node-type}))

(defn artifact-type
  "A resource definition adressing TOSCA NodeTypes"
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :artifact-type :schema ::ds/tosca-artifact-type}))


(defn artifact-reference
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :artifact-reference :schema ::ds/artifact-reference}))

(defn artifact-template
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :artifact-template :schema ::ds/artifact-template}))


(defn deployment-artifact
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :deployment-artifact :schema ::ds/single-deployment-artifact}))


(defn implementation-artifact
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :implementation-artifact :schema ::ds/single-implementation-artifact}))


(defn node-type-implementation
  "A resource definition adressing TOSCA NodeTypes"
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :node-type-implementation :schema ::ds/node-type-implementation}))

(defn node-template
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :node-template :schema ::ds/node-template}))

(defn topology-template
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :topology-template :schema ::ds/topology-template}))

(defn service-template
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :service-template :schema ::ds/service-template}))


(defn definition
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :definition :schema ::ds/definition-schema}))


(defn context-definition
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :context :schema ::ds/context-definition}))

(defn intention-definition
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :intention :schema ::ds/intention-definition}))

(defn definitions
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :definitions :schema ::ds/definitions}))

(defn content
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :content :schema ::ds/content}))


(defn instance-descriptor-type
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :instance-descriptor-type :schema ::ds/instance-descriptor}))

(defn instance-descriptor-element
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :instance-descriptor-element :schema ::ds/instance-descriptor}))

(defn informal-process-definition
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :informal-process-definition :schema ::ds/informal-process-definition}))


(defn resource-model
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :resource-model :schema ::ds/resource-model}))

(defn interface
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :interface :schema ::ds/interface}))

(defn operation
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :operation :schema ::ds/single-operation}))

(defn execution-environment-integrator
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :execution-environment-integrator :schema ::ds/execution-environment-integrator}))

(defn operation-message
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :operation-message :schema ::ds/operation-message}))

(defn def-import
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :import :schema ::ds/import}))

(defn domain-manager-data
  [m]
  (add-data-type-and-validate {:entity-map m :entity-type :domain-manager-data :schema ::ds/domain-manager-message}))




(defn add-base-type-ns-and-name-into-map
  [m]
  {:pre [(:entity-data m)]}
  (-> m
      (assoc :target-namespace (:target-namespace (:entity-data m)))
      (assoc :name (:name (:entity-data m)))))

(defn get-resources
  [m]
  {:pre [(or (= (:entity-type (:entity-data m)) :informal-process-definition)
             (= (:entity-type (:entity-data m)) :informal-process-instance))]}
  (or (filterv #(= (:entity-type %) :node-template)
               (-> m
                   (update-in [:entity-data] #(get-in % [:resource-model-instance]))
                   (assoc :id (:ref (:entity-data m)))
                   (assoc :target-namespace (:target-namespace (:entity-data m)))
                   to/get-matching-service-template
                   to/get-relationsip-and-node-templates-of-service-template
                   :entity-data))
      []))

(defn get-relationships
  [m]
  {:pre [(or (= (:entity-type (:entity-data m)) :informal-process-definition)
             (= (:entity-type (:entity-data m)) :informal-process-instance))]}
  (or (filterv #(= (:entity-type %) :relationship-template)
               (-> m
                   (update-in [:entity-data] #(get-in % [:resource-model-instance]))
                   (assoc :id (:ref (:entity-data m)))
                   (assoc :target-namespace (:target-namespace (:entity-data m)))
                   to/get-matching-service-template
                   to/get-relationsip-and-node-templates-of-service-template
                   :entity-data))
      []))


(defn- add-matching-criteria
  [criteria m]
  (assoc m :match-criteria criteria))




(defn replace-resources-or-relationships-of-informal-process
  "Replace resources and relationships based on matching criteria id
  and target-namespace of given new values "
  [m]
  {:pre [(or (= (:entity-type (:entity-data m)) :informal-process-definition)
             (= (:entity-type (:entity-data m)) :informal-process-instance))]}
  (->> m
       :entity-data
       :resource-model-instance
       (assoc m :entity-data)
       to/get-matching-service-template
       (add-matching-criteria [:id :type])
       to/update-node-templates-of-service-template
       :entity-data
       vector
       (assoc m :new-vals)
       (add-matching-criteria [:id :target-namespace])
       to/update-service-template-of-informal-process-instance
       :entity-data
       (assoc m :entity-data)))


(defn add-informal-process-definitions
  [m]
  {:pre [(or (= (:entity-type (:entity-data m)) :informal-process-definition)
             (= (:entity-type (:entity-data m)) :informal-process-instance))]}
  (let [k (if (= (:entity-type (:entity-data m)) :informal-process-definition)
            :resource-model
            :resource-model-instance)]
    (->> m
         :entity-data
         k
         :definitions
         (assoc m :definitions))))


(defn- filter-execution-environments
  [m]
  {:pre [(:execution-environment-integrators m)
         (:target-interface m)]}
  (debug "Filtering execution environments")
  (->> m
       :execution-environment-integrators
       (filterv #(some->> %
                          :interfaces
                          (mapv (fn [interface] (= (:name (:target-interface m)) (:name interface))))
                          (reduce (fn [v1 v2] (or v1 v2)))))))



(defn add-type-of-execution-environment-for-the-resource
  [m]
  {:pre [(:target-resource m)]}
  (some->> (assoc m :target-template (:target-resource m))
           add-informal-process-definitions
           to/add-type-of-a-template
           to/add-interfaces-of-node-type
           :interfaces
           (filterv #(.contains (:name %) (constants/property :life-cycle-interface-postfix)))
           first
           (assoc m :target-interface)
           filter-execution-environments
           first
           (assoc m :matching-ee-integrator)))

(defn add-execution-environment
  [m]
  {:pre [(:target-resource m)]
   :post [(:matching-ee-integrator %)]}
  (assoc m :matching-ee-integrator
         (some->> (assoc m :target-template (:target-resource m))
                  add-informal-process-definitions
                  to/add-type-of-a-template
                  to/add-type-implementation-of-template
                  to/add-implementation-artifacts-of-implementation
                  to/add-matching-artifact-template-ref
                  to/add-matching-artifact-type
                  to/add-matching-artifact-template-from-ref-and-type
                  :artifact-template
                  :artifact-references
                  :artifact-reference
                  first
                  :reference)))

(defn resource-definition?
  [m]
  (= :node-type (:entity-type m)))

(defn add-deployable-uri-of-target-resource
  [m]
  {:pre [(:target-resource m)]}
  (debug "Adding deployable uri")
  (assoc m :deployable-uri
         (some->> (assoc m
                         :node-template (:target-resource m)
                         :type-name (constants/property :deployable-artifact-type-name))
                  add-informal-process-definitions
                  to/add-node-type-implementation-of-node-template
                  to/add-deployment-artifact-based-on-type-local-name
                  to/add-artifact-ref-from-deployment-artifact
                  to/add-artifact-type-from-deployment-artifact
                  to/add-matching-artifact-template-from-ref-and-type
                  to/add-artifact-references
                  :artifact-references
                  first
                  :reference)))

(defn- any-list->vector-if-neeeded
  [m]
  (update-in m [:any]
             #(if (instance? clojure.lang.Associative %)
                %
                (vector %))))



(defn- add-new-resource-instance-state
  [m]
  {:pre [(:target-resource m) (:new-state m)]}
  (assoc-in m [:target-resource :any 0 :instance-state] (:new-state m)))


(defn update-resource-instance-state
  [m]
  {:pre [(:target-resource m) (:new-state m)]}
  (assoc-in m [:target-resource :any 0 :instance-state] (:new-state m)))

(defn update-resource-instance-location
  [m]
  {:pre [(:target-resource m) (:new-instance-location m)]}
  (assoc-in m [:target-resource :any 0 :instance-location] (:new-instance-location m)))

(defn get-process-state
  [p]
  (read-string (get-in p [:instance-descriptor :instance-state])))


(defn get-resource-instance-descriptor
  [rm]
  (get-in rm [:any 0]))

(defn get-resource-state
  [rm]
  (:instance-state (get-resource-instance-descriptor rm)))


(defn update-resource-instance-descriptor
  [m]
  {:pre [(:new-instance-descriptor m)]}
  (assoc-in m [:target-resource :any 0] (:new-instance-descriptor m)))

(defn- resource->resource-instance
  [m]
  {:pre [(or (= :node-template (:entity-type m)))]}
  (if (= :node-template (:entity-type m))
    (into m {:any [(instance-descriptor-element {:id (:id m)
                                                 :instance-state :initializable})]})
    m))


(defn resources->resource-instances
  [m]
  {:pre [(:entity-data m)]}
  (debug (:entity-data m))
  (->> m
       get-resources
       (mapv resource->resource-instance)
       (assoc m :new-vals)
       replace-resources-or-relationships-of-informal-process))

(defn relationship->relationship-instances
  [m]
  {:pre [(get-in m [:entity-data :entity-map :resources])
         (vector? (get-in m [:entity-data :entity-map :resources]))]}
  (update-in m [:entity-data :entity-map :relationships] #(mapv resource->resource-instance %)))


(defn update-informal-process-resources-state
  [m]
  (->> m
       get-resources
       (mapv #(-> m
                  (assoc :target-resource %)
                  update-resource-instance-state
                  :target-resource))
       (assoc m :new-vals)
       replace-resources-or-relationships-of-informal-process))


(defn update-informal-process-instance-state
  [m]
  {:pre [(:new-state m) (:entity-data m)]}
  (assoc-in m [:entity-data :instance-descriptor :instance-state] (:new-state m)))


(defn add-main-intention
  [m]
  {:pre [(:entity-data m)]}
  (assoc m :target-intention (:target-intention (:entity-data m))))

(defn- create-artifact-uri-reference
  [m]
  {:pre [(:uri m)
         (:type m)
         (:access-protocol-prefix m)
         (:deployable-path m)
         (:artifact-reference-uri-path m)]}
  (if (.startsWith (:uri m) (:access-protocol-prefix m))
    (str (if (.endsWith (str (:uri m)) "/")
           (str (:uri m))
           (str (str (:uri m)) "/"))
         (:deployable-path m)
         (conv/winery-encode (:artifact-reference-uri-path m)))
    (str (:access-protocol-prefix m)
         (if (.endsWith (str (:uri m)) "/")
           (str (:uri m))
           (str (str (:uri m)) "/"))
         (:deployable-path m)
         (conv/winery-encode (:artifact-reference-uri-path m)))))

(defn- create-artifact-references
  [m]
  [(artifact-reference
    {:reference (create-artifact-uri-reference m)})])

(defn create-artifact-template
  [m]
  {:pre [(:artifact-template-name m)
         (:artifact-type m)
         (:artifact-template-id m)]}
  (artifact-template
   {:id (:artifact-template-id m)
    :name (:artifact-template-name m)
    :type (:type m)
    :artifact-references {:artifact-reference (create-artifact-references m)}}))

(defn add-artifact-references
  [m]
  {:pre [(:artifact-template m)]}
  (debug "Adding artifact references")
  (->> m
       :artifact-template
       :artifact-references
       :artifact-reference
       (assoc m :artifact-references)))


(defn- add-artifact-templates
  [m]
  (->> m
       :node-types
       (mapv #(-> m
                  (assoc :node-type %)

                  create-artifact-template))
       (assoc m :artifact-templates)))


(defn add-artifact-template
  [m]
  {:post [(:artifact-template %)]}
  (assoc m :artifact-template (create-artifact-template m)))



(def jaxb-mappings
  {:instance-descriptor-type "de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor"
   :instance-descriptor-element "de.uni_stuttgart.iaas.ipsm.v0.InstanceDescriptor"
   :content "de.uni_stuttgart.iaas.ipsm.v0.TContent"
   :definitions "org.oasis_open.docs.tosca.ns._2011._12.TDefinitions"
   :capability-type "org.oasis_open.docs.tosca.ns._2011._12.TCapabilityType"
   :capability "org.oasis_open.docs.tosca.ns._2011._12.TCapability"
   :definitions-element "org.oasis_open.docs.tosca.ns._2011._12.Definitions"
   :definition "de.uni_stuttgart.iaas.ipsm.v0.TDefinition"
   :context "de.uni_stuttgart.iaas.ipsm.v0.TContext"
   :intention "de.uni_stuttgart.iaas.ipsm.v0.TIntention"
   :informal-process-definition "de.uni_stuttgart.iaas.ipsm.v0.TInformalProcessDefinition"
   :informal-process-instance "de.uni_stuttgart.iaas.ipsm.v0.TInformalProcessInstance"
   :resource-model "de.uni_stuttgart.iaas.ipsm.v0.TResourceModel"
   :resource-model-instance "de.uni_stuttgart.iaas.ipsm.v0.TResourceModelInstance"
   :contexts "de.uni_stuttgart.iaas.ipsm.v0.TContexts"
   :process-definition "de.uni_stuttgart.iaas.ipsm.v0.TProcessDefinition"
   :service-template "org.oasis_open.docs.tosca.ns._2011._12.TServiceTemplate"
   :topology-template "org.oasis_open.docs.tosca.ns._2011._12.TTopologyTemplate"
   :node-template "org.oasis_open.docs.tosca.ns._2011._12.TNodeTemplate"
   :node-type-implementation "org.oasis_open.docs.tosca.ns._2011._12.TNodeTypeImplementation"
   :implementation-artifact "org.oasis_open.docs.tosca.ns._2011._12.TImplementationArtifact"
   :artifact-template "org.oasis_open.docs.tosca.ns._2011._12.TArtifactTemplate"
   :artifact-reference "org.oasis_open.docs.tosca.ns._2011._12.TArtifactReference"
   :artifact-references "org.oasis_open.docs.tosca.ns._2011._12.TArtifactTemplate$ArtifactReferences"
   :deployment-artifact "org.oasis_open.docs.tosca.ns._2011._12.TDeploymentArtifact"
   :interface "org.oasis_open.docs.tosca.ns._2011._12.TInterface"
   :artifact-type "org.oasis_open.docs.tosca.ns._2011._12.TArtifactType"
   :operation "org.oasis_open.docs.tosca.ns._2011._12.TOperation"
   :input-parameters "org.oasis_open.docs.tosca.ns._2011._12.TOperation$InputParameters"
   :output-parameters "org.oasis_open.docs.tosca.ns._2011._12.TOperation$OutputParameters"
   :operation-messsage "de.uni_stuttgart.iaas.ipsm.v0.TOperationMessage"
   :parameter "org.oasis_open.docs.tosca.ns._2011._12.TParameter"
   :import "org.oasis_open.docs.tosca.ns._2011._12.TImport"
   :relationship-type "org.oasis_open.docs.tosca.ns._2011._12.TRelationshipType"
   :node-type "org.oasis_open.docs.tosca.ns._2011._12.TNodeType"})
