(ns de.uni-stuttgart.iaas.ipsm.utils.schema-access
  (:import [java.net URI]
           [de.uni_stuttgart.iaas.ipsm.v0 TInitializableEntityDefinition TInteractiveInitializableEntityDefinition TInteractiveEntityDefinition TInstanceDescriptor TIdentifiableEntityDefinition InstanceDescriptor TEntityIdentity])
  (:gen-class
   :name uni_stuttgart.iaas.ipsm.utils.SchemaAccess
   :methods [^:static [getNameOfEntityIdentity [de.uni_stuttgart.iaas.ipsm.v0.TEntityIdentity] String]
             ^:static [getTargetNamespaceOfEntityIdentity [de.uni_stuttgart.iaas.ipsm.v0.TEntityIdentity] String]
             ^:static [getTargetNameOfInitializableEntityDefinition [de.uni_stuttgart.iaas.ipsm.v0.TInitializableEntityDefinition] String]
             ^:static [getTargetNamespaceOfInitializableEntityDefinition [de.uni_stuttgart.iaas.ipsm.v0.TInitializableEntityDefinition] String]
             ^:static [getNameOfIdentifiableEntityDefinition [de.uni_stuttgart.iaas.ipsm.v0.TIdentifiableEntityDefinition] String]
             ^:static [getTargetNamespaceOfIdentifiableEntityDefinition [de.uni_stuttgart.iaas.ipsm.v0.TIdentifiableEntityDefinition] String]
             ^:static [getTargetNamespaceOfInstanceDescriptor [de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor] String]
             ^:static [getNameOfInstanceDescriptor [de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor] String]
             ^:static [areEntityIdentitiesEqual [de.uni_stuttgart.iaas.ipsm.v0.TEntityIdentity
                                                 de.uni_stuttgart.iaas.ipsm.v0.TEntityIdentity]
                       Boolean]
             ^:static [areInstanceDescriptorsEqual [de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor
                                                    de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor]
                       Boolean]
             ^:static [doInstanceDescriptorsRepresentSameExternalEntity [de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor
                                                                         de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor]
                       Boolean]]))


(defn get-target-namespace-of-entity-identity
  [ei]
  (.getTargetNamespace ei))

(defn get-name-of-entity-identity
  [ei]
  (.getName ei))

(defn -getNameOfEntityIdentity
  [ei]
  (get-name-of-entity-identity ei))

(defn -getTargetNamespaceOfEntityIdentity
  [ei]
  (get-target-namespace-of-entity-identity ei))

(defn get-name-of-identifiable-entity-definition
  [ied]
  (-> ied
      .getEntityIdentity
      get-name-of-entity-identity))

(defn get-target-namespace-of-identifiable-entity-definition
  [ied]
  (-> ied
      .getEntityIdentity
      get-target-namespace-of-entity-identity))

(defn -getNameOfIdentifiableEntityDefinition
  [ied]
  (-> ied
      get-name-of-identifiable-entity-definition))

(defn -getTargetNamespaceOfIdentifiableEntityDefinition
  [ied]
  (-> ied
      get-target-namespace-of-identifiable-entity-definition))


(defn get-name-of-initializable-entity-definition
  [ied]
  (-> ied
      .getIdentifiableEntityDefinition
      get-name-of-identifiable-entity-definition))


(defn get-target-namespace-of-initializable-entity-definition
  [ied]
  (-> ied
      .getIdentifiableEntityDefinition
      get-target-namespace-of-identifiable-entity-definition))

(defn -getNameOfInitializableEntityDefinition
  [ied]
  (-> ied
      get-name-of-initializable-entity-definition))


(defn -getTargetNamespaceOfInitializableEntityDefinition
  [ied]
  (-> ied
      get-target-namespace-of-initializable-entity-definition))


(defn get-name-of-instance-descriptor
  [id]
  (-> id
      .getEntityIdentity
      get-name-of-entity-identity))

(defn get-target-namespace-of-instance-descriptor
  [id]
  (-> id
      .getEntityIdentity
      get-target-namespace-of-entity-identity))

(defn -getNameOfInstanceDescriptor
  [id]
  (-> id
      get-name-of-instance-descriptor))


(defn -getTargetnamespaceOfInstanceDescriptor
  [id]
  (-> id
      get-target-namespace-of-instance-descriptor))


(defn entity-identities-equal?
  "Id based comparison"
  [id1 id2]
  (and (= (get-target-namespace-of-entity-identity id1)
          (get-target-namespace-of-entity-identity id2))
       (= (get-name-of-entity-identity id1)
          (get-name-of-entity-identity id2))))

(defn -areEntityIdentitiesEqual
  "Id based comparison"
  [id1 id2]
  (entity-identities-equal? id1 id2))


(defn identifiable-entitiy-definitions-equal?
  "Id based comparison"
  [ied1 ied2]
  (and (= (get-target-namespace-of-identifiable-entity-definition ied1)
          (get-target-namespace-of-identifiable-entity-definition ied2))
       (= (get-name-of-identifiable-entity-definition ied1)
          (get-name-of-identifiable-entity-definition ied2))))

(defn -areIdentifiableEntityDefinitionsEqual
  "Id based comparison"
  [ied1 ied2]
  (identifiable-entitiy-definitions-equal? ied1 ied2))

(defn instace-descriptors-equal?
  "Id based comparison"
  [id1 id2]
  (and (= (get-target-namespace-of-instance-descriptor id1)
          (get-target-namespace-of-instance-descriptor id2))
       (= (get-name-of-instance-descriptor id1)
          (get-name-of-instance-descriptor id2))))

(defn instances-represent-same-external-entity?
  "Compare instance uris"
  [id1 id2]
  (= (.getInstanceURI id1)
     (.getInstanceURI id2)))

(defn -doInstanceDescriptorsRepresentSameExternalEntity
  "Compare instance uris"
  [id1 id2]
  (instances-represent-same-external-entity? id1 id2))

(defn -areInstanceDescriptorsEqual
  "Id based comparison"
  [id1 id2]
  (instace-descriptors-equal? id1 id2))
