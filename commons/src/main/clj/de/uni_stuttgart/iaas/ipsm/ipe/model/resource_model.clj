(ns de.uni-stuttgart.iaas.ipsm.ipe.model.resource-model
  (:require [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.transformations :as transformations]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as c]
            [loom.graph :as graph]
            [loom.alg :as alg]
            [taoensso.timbre :as timbre]))

(timbre/refer-timbre)

(defn- create-maps-for-resource-model
  "Takes a map in the form of {:entity-data {:source-str source-str}}
  and converts model into a set of relationship and resource maps"
  [m]
  {:pre [(:entity-data m) (:source-str m)]
   :post [(:entity-data %) (:resource-instance (:entity-data %)) (:relationship-definition (:entity-data %))]}
  (update-in [:entity-data] #(transformations/get-relationship-and-node-templates-for-ipe-process-definition (:source-str m))))


(defn- filter-relationships
  [m]
  {:pre [(:relationships m)
         (:id (:source-node m))]}
  (update-in m [:relationships]
             (fn [relationships]
               (filterv
                #(some #{(:id (:source-node m))}
                       (:ref (:source-element %)))
                relationships))))


(defn- filter-resources
  [m]
  {:pre [(:relationships m)
         (:resources m)
         (:id (:source-node m))]}
  (->> m
       :relationships
       (mapv (fn [relationship]
               (->> m
                    :resources
                    (filterv #(some #{(:id %)}
                                    (:ref (:target-element relationship))))
                    (mapv #(vector (:source-node m) % relationship)))))
       (assoc-in m [:resources])))



(defn- add-nodes-to-graph
  [m]
  {:pre [(:resources m) (:source-node m) (:node-vector m) (vector? (:node-vector m))]}
  ;(debug "Created node vector is:" (:node-vector m))
  (update-in m [:graph] #(if (= 1 (count (:node-vector m)))
                           (apply graph/add-nodes % (:node-vector m))
                           (apply graph/add-edges % (:node-vector m)))))

(defn- create-node-vectors
  [m]
  {:pre [(:resources m) (:source-node m)]}
  ;; (debug "Node vectors will be created from the followig resource map" (:resources m) "for the following resource" (:source-node m))
  (assoc m :node-vector (if (seq (:resources m))
                          (mapv
                           #(hash-map (:source-node m) %)
                           (:resources m))
                          [(:source-node m)])))

(defn- add-node-to-graph
  [node m g]
  {:pre [(:graph-data m)
         (:resource-instance (:graph-data m))
         (:relationship-definition (:graph-data m))
         (satisfies? graph/WeightedGraph g)]}
  (-> {:source-node node
       :graph g
       :relationships (:relationship-definition (:graph-data m))
       :resources (:resource-instance (:graph-data m))}
      filter-relationships
      filter-resources
      create-node-vectors
      add-nodes-to-graph
      :graph))

(defn- create-graph
  [m]
  {:pre [(:graph-data m)
         (:resource-instance (:graph-data m))
         (:relationship-definition (:graph-data m))]}
  (let [node-count (count (:resource-instance (:graph-data m)))]
    (loop [resource-index 0
           g (graph/weighted-digraph)]
      (if (< resource-index node-count)
       (recur (inc resource-index)
              (add-node-to-graph (get (:resource-instance (:graph-data m)) resource-index) m g))
       g))))



(defn add-graph-for-informal-process
  [m]
  {:pre [(= :informal-process-instance (:entity-type (:entity-data m)))]
   :post [(:resource-graph (:graph-data %))]}
  (as-> m x
    (assoc-in x [:graph-data :relationship-definition] (or (domain-language/get-relationships m) []))
    (assoc-in x [:graph-data :resource-instance] (or (domain-language/get-resources m) []))
    (create-graph x)
    (assoc-in m [:graph-data :resource-graph] x)))


(defn- add-new-to-be-removed-resource
  [m]
  (assoc m :new-resource (domain-language/update-resource-instance-state (:target-resource m))))



(defn- get-graph
  [m]
  (-> m
      :graph-data
      :resource-graph))


;;
;; define resource-graph
;; for each resource
;;    add resource into resource-graph
;; for each relationship
;;    add relationship into resource-graph
;;
;; resource-list = top-sort(resource-graph)
;; resource-list = reverse(resource-list)
;; for-each resource in resource-list
;;   incoming-links = resource-graph.incoming-links(resource)
;;   for-each link



(defn remove-resource
  [resource-graph resource]
  (graph/remove-nodes resource-graph resource))


(defn resource-should-be-acquired?
  [resource]
  (and (= (:entity-type resource) :node-template)
       (= :initializable (:instance-state (:instance-descriptor (first (:any resource)))))))



(defn clean-up-acquirement-list
  [resources]
  (remove
   resource-should-be-acquired?
   resources))

(defn resource-model->acquirement-vector
  [m]
  {:pre [(get-graph m)]}
  ;; (debug "Acquirement vector will be returned for the following resource model:" (:resource-model m))
  (-> m
      get-graph
      alg/topsort
      clean-up-acquirement-list
      reverse
      vec))


(defn add-acquirement-vector
  "Creates an in-memory resource graph using resource model at hand"
  [m]
  {:pre [(:entity-data m)]}
  (debug "Acquirement vector will be added" m)
  (assoc-in m [:graph-data :acquirement-vector]
            (resource-model->acquirement-vector m)))



(defn add-dependency-resources
  [m]
  {:pre [(get-graph m) (:target-resource m)]}
  (assoc m :dependencies (or (graph/successors (get-graph m) (:target-resource m)) [])))

(defn add-dependent-resources
  [m]
  {:pre [(get-graph m) (:target-resource m)]}
  (assoc m :dependents (or (graph/predecessors (get-graph m) (:target-resource m)) [])))

(defn add-outbound-relationships
  [m]
  {:pre [(get-graph m) (:dependencies m) (:target-resource m)]}
  (assoc m :outbound-relationships
         (or
          (mapv
           #(graph/weight (get-graph m) (:target-resource m) %)
           (:dependencies m))
          [])))


(defn add-preconditioner-relationships
  [m]
  {:pre [(:outbound-relationships m) (:inbound-relationships m)]}
  (assoc m :preconditioner-relationship-definitions
         (or
          (filterv
           #(= :preconditioner-relationship-definition (:entity-type %))
           (into (:outbound-relationships m) (:inbound-relationships m)))
          [])))

(defn add-postconditioner-relationships
  [m]
  {:pre [(:outbound-relationships m) (:inbound-relationships m)]}
  (assoc m :postconditioner-relationship-definitions
         (or
          (filterv
           #(or (= :postconditioner-relationship-definition (:entity-type %))
                (= :instantiable-relationship-definition (:entity-type %)))
           (into (:outbound-relationships m) (:inbound-relationships m)))
          [])))


(defn add-inbound-relationships
  [m]
  {:pre [(get-graph m) (:dependencies m) (:target-resource m)]}
  (assoc m :inbound-relationships
         (or
          (mapv
           #(graph/weight (get-graph m) % (:target-resource m))
           (:dependents m))
          [])))

(defn- add-new-outbound-resource-relationship-pairs
  [m]
  {:pre [(get-graph m) (:dependencies m) (:target-resource m) (:new-resource m)]}
  ;; (debug "adding resource relationship pairs" m)
  (assoc m :outbound-resource-relationship-pairs
         (or
          (mapv
           #(apply vector (:new-resource m)
                   [% (graph/weight (get-graph m) (:target-resource m) %)])
           (:dependencies m))
          [])))

(defn- add-new-inbound-resource-relationship-pairs
  [m]
  {:pre [(get-graph m) (:dependencies m) (:target-resource m) (:new-resource m)]}
  ;; (debug "adding resource relationship pairs" m)
  (assoc m :inbound-resource-relationship-pairs
         (or
          (mapv
           #(apply vector % [(:new-resource m) (graph/weight (get-graph m) % (:target-resource m))])
           (:dependents m))
          [])))

(defn- remove-old-resource
  [m]
  {:pre [(get-graph m) (:old-resource m)]}
  (update-in m [:graph-data :resource-graph]
             #(graph/remove-nodes % (:old-resource m))))

(defn- add-new-resource-with-relationships
  [m]
  {:pre [(get-graph m) (:outbound-resource-relationship-pairs m) (:new-resource m) (:inbound-resource-relationship-pairs m)]}
  ;; (debug "Add new resource with relationships " m)
  (-> m
      (update-in [:graph-data :resource-graph]
                 #(graph/add-nodes % (:new-resource m)))
      (update-in [:graph-data :resource-graph]
                 #(apply graph/add-edges %
                         (concat (:inbound-resource-relationship-pairs m)
                                 (:outbound-resource-relationship-pairs m))))))


(defn- update-resource-graph
  [m]
  {:pre [(:target-resource m) (:new-resource m)]}
  (-> m
      (assoc :old-resource (:target-resource m))
      add-dependency-resources
      add-dependent-resources
      add-new-outbound-resource-relationship-pairs
      add-new-inbound-resource-relationship-pairs
      remove-old-resource
      add-new-resource-with-relationships))

(defn update-target-resource-in-graph
  [m]
  (->> m
       update-resource-graph
       :graph-data
       :resource-graph
       (assoc-in m [:graph-data :resource-graph])))

(declare mark-dependencies-of-to-be-removed-resource)

(defn- mark-dependency-recursively
  [m]
  {:pre [(:target-resource m)]}
  (->> m
       domain-language/update-resource-instance-state
       :target-resource
       (assoc m :new-resource)
       update-target-resource-in-graph
       mark-dependencies-of-to-be-removed-resource))



(defn- mark-dependencies-of-to-be-removed-resource
  [m]
  {:pre [(:to-be-removed-resource m)
         (:graph-data m)
         (:resource-graph (:graph-data m))]}
  (let [sucessors (graph/successors (:resource-graph (:graph-data m)) (:to-be-removed-resource m))
        limit (count sucessors)]
    (loop [index 0
           entity-map m]
      (if (< index limit)
        (recur
         (inc index)
         (-> entity-map
             (assoc :target-resource (get sucessors index))
             (assoc :to-be-removed-resource (:target-resource m) :new-state :to-be-removed)
             mark-dependency-recursively))
        entity-map))))



(defn- mark-dependencies-of-to-be-removed-resources
  [m]
  {:pre [(:to-be-removed-resources m)
         (:graph-data m)
         (:resource-graph (:graph-data m))]}
  (let [limit (count (:to-be-removed-resources m))]
    (loop [index 0
           resource-graph (:resource-graph (:graph-data m))]
      (if (< index limit)
        (recur (inc index)
               (-> m
                   (assoc :to-be-removed-resource (get (:to-be-removed-resources m) index))
                   mark-dependencies-of-to-be-removed-resource
                   (dissoc :to-be-removed-resource)
                   :graph-data
                   :resource-graph))
        (assoc-in m [:graph-data :resource-graph] resource-graph)))))



(defn mark-additional-to-be-removed-resources
  "All dependent resources on the removed resources must be also
  removed, this step marks all dependent resources as to be removed"
  [m]
  {:pre [(:graph-data m) (:resource-graph (:graph-data m))]}
  (debug "Marking additional to-be removed resources")
  (dissoc (->> m
               :graph-data
               :resource-graph
               graph/nodes
               (filterv #(or (= (-> %
                                    domain-language/get-resource-state) ":to-be-removed")
                             (= (-> %
                                    domain-language/get-resource-state) :to-be-removed)))
               (assoc m :to-be-removed-resources)
               mark-dependencies-of-to-be-removed-resources)
          :to-be-removed-resources))




(defn get-target-from-resource-id
  [m]
  {:pre [(get-graph m) (:target-resource-id m)]}
  (->> m
       get-graph
       alg/post-traverse
       (filterv
        #(= (:id %) (:target-resource-id m)))
       first))

;; (def simple-toplogy
;;   (graph/weighted-digraph {{:name :team-leader} {:redmine :manages}}
;;                           {{:name :developer} {{:name :team-leader} :is-a-friend}}
;;                           {{:name :developer} {{:name :mediawiki} :uses}}))

(defn resource-graph->resources-relationships-map
  [m]
  {:pre [(get-graph m)]}
  (debug "Converting resource graph to nodes and relationships vectors")
  {:resources (into [] (graph/nodes (get-graph m)))
   :relationships (into [] (graph/edges (get-graph m)))})
