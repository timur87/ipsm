(ns de.uni-stuttgart.iaas.ipsm.ipe.model.schema
  (:require [clojure.spec :as sc])
  #?(:clj (:import [java.net URI])))



;; Single entities
(sc/def ::entity-type keyword?)
(sc/def ::name string?)
(sc/def ::target-namespace string?)
(sc/def ::definition-language #?(:clj #(instance? URI  %)
                                 :cljs string?))
(sc/def ::value string?)
(sc/def ::element string?)
(sc/def ::type string?)
(sc/def ::required string?)
(sc/def ::start-time string?)
(sc/def ::end-time string?)
(sc/def ::instance-state string?)
(sc/def ::instance-uri string?)
(sc/def ::source-model string?)
(sc/def ::type-ref string?)
(sc/def ::parent-instance string?)
(sc/def ::id string?)
(sc/def ::abstract boolean?)
(sc/def ::final boolean?)
(sc/def ::priority (sc/and int? #(<= 10 %) #(>= 0 %)))
(sc/def ::due string?)
(sc/def ::participant-ref string?)
(sc/def ::participant-type string?)
(sc/def ::process-id string?)
(sc/def ::operation-type string?)
(sc/def ::preconditioner-relationship (sc/coll-of ::relationship-template))
(sc/def ::preconditioner-relationships (sc/keys :req-un [::preconditioner-relationship]))
(sc/def ::dependency (sc/coll-of ::node-template))
(sc/def ::dependencies (sc/keys :req-un [::dependency]))

(sc/def ::abstract #{"yes" "no"})
(sc/def ::final #{"yes" "no"})
(sc/def ::required #{"yes" "no"})
(sc/def ::derived-from string?)
(sc/def ::properties-definition (sc/keys :opt-un [::element ::type]))
(sc/def ::tag (sc/coll-of (sc/keys :opt-un [::name ::value])))
(sc/def ::tags (sc/keys :req-un [::tag]))
(sc/def ::pattern string?)
(sc/def ::reference string?)

(sc/def ::include (sc/keys :req-un [::pattern]))
(sc/def ::exclude (sc/keys :req-un [::pattern]))

(sc/def ::extensible-elements (sc/keys :opt-un [::any
                                                ::documentation
                                                ::import
                                                ::other-attributes]))

(sc/def ::parameter (sc/merge ::extensible-elements (sc/keys :req-un [::name
                                                                   ::type
                                                                   ::required])))

(sc/def ::input-parameter (sc/coll-of ::parameter))
(sc/def ::output-parameter (sc/coll-of ::parameter))

(sc/def ::input-parameters (sc/keys :req-un [::input-parameter
                                          ::entity-type]))

(sc/def ::output-parameters (sc/keys :req-un [::output-parameter
                                           ::entity-type]))

(sc/def ::single-operation (sc/merge ::extensible-elements
                              (sc/keys :opt-un [::input-parameters
                                                ::output-parameters]
                                       :req-un [::name])))

(sc/def ::operation (sc/coll-of (sc/merge ::extensible-elements
                                          (sc/keys :opt-un [::input-parameters
                                                            ::output-parameters]
                                                   :req-un [::name]))))

(sc/def ::interface (sc/keys :opt-un [::operation
                                      ::name]
                             :req-un [::entity-type]))

(sc/def ::interfaces (sc/keys :req-un [::interface]))
(sc/def ::source-interfaces (sc/keys :req-un [::interface]))
(sc/def ::target-interfaces (sc/keys :req-un [::interface]))

(sc/def ::tosca-entity-type (sc/merge ::extensible-elements (sc/keys :req-un [::name
                                                                           ::target-namespace]
                                                                     :opt-un [::tags
                                                                           ::abstract
                                                                           ::final
                                                                           ::properties-definition
                                                                           ::derived-from])))

(sc/def ::node-type (sc/merge ::tosca-entity-type
                              (sc/keys :opt-un [::req-unuirement-definitions
                                                ::capability-definitions
                                                ::instance-states
                                                ::interfaces]
                                       :req-un [::entity-type])))

(sc/def ::valid-source (sc/coll-of ::type-ref))
(sc/def ::valid-target (sc/coll-of ::type-ref))

(sc/def ::relationship-type (sc/merge ::tosca-entity-type
                                      :opt-un [::valid-target
                                            ::valid-source
                                            ::instance-states
                                            ::capability-definitions
                                            ::source-interfaces
                                            ::target-interfaces]
                                      :req-un [::entity-type]))

(sc/def ::tosca-artifact-type (sc/merge ::tosca-entity-type (sc/keys :req-un [::entity-type])))

(sc/def ::artifact-reference (sc/keys :req-un [::reference ::entity-type]
                                      ::opt-un [::include ::exclude]))

(sc/def ::tosca-entity-template (sc/merge
                                 ::extensible-elements
                                 (sc/keys :req-un [::id
                                               ::type
                                               ::entity-type]
                                         :opt-un [::properties
                                               ::property-constraints])))



(sc/def ::min-instances number?)
(sc/def ::max-instances string?)

(sc/def ::source-element (sc/keys :ref [::ref]))
(sc/def ::target-element (sc/keys :ref [::ref]))

(sc/def ::node-template (sc/merge ::tosca-entity-template
                                  (sc/keys :opt-un [::requirements
                                       ::capabilities
                                       ::policies
                                       ::deployment-artifacts
                                       ::name
                                       ::min-instances
                                       ::max-instances]
                                           :req-un [::entity-type])))

(sc/def ::relationship-template (sc/merge ::tosca-entity-template
                                          (sc/keys :opt-un [::relationship-constraints
                                                         ::capabilities
                                                         ::policies
                                                         ::deployment-artifacts
                                                         ::name
                                                         ::min-instances
                                                         ::max-instances]
                                                   :req-un [::entity-type
                                                         ::source-element
                                                         ::target-element])))

(sc/def ::single-deployment-artifact (sc/keys :req-un [::name
                                                       ::artifact-type
                                                       ::artifact-ref
                                                       ::entity-type]))

(sc/def ::deployment-artifact (sc/coll-of ::single-deployment-artifact))

(sc/def ::deployment-artifacts (sc/keys :req-un [::deployment-artifact]))


(sc/def ::single-implementation-artifact (sc/keys :req-un [::interface-name
                                                           ::operation-name
                                                           ::artifact-type
                                                           ::artifact-ref
                                                           ::entity-type]))

(sc/def ::implementation-artifact (sc/coll-of ::single-implementation-artifact))

(sc/def ::implementation-artifacts (sc/keys :req-un [::implementation-artificat]))

(sc/def ::node-type-implementation (sc/keys :req-un [::name
                                                  ::target-namespace
                                                  ::entity-type]
                                            :opt-un [::abstract
                                                  ::final
                                                  ::tags
                                                  ::derived-from
                                                  ::required-container-features
                                                  ::implementation-artifacts
                                                  ::deployment-artifacts]))

(sc/def ::operation-coverage (sc/keys :opt-un [::name
                                            :str/interface
                                            ::operation-target]))

(sc/def ::artifact-template ::tosca-entity-template)

(sc/def ::execution-environment-integrator (sc/keys :req-un [::id
                                                         ::name
                                                         ::resource-specific-type-groups
                                                         ::domain-specific-type-groups
                                                         ::uri
                                                         ::entity-type]
                                                   ::opt-un [::artifact-type
                                                             ::artifact-templates]))




(sc/def ::target-resource ::node-template)

(sc/def ::operation-message (sc/keys :req-un [::process-id
                                           ::operation-type
                                           ::target-resource
                                           ::entity-type]
                                     ::opt-un [::dependencies
                                            ::informal-process-definition
                                            ::preconditioner-relationships
                                            ::instance-state
                                            ::instance-location
                                            ::error-definition]))

(sc/def ::imports (sc/coll-of (sc/keys :req-un [::namespace
                                             ::location
                                             ::imported-type])))

(sc/def :dm/types (sc/coll-of ::node-type
                              ::relationship-type
                              ::node-type-implementation
                              ::artifact-type
                              ::artifact-template))

(sc/def ::domain-manager-message (sc/keys :req-un [:dm/types]
                                          :opt-un [::imports
                                                ::target-namespace
                                                ::id]))


(sc/def ::entity-identity (sc/keys :req-un [::name
                                         ::target-namespace]))
(sc/def ::entity-definition (sc/keys :req-un [::definition-content
                                           ::definition-language]
                                     :opt-un [::properties]))
(sc/def ::entity-definitions (sc/coll-of ::entity-definition))
(sc/def ::identifiable-entity-definition (sc/keys :opt-un [::entity-identity
                                                        ::entity-definitions]))
(sc/def ::instance-descriptor (sc/keys :req-un [::instance-state
                                             ::source-model]
                                       :opt-un [::start-time
                                             ::end-time
                                             ::instance-uri
                                             ::parent-instance]))
(sc/def ::instance-descriptors (sc/coll-of ::instance-descriptor))
(sc/def ::initializable-entity-definition (sc/keys :req-un [::identifiable-entity-definition]
                                                   :opt-un [::instance-descriptors]))
(sc/def ::participant (sc/keys :req-un [::participant-ref]
                               :opt-un [::participant-type]))
(sc/def ::participant-list (sc/coll-of ::participant))
(sc/def ::interactive-entity-definition (sc/keys :req-un [::identifiable-entity-definition]
                                                 :opt-un [::participant-list]))
(sc/def ::interactive-initializable-entity-definition (sc/keys :req-un [::initializable-entity-definition]
                                                               :opt-un [::participant-list]))
(sc/def ::intention-definition (sc/keys :req-un [::interactive-entity-definition]
                                        :opt-un [::related-intentions
                                              ::achieving-strategies
                                              ::initial-contexts
                                              ::final-contexts
                                              ::priority
                                                 ::due]))
(sc/def ::organizational-capability (sc/coll-of string?))
(sc/def ::organizational-capabilities (sc/keys :req-un [::organizational-capability]))

(sc/def ::required-intention (sc/coll-of string?))
(sc/def ::required-intentions (sc/keys :req-un [::organizational-capability]))

(sc/def ::operational-process (sc/coll-of string?))
(sc/def ::operational-processes (sc/keys :req-un [::organizational-capability]))

(sc/def ::strategy-definition (sc/keys :req-un [::interactive-initializable-entity-definition]
                                       :opt-un [::required-intentions
                                                ::organizational-capabilities
                                                ::operational-processes]))

(sc/def ::strategy-definitions (sc/coll-of ::strategy-definition))


(sc/def ::context-definition
  (sc/merge ::interactive-entity-definition (sc/keys :opt-un-un [::contained-contexts])))

(sc/def ::context-definitions (sc/coll-of ::context-definition))

;; (sc/def ::capability-type number?)
(sc/def ::providing-resources (sc/coll-of string?))
(sc/def ::desired-resources (sc/coll-of string?))
(sc/def ::capability-definition
  (sc/merge ::interactive-entity-definition
           (sc/keys :opt-un [
                            ::capability-type
                            ::providing-resources
                            ::desired-resources])))
(sc/def ::capability-definitions (sc/map-of keyword? ::capability-definition))


(sc/def ::informal-process-definition
  (sc/merge ::interactive-entity-definition
           (sc/keys :opt-un [::target-intention
                            ::required-intentions
                            ::organizational-capabilities
                            ::resource-model])))

(sc/def ::informal-process-definitions (sc/map-of keyword? ::informal-process-definition))

(sc/def ::instance-descriptor
  (sc/merge ::identifiable-entity-definition
           (sc/keys :opt-un [::start-time
                            ::end-time
                            ::instance-state
                            ::instance-uri
                            ::source-model
                            ::parent-instance])))
