(ns de.uni-stuttgart.iaas.ipsm.ipe.model.tosca
  (:require [taoensso.timbre :as timbre]))

(timbre/refer-timbre)



(defn add-node-and-relationships-of-service-template
  "Return service template using given id and :target-namespace from
  definitions. If no :id and :target-namespace is present, return the
  first. Assumption is :entity-type of each service-template
  is :service-template, and this applies to the other
  sub-entities. Definitions is passed using :entity-data key."
  [m]
  (->> m
       :entity-data
       :topology-template
       :node-template-or-relationship-template
       (assoc m :entity-data)))


(defn get-matching-service-template
  "Return the first service template that has the given
  {:target-namespace}:id. If these keys are not specified, return the
  first one. Definitions map is stored in {:entity-data}"
  [m]
  {:pre [(:entity-data m)]}
  (debug "Getting matching service template")
  (if (and (:target-namespace m) (:id m))
    (->> m
         :entity-data
         :definitions
         :service-template-or-node-type-or-node-type-implementation
         (filterv #(= :service-template (:entity-type %)))
         (filterv #(= (:id m) (:id %)))
         (filterv #(= (:target-namespace m) (:target-namespace %)))
         first
         (assoc m :entity-data))
    (->> m
         :entity-data
         :definitions
         :service-template-or-node-type-or-node-type-implementation
         (filterv #(= :service-template (:entity-type %)))
         first
         (assoc m :entity-data))))

(defn get-relationsip-and-node-templates-of-topology-template
  [m]
  {:pre [(:entity-data m) (= :service-template (:entity-type (:entity-data m)))]}
  (->> m
       :entity-data
       :node-template-or-relationship-template
       (assoc m :entity-data)))

(defn get-relationsip-and-node-templates-of-service-template
  [m]
  {:pre [(:entity-data m) (= :service-template (:entity-type (:entity-data m)))]}
  (->> m
       :entity-data
       :topology-template
       :node-template-or-relationship-template
       (assoc m :entity-data)))


(defn- match?
  [m]
  {:pre [(:match-criteria m) (:current-val m) (:new-val m)]}
  (debug "Is it a match?")
  (->> m
       :match-criteria
       (mapv #(and (get (:current-val m) %)
                   (get (:new-val m) %)
                   (= (get (:current-val m) %)
                      (get (:new-val m) %))))
       (reduce #(and %1 %2))))


(defn- replace-if-matching
  [m]
  {:pre [(:match-criteria m) (:new-vals m) (:current-val m)]}
  (or (->> m
           :new-vals
           (filterv #(match? (assoc m :new-val %)))
           first)
      (:current-val m)))


(defn- add-if-matching
  [m]
  {:pre [(:match-criteria m) (:new-vals m) (:current-val m)]}
  (->> m
       :new-vals
       (mapv #(if (match? (assoc m :new-val %))
                %
                (:current-val m)))))

(defn update-node-templates-of-service-template
  [m]
  {:pre [(:entity-data m)]}
  (update-in m
             [:entity-data :topology-template :node-template-or-relationship-template]
             #(mapv (fn [t] (replace-if-matching (assoc m :current-val t))) %)))


(defn update-service-template-of-informal-process-instance
  [m]
  (update-in m
             [:entity-data
              :resource-model-instance
              :definitions
              :service-template-or-node-type-or-node-type-implementation]
             #(mapv (fn [t] (replace-if-matching (assoc m :current-val t))) %)))


(defn add-matching-type
  [m]
  {:pre [(:definitions m)
         (= :definitions (:entity-type (:definitions m)))
         (:name m)
         (:target-namespace m)]}
  (assoc m :node-type (->> m
                           :definitions
                           :service-template-or-node-type-or-node-type-implementation
                           (filterv #(and (= (:target-namespace m) (:target-namespace %))
                                          (= (:name m) (:name %))))
                           first)))

(defn add-matching-implementation
  [m]
  {:pre [(:definitions m)
         (= :definitions (:entity-type (:definitions m)))
         (:type m )]
   :post [(:implementation %)]}
  (assoc m :implementation (->> m
                                :definitions
                                :service-template-or-node-type-or-node-type-implementation
                                (filterv #(or (= (:type m) (:node-type %))
                                              (= (:type m) (:relationship-type %))))
                                first)))

(defn add-type->name-and-target-namespace
  [m]
  {:pre [(:type m)]}
  (-> m
      (assoc :name (.getLocalPart (javax.xml.namespace.QName/valueOf (:type m))))
      (assoc :target-namespace (.getNamespaceURI (javax.xml.namespace.QName/valueOf (:type m))))))

(defn add-type-of-a-template
  [m]
  {:pre [(:target-template m)]}
  (when-not (:type (:target-template m)) (error "No type is defined!!"))
  (-> m
      (assoc :type (:type (:target-template m)))
      add-type->name-and-target-namespace
      add-matching-type))

(defn add-implementation-artifacts-of-implementation
  [m]
  {:pre [(:implementation m)]
   :post [(:implementation-artifacts %)]}
  (->> m
       :implementation
       :implementation-artifacts
       :implementation-artifact
       (assoc m :implementation-artifacts)))


(defn add-matching-artifact-type
  [m]
  {:pre [(:target-interface m)]}
  (assoc m :artifact-type
         (some->> m
                  :implementation-artifacts
                  (filterv #(and (= (:target-interface m) (:interface-name %))
                                 (= (:operation-type m) (:operation-name %))))
                  first
                  :artifact-type)))

(defn add-matching-artifact-template-ref
  [m]
  {:pre [(:target-interface m)]}
  (assoc m :artifact-ref
         (some->> m
                  :implementation-artifacts
                  (filterv #(and (= (:target-interface m) (:interface-name %))
                                 (= (:operation-type m) (:operation-name %))))
                  first
                  :artifact-ref)))

(defn add-matching-artifact-template-from-ref-and-type
  [m]
  {:pre [(:artifact-ref m)
         (:artifact-type m)]}
  (debug "Adding matching artifact templates" (:artifact-ref m) (:artifact-type m))
  (assoc m :artifact-template
         (some->> m
                  :definitions
                  :service-template-or-node-type-or-node-type-implementation
                  (filterv #(= :artifact-template (:entity-type %)))
                  (filterv #(do
                              (debug (:id %)
                                     (-> m
                                         :artifact-ref
                                         javax.xml.namespace.QName/valueOf
                                         .getLocalPart)
                                     (:artifact-type m)
                                     (:type %)
                                     (and (= (-> m
                                                 :artifact-ref
                                                 javax.xml.namespace.QName/valueOf
                                                 .getLocalPart)
                                             (:id %))
                                          (= (:artifact-type m) (:type %))))
                              (and (= (-> m
                                        :artifact-ref
                                        javax.xml.namespace.QName/valueOf
                                        .getLocalPart)
                                    (:id %))
                                 (= (:artifact-type m) (:type %)))))
                  first)))

(defn add-type-implementation-of-template
  [m]
  {:pre [(:target-template m)]}
  (when-not (:type (:target-template m)) (error "No type is defined!!"))
  (-> m
      (assoc :type (:type (:target-template m)))
      add-matching-implementation))

(defn add-interfaces-of-node-type
  [m]
  {:pre [(:node-type m)]}
  (debug "Adding interfaces of a nodetype")
  (some->> m
           :node-type
           :interfaces
           :interface
           (assoc m :interfaces)))


(defn add-interface-names
  [m]
  {:pre [(:interfaces m)]}
  (assoc m (->> m
                :interfaces
                (mapv :name))))

(defn add-node-type-implementation-of-node-template
  [m]
  {:pre [(:node-template m) (:definitions m)]}
  (debug "Addin node type implementations")
  (assoc m :node-type-implementation
         (some->> m
                  :definitions
                  :service-template-or-node-type-or-node-type-implementation
                  (filterv #(= :node-type-implementation (:entity-type %)))
                  (filterv #(= (->> m :node-template :type) (:node-type %)))
                  first)))


(defn add-deployment-artifact-based-on-type-local-name
  "Takes the first one when more than one exists"
  [m]
  {:pre [(:type-name m) (:node-type-implementation m)]}
  (assoc m :deployment-artifact
         (some->> m
                  :node-type-implementation
                  :deployment-artifacts
                  :deployment-artifact
                  (filterv #(-> %
                                :artifact-type
                                javax.xml.namespace.QName/valueOf
                                .getLocalPart
                                (= (:type-name m))))
                  first)))

(defn add-artifact-ref-from-deployment-artifact
  "Takes the first one when more than one exists"
  [m]
  {:pre [(:deployment-artifact m)]}
  (assoc m :artifact-ref
         (some->> m
                  :deployment-artifact
                  :artifact-ref)))

(defn add-artifact-type-from-deployment-artifact
  "Takes the first one when more than one exists"
  [m]
  {:pre [(:deployment-artifact m)]}
  (debug "Adding deployment artifact")
  (assoc m :artifact-type
         (some->> m
                  :deployment-artifact
                  :artifact-type)))


(defn add-artifact-template-based-id
  "Takes the first one when more than one exists"
  [m]
  (debug "Adding artifact template")
  {:pre [(:definitions m) (:deployment-artifact m)]}
  (->> m
       :definitions
       :service-template-or-node-type-or-node-type-implementation
       (filterv #(= :artifact-template (:entity-type %)))
       (filterv #(->> %
                      :id
                      (= (-> m
                             :deployment-artifact
                             :artifact-ref))))
       first
       (assoc m :artifact-template)))

(defn add-artifact-references
  [m]
  {:pre [(:artifact-template m)]}
  (debug "Adding artifact references")
  (->> m
       :artifact-template
       :artifact-references
       :artifact-reference
       (assoc m :artifact-references)))
