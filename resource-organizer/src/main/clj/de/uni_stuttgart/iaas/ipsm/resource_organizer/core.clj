(ns de.uni-stuttgart.iaas.ipsm.resource-organizer.core
  (:require [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.transformations :as transformations]
            [de.uni-stuttgart.iaas.ipsm.utils.communications :as communications]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protocols]
            [overtone.at-at :as at]
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                          logf tracef debugf infof warnf errorf fatalf reportf
                          spy logged-future with-log-level with-logging-config
                          sometimes)]))


(def access-protocol-prefix  "http://")


(defn- poll-resource-providers!
  [m]
  {:pre [(:polling-period m) (:handler m) (:polling-pool m)]}
  (debug "Polling will be started! Period:" (:polling-period m))
  (debug "Polling pool:" (:polling-pool m))
  ((:handler m) m)
  (at/every (:polling-period m) #((:handler m) m) (:polling-pool m))
  nil)

(defn resource-list-handler
  [ch {:keys [content-type delivery-tag type] :as meta} ^bytes payload]
  (debug (format "[consumer] Received a message: %s, delivery tag: %d, content type: %s, type: %s" ;
                 (String. payload "UTF-8") delivery-tag content-type type)))



(defn persist-entites!
  [m]
  {:pre [(:persistence m) (:entity-data m)]}
  (debug "Removing entities")
  ;; removing all types
  (protocols/delete-entity-by-id! (:persistence m) (:entity-data m))
  (debug "Persisting entities")
  (protocols/add-entity! (:persistence m) (:entity-data m)))

(defn tosca-persist-entites!
  [m]
  {:pre [(:tosca-persistence m) (:entity-data m)]}
  ;; (debug "Removing entities")
  ;; removing all types
  ;; (protocols/delete-tosca-entities! (:tosca-persistence m) (assoc m :entity-types [:artifact-type :node-type :relationship-type :artifact-template]))
  (debug "Persisting entities")
  (protocols/add-domain-manager-data! (:tosca-persistence m) (:entity-data m)))


(defn generate-identifiable-entity
  [m resource]
  (debug "Source map in generate-identifiable-resource is")
  (debug "Adding ids to the resource" resource)
  (debug "Resource-id" (:type (:entity-map resource)))
  (let [domain-uri (:domain-uri m)
        source-name (:source-name (:message-payload m))
        resource-type (:type (:entity-map resource))]
    (debug "Resource type is" resource-type)
    (debug "Generated id is" (conversions/create-unique-id-from-str (str domain-uri resource-type)))
    (if (and domain-uri resource-type)
      (assoc resource
             :id (conversions/create-unique-id-from-str (str domain-uri resource-type))
             :source-name source-name)
      (do
        (error "Resource id or domain uri is null")
        (error "Domain uri" domain-uri)
        (error "Resource type" resource-type)))))

(defn create-unique-ids
  [m]
  (debug "Creating unique ids ")
  (mapv
   #(generate-identifiable-entity m %)
   (:entity-data m)))

(defn update-entity-data-with-unique-ids
  [m]
  (debug "Updating map ids")
  (assoc m :entity-data (create-unique-ids m)))

(defn- extract-dsls
  [m]
  (debug "Extracting DSLs")
  (if (= :xml (:response-format (:message-payload m)))
    (assoc m :entity-data {:entity-map (transformations/get-types-from-definitions-xml (:response (:message-payload m)))})
    (assoc m :entity-data (:response (:message-payload m)))))

(defn- add-domain-manager-id
  [m]
  (debug "Domain manager uri will be added" (:domain-uri (:message-payload m)))
  (assoc m :domain-uri (:domain-uri (:message-payload m))))


(defn- add-definitions-entity-type
  [m]
  {:pre [(:entity-data m)]}
  (debug "Adding definitions entity type for dm data")
  (assoc-in m [:entity-data :entity-type] :dm-data))

(defn- add-entity-id
  [m]
  {:pre [(:domain-uri m)]}
  (debug "Adding entity id" (:domain-uri m))
  (assoc-in m [:entity-data :id] (conversions/create-unique-id-from-str (:domain-uri m))))

(defn- add-available-types-and-implementations-from-tosca-repo
  "Adds available types from TOSCA persistence"
  [m]
  {:pre [(:tosca-persistence m)]
   :post [(:tosca-entities %)]}
  (debug "Add available types and implementations from tosca reposiory")
  (assoc m :tosca-entities
         (protocols/get-tosca-entities (:tosca-persistence m)
                                       {:entity-types [:node-type
                                                       :relationship-type
                                                       :node-type-implementation
                                                       :relationship-type-implementation]})))

;; TODO duplicates should be probably removed!!
(defn- update-tosca-type
  [m]
  {:pre [(:group-type m)
         (:tosca-entity m)
         (:group-val m)]}
  (if (:target-entity? m)
    (debug "Update tosca type will be executed for" (:tosca-entity m) (:group-val m) (:group-type m)))
  (if (:target-entity? m)
    (assoc m :tosca-entity
           (cond-> m
             true :tosca-entity

             (and (= :node-type (:entity-type (:tosca-entity m)))
                  (:supported-interfaces m))
             (update-in [:interfaces :interface]
                        #(if %
                           (into % (:supported-interfaces m))
                           (into [] (:supported-interfaces m))))

             (and (= :relationship-type (:entity-type (:tosca-entity m)))
                  (:supported-source-interfaces m))
             (update-in [:source-interfaces :interface]
                        #(if %
                           (into % (:supported-source-interfaces m))
                           (into [] (:supported-source-interfaces m))))

             (and (= :relationship-type (:entity-type (:tosca-entity m)))
                  (:supported-target-interfaces m))
             (update-in [:target-interfaces :interface]
                        #(if %
                           (into % (:supported-target-interfaces m))
                           (into [] (:supported-target-interfaces m))))))
    m))

(defn- add-target-entity?
  [m]
  "Returns if it is a target type of an interface based on the group type"
  {:pre [(:group-type m)
         (:tosca-entity m)
         (:group-val m)
         (:allowed-entity-type-set m)]}
  (debug "Returns if it is a target type of an interface based on the group type")
  (cond ;; if domain specific type just check group type
    (:domain-specific-type? m)
    (assoc m :target-entity? (and (= (:group-type m) (:target-namespace (:tosca-entity m)))
                                  ((:allowed-entity-type-set m) (:entity-type (:tosca-entity m)))))
    :else ;; otherwise check both local parts and target namespace
    (let [group-type (javax.xml.namespace.QName/valueOf (:group-type m))
          group-type-name (.getLocalPart group-type)
          group-type-ts (.getNamespaceURI group-type)]
      (assoc m :target-entity? (and (= group-type-ts (:target-namespace (:tosca-entity m)))
                                  (= group-type-name (:name (:tosca-entity m)))
                                  ((:allowed-entity-type-set m) (:entity-type (:tosca-entity m))))))))


(defn- add-group-type
  [m]
  {:pre [(or (:type-group m)
             (and (:type-groups m)(:index m)))]
   :post [(:group-type %)]}
  (debug "Adding group type")
  (cond-> m
    (:index m) (assoc :group-type (conversions/winery-decode (name (first (get (vec (->> m
                                                                                         :type-groups))
                                                                               (:index m))))))
    (not (:index m)) (assoc :group-type (conversions/winery-decode (name (first (:type-group m)))))))



(defn- add-group-val
  [m]
  {:pre [(or (:type-group m)
             (and (:type-groups m)(:index m)))]
   :post [(:group-val %)]}
  (debug "Adding group val")
  (cond->  m
    (:index m) (assoc :group-val (second (get (vec (->> m
                                                        :type-groups))
                                              (:index m))))
    (not (:index m)) (assoc :group-val (second (:type-group m)))))
;;; These MUST be corrected and recursion error must be corrected too
(defn- add-supported-interfaces
  [m]
  {:pre [(:group-val m)]}
  (debug "Addingg supported interfaces")
  (if (:target-entity? m)
    (some->> m
         :group-val
         vals
         (mapv :interface)
         (assoc m :supported-interfaces))
    m))

(defn- add-supported-source-interfaces
  [m]
  {:pre [(:group-val m)]}
  (debug "Addingg source supported interfaces")
  (if (:target-entity? m)
    (some->> m
             :group-val
             vals
             (mapv :source-interface)
             (assoc m :supported-source-interfaces))
    m))

(defn- add-supported-target-interfaces
  [m]
  {:pre [(:group-val m)]}
  (debug "Addingg target supported interfaces")
  (if (:target-entity? m)
    (some->> m
         :group-val
         vals
         (mapv :target-interface)
         (assoc m :supported-target-interfaces))
    m))


(defn- update-tosca-entity-types-with-new-interfaces
  [m]
  {:pre [(:tosca-entity m)
         (:type-groups m)]
   :post [(:tosca-entity m)]}
  (debug "Updatin tosca types with interfaces of eei data")
  (if (or (= (:entity-type (:tosca-entity m)) :node-type)
          (= (:entity-type (:tosca-entity m)) :relationship-type))
    (let [type-group-count (count (:type-groups m))]
      (loop [index 0
             updated-type (:tosca-entity m)]
        (if (< index type-group-count)
          (recur (inc index)
                 (-> m
                     (assoc :tosca-entity updated-type)
                     (assoc :index index)
                     add-group-type
                     add-group-val
                     (assoc :allowed-entity-type-set #{:node-type :relationship-type})
                     add-target-entity?
                     add-supported-interfaces
                     add-supported-target-interfaces
                     add-supported-source-interfaces
                     update-tosca-type
                     :tosca-entity))
          (assoc m :tosca-entity updated-type))))
    m))

(defn- add-temp-list-without-implementations
  [m]
  {:pre [(:tosca-entities m)]}
  (debug "Adding temporary list without implementations")
  (->> m
       :tosca-entities
       (filterv #(and (not= (:entity-type %) :node-type-implementation)
                      (not= (:entity-type %) :relationship-type-implementation)))
       (assoc m :temp-list)))

(defn- add-implementations
  [m]
  {:pre [(:implementations m)]}
  (debug "Adding implementations")
  (->> m
       :tosca-entities
       (filterv #(or (= (:entity-type %) :node-type-implementation)
                     (= (:entity-type %) :relationship-type-implementation)))

       (assoc m :implementations)))


(defn- add-artifact-template-name
  [m]
  {:pre [(:tosca-entity m)]}
  (debug "Adding artifact-template name " (str (:name (:tosca-entity m)) "-operation-endpoint"))
  (assoc m :artifact-template-name (str (:name (:tosca-entity m)) "-operation-endpoint")))

(defn- add-artifact-template-id
  [m]
  {:pre [(:artifact-template-name m)]}
  (debug "Adding artifact-template id " (conversions/create-unique-id-from-str (:artifact-template-name m)))
  (assoc m :artifact-template-id (conversions/create-unique-id-from-str (:artifact-template-name m))))

(defn- add-artifact-template-type
  [m]
  {:pre [(:artifact-type m)]}
  (assoc m :type (str (javax.xml.namespace.QName. (:target-namespace (:artifact-type m))
                                                  (:name (:artifact-type m))))))
(defn- add-type-groups
  [m]
  {:pre [(or (:domain-specific-type-groups m) (:resource-specific-type-groups m))]
   :post [(:type-groups %)]}
  (cond-> m
    (and (:domain-specific-type-groups m)
         (not (empty? (:domain-specific-type-groups m)))) (update-in [:type-groups] #(if %
                                                                                       (into % (:domain-specific-type-groups m))
                                                                                       (:domain-specific-type-groups m)))
    (and (:domain-specific-type-groups m)
         (not (empty? (:domain-specific-type-groups m)))) (assoc :domain-specific-type? true)
    (and (:resource-specific-type-groups m)
         (not (empty? (:resource-specific-type-groups m)))) (update-in [:type-groups] #(if %
                                                                                         (into % (:resource-specific-type-groups m))
                                                                                         (:resource-specific-type-groups m)))
    (and (:resource-specific-type-groups m)
         (not (empty? (:resource-specific-type-groups m)))) (assoc :domain-specific-type? false)))



(defn- add-target-implementation-of-type-groups?
  "Checks if an implementation is targeted by a resource or domain
  specific group in an eei data"
  [m]
  {:pre [(or (:resource-specific-type-groups m)
             (:domain-specific-type-groups m))]}
  (assoc m :target-entity? (or (some->> m
                                      :resource-specific-type-groups
                                      (mapv #(-> m
                                                 (assoc :type-group %)
                                                 add-group-type
                                                 add-type-groups
                                                 add-group-val
                                                 (assoc :domain-specific-type? false)
                                                 (assoc :allowed-entity-type-set #{:node-type-implementation :relationship-type-implementation})
                                                 add-target-entity?
                                                 :target-entity?))
                                      seq
                                      (reduce #(or %1 %2)))
                             (some->> m
                                      :domain-specific-type-groups
                                      (mapv #(-> m
                                                 (assoc :type-group %)
                                                 add-group-type
                                                 add-group-val
                                                 add-type-groups
                                                 (assoc :allowed-entity-type-set #{:node-type-implementation :relationship-type-implementation})
                                                 (assoc :domain-specific-type? true)
                                                 add-target-entity?
                                                 :target-entity?))
                                      seq
                                      (reduce #(or %1 %2))))))

(defn- append-or-create-target-implementations
  [m v]
  {:post [(:target-implementations %)]}
  (update-in m [:target-implementations] #(if % (into % v) v)))



(defn- add-target-implementations
  [m]
  "Append or create type implementations targeted by eei message"
  {:pre [(:tosca-entities m)]}
  (debug "Adding target implementations")
  (->> m
       :tosca-entities
       (filterv #(or (= (:entity-type %) :node-type-implementation)
                     (= (:entity-type %) :relationship-type-implementation)))
       (filterv #(-> m
                     (assoc :tosca-entity %)
                     add-target-implementation-of-type-groups?
                     :target-entity?))
       (append-or-create-target-implementations m)))


(defn- add-implementation-artifacts-using-operations
  [m]
  {:pre [(:interface m)
         (:artifact-type m)
         (:artifact-template m)]}
  (debug "Adding implementation artifacts using operations")
  (->> m
       :interface
       :operation
       (mapv #(domain-language/implementation-artifact
               {:interface-name (:name (:interface m))
                :operation-name (:name %)
                :artifact-type (str (javax.xml.namespace.QName. (:target-namespace (:artifact-type m))
                                                                (:name (:artifact-type m))))
                :artifact-ref (str (javax.xml.namespace.QName. (:target-namespace (:artifact-type m))
                                                               (:id (:artifact-template m))))}))
       (assoc m :implementation-artifacts)))

(defn- add-implementation-artifacts-of-interfaces
  [m]
  {:pre [(:interfaces m)]}
  (debug "Adding implementation artifacts of interfaces")
  (->> m
       :interfaces
       (mapv #(-> (assoc m :interface %)
                  add-implementation-artifacts-using-operations
                  :implementation-artifacts))
       flatten
       vec
       (assoc m :implementation-artifacts)))


(defn add-cumilative-interfaces
  "Aggregates all supported interfaces of the node / relationship type
  implementation under :interfaces keyword."
  [m]
  {:pre [(or (:supported-source-interfaces m)
             (:supported-target-interfaces m)
             (:supported-interfaces m))]
   :post [(:interfaces %)]}
  (debug "Adding cummilated interfaces")
  (->> (-> m
           (assoc :interfaces [])
           (update-in [:interfaces] into (:supported-source-interfaces m))
           (update-in [:interfaces] into (:supported-target-interfaces m))
           (update-in [:interfaces] into (:supported-interfaces m)))
       :interfaces
       (filterv identity)
       (assoc m :interfaces)))



(defn- update-existing-intities-by-removing-duplicates
  "Check if an entitiy in the list and if yes removes it"
  [m]
  {:pre [(:new-entities m)
         (:existing-entities m)
         (:compare-cretiria1 m)
         (:compare-cretiria2 m)]}
  (debug "Check if an entitiy in the list and if yes removes it...")
  (->> m
       :existing-entities
       (filterv #(->> m
                      :new-entities
                      (mapv (fn [new-entitiy] (and (= ((:compare-cretiria1 m) new-entitiy)
                                                      ((:compare-cretiria1 m) %))
                                                   (= ((:compare-cretiria2 m) new-entitiy)
                                                      ((:compare-cretiria2 m) %)))))
                      (reduce (fn [v1 v2] (or v1 v2)))))
       (assoc m :existing-entities)))

(defn- add-new-entities-into-existing-entities
  "Adds new entities into the list of existing entities"
  [m]
  {:pre [(:new-entities m) (:existing-entities m)]}
  (debug "Adds new entities into the list of existing entities...")
  (update-in m [:existing-entities] #(into % (:new-entities m))))

(defn- add-new-entitites-to-the-list
  "Adds new entitites to the list first checks if an item is already
  there if yes the item is removed"
  [m]
  {:pre [(:new-entities m)]}
  (debug "Adds new entitites to the list first checks if an item is already
  there if yes the item is removed...")
  (if (:existing-entities m)
    (-> m
        update-existing-intities-by-removing-duplicates
        add-new-entities-into-existing-entities)
    (assoc m :existing-entities (:new-entities m))))

(defn- prepare-final-list-of-implementation-types
  "Updates the list of implementation artifacts using the old list and
  by removing the duplicates"
  [m]
  (debug "Updates the list of implementation artifacts using the old list and
  by removing the duplicates...")
  {:pre [(:implementation-artifacts m)]}
  (assoc m :implementation-artifacts (-> m
                                         (assoc :new-entities (:implementation-artifacts m))
                                         (assoc :existing-entities (get-in m [:tosca-entity :implementation-artifacts :implementation-artifact]))
                                         (assoc :compare-cretiria1 :interface-name)
                                         (assoc :compare-cretiria2 :operation-name)
                                         add-new-entitites-to-the-list
                                         :existing-entities)))

(defn- add-implementation-artifacts-into-implementation-type
  "Add implementation artifacts under node type implementation"
  [m]
  {:pre [(:implementation-artifacts m)]}
  (debug "Add implementation artifacts under node type implementation...")
  (-> m
      prepare-final-list-of-implementation-types
      (assoc-in [:tosca-entity :implementation-artifacts :implementation-artifact] (:implementation-artifacts m))))

(defn- add-implementation-artifacts
  [m]
  (debug "Adding implementation artifacts")
  (if-not (:target-entity? m)
    m
    (-> m
        add-supported-interfaces
        add-supported-source-interfaces
        add-supported-target-interfaces
        add-cumilative-interfaces
        add-implementation-artifacts-of-interfaces
        add-implementation-artifacts-into-implementation-type)))

(defn- update-implementation
  [m]
  (debug "Updating implementation" )
  (let [type-limit (count (:type-groups m))]
    (loop [index 0
           node-type-imp (:tosca-entity m)]
      (if (< index type-limit)
        (recur (inc index)
               (-> m
                   (assoc :index index)
                   add-group-type
                   add-group-val
                   (assoc :tosca-entity node-type-imp)
                   (assoc :allowed-entity-type-set #{:node-type-implementation :relationship-type-implementation})
                   add-target-entity?
                   add-implementation-artifacts
                   :tosca-entity))
        (assoc m :tosca-entity node-type-imp)))))

(defn- merge-with-artifact-template
  [m]
  {:pre [(:artifact-template m) (:tosca-entity m)]}
  (assoc m :types [(:artifact-template m) (:tosca-entity m)]))

(defn- add-artifact-reference-uri-path
  [m]
  (assoc m :artifact-reference-uri-path (or (:node-type (:tosca-entity m))
                                            (:relationship-type (:tosca-entity m)))))

(defn- update-implementations
  [m]
  {:post [(:types %)]}
  (debug "Updating implementations")
  (->> m
       :target-implementations
       (mapv #(->> (assoc m :tosca-entity %)
                   add-artifact-template-name
                   add-artifact-template-id
                   add-artifact-template-type
                   add-artifact-reference-uri-path
                   domain-language/add-artifact-template
                   add-type-groups
                   update-implementation
                   merge-with-artifact-template
                   :types))
       flatten
       vec
       (assoc m :types)))

(defn- add-remaining-entities
  "Store all other tosca entities than target implementations in a
  separate keyword. A target implementation is a node or relationship
  type implementation targeted by a eei"
  [m]
  {:pre [(:target-implementations m) (:tosca-entities m)]
   :post [(:remaining-entities %)]}
  (debug "Adding remaning entities")
  (->> m
       :tosca-entities
       (remove #(some->> m
                         :target-implementations
                         (mapv (fn [imp] (= % imp)))
                         seq
                         (reduce (fn [v1 v2] (or v1 v2)))))

       (into [])
       (assoc m :remaining-entities)))

(defn- merge-remainings-and-updated-implementations
  [m]
  (debug "Merge remaning entities with updated entiteis")
  (->> m
       :remaining-entities
       (into [])
       (into (:types m))
       (assoc m :updated-entites)))

(defn- update-types-based-on-new-interfaces
  "Updates implementation definitions based on new interface data"
  [m]
  {:pre [(:tosca-entities m)
         (or (:resource-specific-type-groups m) (:domain-specific-type-groups m))]}
  (debug "Updates implementation definitions based on new interface data...")
  (->> m
       :tosca-entities
       (mapv #(-> m
                  (assoc :tosca-entity %)
                  add-type-groups
                  update-tosca-entity-types-with-new-interfaces
                 :tosca-entity))
       (assoc m :tosca-entities)
       add-target-implementations
       add-remaining-entities
       update-implementations
       merge-remainings-and-updated-implementations
       :updated-entites
       (assoc m :tosca-entities)))

(defn- add-both-type-groups
  [m]
  (debug "Adding both type groups")
  (-> m
      (assoc :domain-specific-type-groups (get-in m [:eei-data :domain-specific-type-groups]))
      (assoc :resource-specific-type-groups (get-in m [:eei-data :resource-specific-type-groups]))))

(defn- add-artifact-type
  [m]
  (assoc m :artifact-type (get-in m [:eei-data :artifact-type])))

(defn- add-uri-from-message-body
  [m]
  (assoc m :uri (get-in m [:eei-data :uri])))

(defn- add-deployable-path
  [m]
  (assoc m :deployable-path ""))

(defn- add-imports-from-message
  [m]
  (assoc m :imports (get-in m [:eei-data :imports])))

(defn- add-tosca-persistence-message
  [m]
  {:pre [(:tosca-entities m)]}
  (assoc m :entity-data {:types (:tosca-entities m)
                         :imports (:imports m)}))

(defn- persist-eei-data!
  [m]
  {:pre [(:persistence m) (:eei-data m)]}
  (protocols/add-entity! (:persistence m) (:eei-data m)))

(defn- add-eei-data
  [m]
  {:pre [(:message-payload m)]
   :post [(:eei-data %)]}
  (assoc m :eei-data (get-in m [:message-payload (get-in m [:message-payload :request-type])])))


(defn- add-artifact-types-into-tosca-entities
  [m]
  {:pre [(:tosca-entities m) (:artifact-type m)]}
  (assoc m :tosca-entities (conj (:tosca-entities m) (:artifact-type m))))


(defn- update-tosca-entities-using-eei-data
  [m]
  (debug "Updating tosca entities!!!!")
  (-> m
      add-both-type-groups
      add-artifact-type
      add-uri-from-message-body
      add-deployable-path
      update-types-based-on-new-interfaces
      add-artifact-types-into-tosca-entities))

(defn- add-available-dm-data
  [m]
  {:pre [(:persistence m)]}
  (assoc m :available-dm-data (protocols/get-all-entities (:persistence m) {:entity-type :dm-data})))

(defn- add-tosca-entities-from-dm-data
  [m]
  {:pre [(:dm-data m)]
   :post [(:tosca-entities %)]}
  (assoc m :tosca-entities (:types (:dm-data m))))

(defn- replace-dm-data-types
  [m]
  {:pre [(:dm-data m)]}
  (debug "Replacing dm data types")
  (assoc-in m [:dm-data :types] (:tosca-entities m)))

(defn- update-available-dm-data
  [m]
  {:pre [(:available-dm-data m)]}
  (debug "Updating available dm data" )
  (->> m
       :available-dm-data
       (mapv #(->> (assoc m :dm-data %)
                   add-tosca-entities-from-dm-data
                   update-tosca-entities-using-eei-data
                   replace-dm-data-types
                   :dm-data))
       (assoc m :available-dm-data)))

(defn- persist-entitities-into-tosca-and-default-persistence!
  [m]
  (doseq [dm-data (:available-dm-data m)]
    (doto (assoc m :entity-data dm-data)
      persist-entites!
      tosca-persist-entites!)))

(defn handle-execution-environment-message
  [m]
  (fn [ch {:keys [content-type delivery-tag type] :as meta} ^bytes payload]
    (debug "Message received" )
    (debug "Message payload" (String. payload "UTF-8"))
    (doto (-> m
              (communications/add-edn-message-body payload)
              add-eei-data
              add-available-dm-data
              update-available-dm-data
              add-imports-from-message)
      persist-entitities-into-tosca-and-default-persistence!
      persist-eei-data!)))




(defn- add-tosca-entities-from-dm-msg
  [m]
  (assoc m :tosca-entities (:types (:entity-data m))))

(defn- add-available-eeis
  [m]
  {:pre [(:persistence m)]}
  (assoc m :available-eeis (protocols/get-all-entities (:persistence m) {:entity-type :execution-environment-integrator})))

(defn- update-entities-iteratively
  [m]
  {:pre [(:available-eeis m)
         (:tosca-entities m)]}
  (debug "Updating entities iteratively! Meaining that a new dm message has been received")
  (let [eeis-size (count (:available-eeis m))]
    (loop [index 0
           tosca-entities (:tosca-entities m)]
      (if (< index eeis-size)
        (recur (inc index)
               (-> m
                   (assoc :eei-data (get (:available-eeis m) index))
                   update-tosca-entities-using-eei-data
                   :tosca-entities))
        (assoc m :tosca-entities tosca-entities)))))

(defn- replace-types-with-tosca-entities
  [m]
  {:pre [(:tosca-entities m) (get-in m [:entity-data :types])]}
  (debug "Replacing types and entities")
  (assoc-in m [:entity-data :types] (:tosca-entities m)))

(defn aggregate-response
  [m]
  (fn [ch {:keys [content-type delivery-tag type] :as meta} ^bytes payload]
    (debug "Message received")
    (debug "Message payload" (String. payload "UTF-8"))
    (doto (-> m
              (communications/add-edn-message-body payload)
              add-domain-manager-id
              extract-dsls
              add-definitions-entity-type
              add-entity-id
              add-tosca-entities-from-dm-msg
              add-available-eeis
              update-entities-iteratively
              replace-types-with-tosca-entities)
      persist-entites!
      tosca-persist-entites!)))



(defn init-service
  [initialization-data]
  (debug "Resource announcer service is initialized...")
  (debug "Consumer queue is prepared...")
  (doto (communications/add-connection-data initialization-data)
    communications/create-exchanges!
    communications/create-queues-and-bind-them-to-exchanges!)
  (debug "Resource announcer service is initialized!"))


(defn- resource-polling-handler
  [mi]
  {:pre [(:tosca-persistence mi)]}
  (fn [mf]
    (do
      (debug "Polling domain managers!!" mf)
      ;; TODO add resource type removal
      ;; (->> (protocols/get-all-entities (:persistence mi) {:entity-type :dm-data})
      ;;      (mapv #(protocols/delete-entity-by-id! (:persistence mi) %)))
      (communications/publish-message! mf))))

(defn- add-handler-for-polling
  [m]
  (assoc m :handler (resource-polling-handler m)))

(defn start-service
  [initialization-data]
  (info "Resource organizer service is starting")
  (debug "With the following initialization map" initialization-data)
  (-> initialization-data
      communications/add-connection-data
      communications/subscribe-to-queues!
      (assoc :queue-name (communications/get-queue-name :resource-requestor-queue))
      (assoc :request {:request-type :list-domain :correlation-id (conversions/create-unique-id)})
      add-handler-for-polling
      poll-resource-providers!)
  (info "Resource aggregator service has been started!"))


(defn stop-services
  [initialization-data]
  {:pre [(:polling-pool initialization-data)]}
  (at/stop-and-reset-pool! (:polling-pool initialization-data)))
