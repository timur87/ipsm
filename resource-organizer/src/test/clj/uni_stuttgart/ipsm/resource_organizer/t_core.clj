(ns uni-stuttgart.ipsm.resource-organizer.t-core
  (:require [de.uni-stuttgart.iaas.ipsm.resource-organizer.core :as c]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as dl]
            [uni-stuttgart.ipsm.resource-organizer.t-data :as d]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as p]))

(#'c/add-type-groups add-artifact-type)
(u/testable-privates de.uni-stuttgart.iaas.ipsm.resource-organizer.core
                     add-type-groups add-artifact-type
                     add-available-types-and-implementations-from-tosca-repo
                     update-types-based-on-new-interfaces
                     add-uri-from-message-body
                     add-access-protocol-prefix
                     add-eei-data
                     update-tosca-entities-using-eei-data
                     update-available-dm-data)


(defonce app (user/go))
(def persistence (puppetlabs.trapperkeeper.app/get-service app :PersistenceProtocol))
(def tosca-persistence (puppetlabs.trapperkeeper.app/get-service app :ToscaPersistenceProtocol))
;; (def process-model-id "8a6589a4be864fc7d041006b")


(-> {:eei-data d/eei-data
     :available-dm-data d/available-dm-data
     :persistence persistence
     :access-protocol-prefix "http://"}

    (#'c/add-tosca-entities-from-dm-data)


    ;; add-imports-from-message
    )
(print (mapv
      d/available-dm-data))

(print "WE ARE" (str (->> {:eei-data d/eei-data
                                      :available-dm-data d/available-dm-data
                                        ; :persistence persistence
                                      :access-protocol-prefix "http://"
                                      :tosca-entities d/available-dm-data}

                                     (#'c/update-tosca-entities-using-eei-data)

                                     :tosca-entities
                                     )))


(mapv
      )
(:types (first d/available-dm-data))
(print (->> {:eei-data d/eei-data
             :available-dm-data d/available-dm-data
             :persistence persistence
             :access-protocol-prefix "http://"
             :artifact-type "as"
             :target-implementations [{:entity-type :node-type-implementation, :deployment-artifacts {:deployment-artifact [{:entity-type :deployment-artifact, :artifact-ref "{http://www.iaas.uni-stuttgart.de/ipsm/domains/docker-compose/metadata-based}37fcd1da75693fecb4984efd", :artifact-type "{http://www.uni-stuttgart.de/ipsm/artifact-types/}docker-compose-ipsm-deployable", :name "ipsm-deployable"}]}, :final "yes", :abstract "no", :node-type "{http://www.iaas.uni-stuttgart.de/ipsm/domains/docker-compose/metadata-based}redmine", :target-namespace "http://www.iaas.uni-stuttgart.de/ipsm/domains/docker-compose/metadata-based", :name "redmine-node-type-implementation"}]}
            (#'c/add-both-type-groups)
            (#'c/add-type-groups)
            (#'c/update-implementations)
            ))


(fact "about eei operation extension"
      (->> {:eei-data d/eei-data
            :available-dm-data d/available-dm-data
            :deployable-path ""}
           update-available-dm-data
           :available-dm-data
           (mapv #(dl/domain-manager-data %))))
