(ns de.uni-stuttgart.iaas.ipsm.persistence.mongo
  (:require [monger.core :as mg]
            [monger.collection :as mc]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as con]
            [environ.core :refer [env]]
            [clojure.string :as s]
            [taoensso.timbre :as timbre])
  (:import [com.mongodb MongoOptions ServerAddress]
           [org.bson.types ObjectId]))

(timbre/refer-timbre)

;;(let [conn (mg/connect {:host "db.megacorp.internal" :port 7878})])
;; localhost, default port

;; try to get possible uris from environment

(defn get-db-uri
  []
  (info "Getting the URI option from the environment: " (env :mongolab-uri) " or " (env :mongolab-uri))
  (or (env :mongolab-uri) (env :mongosoup-url)))

(defn- connect
  ([] (mg/connect))
  ([host] (mg/connect {:host host}))
  ([host port] (mg/connect {:host host :port port})))

(defn- get-db
  []
  (info "Datastore connection and instance will be created...")
  (if (get-db-uri)
    (:db (mg/connect-via-uri (get-db-uri)))
    (let [host (constants/property "db-host")
          port (constants/property "db-port")
          conn (if (and host port)
                   (mg/connect host port)
                   (if host
                     (mg/connect host)
                     (mg/connect)))]
                     {:db (mg/get-db conn constants/ds-name)
                      :conn conn})))


(defn- convert-id-to-objectid
  "convert id to object id if its a string"
  [str-id]
  (if (instance? String str-id)
    (ObjectId. str-id)
    str-id))

(defn- swap-id
  "swap id if exists otherweise add one"
  [col old-key new-key]
  (if (contains? col old-key)
    (let [value (get col old-key)]
      (merge (dissoc col old-key) {new-key (convert-id-to-objectid value)}))
    (merge col {new-key (ObjectId.)})))

(defn- val-to-str
  "Converts the given val to string in a map. Val is identified by the
  given keyword key"
  [m key]
  (assoc m key (str (key m))))

(defn- swap-id-for-mongo
  [item]
 ;; (println (str ((keyword "_id") item) " " ))
  (if (or (:id item) (:_id item))
    (if (contains? item (keyword "_id"))
      (val-to-str (swap-id item (keyword "_id") (keyword constants/id-tag)) (keyword constants/id-tag))
      (swap-id item (keyword constants/id-tag) (keyword "_id")))
    nil))

(defn- run-command
  [m]
  {:pre [(:command m)
         (:db m)
         (:conn m)]}
  (-> m
      (assoc :response ((:command m) (:db m)))
      (assoc :dissconnect-res (mg/disconnect (:conn m)))
      :response) ;; execute command using db

  )


(defn map-key->encoded-map-key
  [m]
  (debug m)
  (->> m
       (mapv #(vector (cond-> (first %)
                        (keyword? (first %)) name
                        :true (s/replace #"\." "&#46;")
                        (keyword? (first %)) keyword)
                      (cond
                        (map? (second %))
                        (map-key->encoded-map-key (second %))
                        (vector? (second %))
                        (mapv (fn [e] (map-key->encoded-map-key e)) (second %))
                        :else
                        (second %))))
       (into {})))

(defn encoded-map-key->map-key
  [m]
  (debug m)
  (->> m
       (mapv #(vector (cond-> (first %)
                        (keyword? (first %)) name
                        :true (s/replace #"&#46;" ".")
                        (keyword? (first %)) keyword)
                      (cond
                        (map? (second %))
                        (encoded-map-key->map-key (second %))
                        (vector? (second %))
                        (mapv (fn [e] (encoded-map-key->map-key e)) (second %))
                        :else
                        (second %))))
       (into {})))


(defn- post-process-entity
  [m]
  (-> m
      swap-id-for-mongo
      con/entity-type-str>entity-type-keyword
      encoded-map-key->map-key))

(defn- pre-process-entity
  [m]
  (-> m
      map-key->encoded-map-key))




(defn get-all
  ([collection]
   (-> (get-db)
       (assoc :command
              #(mapv
                post-process-entity
                (mc/find-maps % collection)))
       run-command))
  ([collection specs]
   (-> (get-db)
       (assoc :command
              #(mapv
                post-process-entity
                (mc/find-maps % collection specs)))
       run-command)))


(defn get-by-id
  [collection id]
  (-> (get-db)
      (assoc :command
             #(post-process-entity
               (mc/find-map-by-id %
                                  collection
                                  (convert-id-to-objectid id))))
      run-command))

(defn add-to-collection!
  [collection item]
  (-> (get-db)
      (assoc :command
             #(-> (mc/insert-and-return %
                                        collection
                                        (pre-process-entity (swap-id-for-mongo item)))
                  post-process-entity))
      run-command))

(defn remove-obj-by-id!
  [collection oid]
  (-> (get-db)
      (assoc :command
             #(mc/remove-by-id %
                               collection
                               (convert-id-to-objectid oid)))
      run-command))

(defn update-obj-by-id!
  [collection oid item]
  (-> (get-db)
      (assoc :command
             #(mc/update-by-id %
                               collection
                               (convert-id-to-objectid oid)
                               (pre-process-entity item)))
      run-command))

