(ns de.uni-stuttgart.iaas.ipsm.persistence.core
  (:require [taoensso.timbre :as timbre]
            [de.uni-stuttgart.iaas.ipsm.persistence.mongo :as mongo]))

(timbre/refer-timbre)



(defn add-entity!
  [m]
  {:pre [(:entity-type (:entity-data m)) (:id (:entity-data m))]}
  (debug "Adding entity of the type " (:entity-type (:entity-data m)))
  (mongo/add-to-collection! (:entity-type (:entity-data m)) (:entity-data m)))

(defn get-entity
  [m]
  {:pre [(:entity-type (:entity-data m)) (:id (:entity-data m))]}
  (debug "Getting entity of the type " (:entity-type (:entity-data m)))
  (mongo/get-by-id
   (:entity-type (:entity-data m))
   (:id (:entity-data m))))

(defn get-all-entities
  [m]
  {:pre [(:entity-type (:entity-data m))]}
  (if (seq (dissoc (:entity-data m) :entity-type))
    (mongo/get-all
     (:entity-type (:entity-data m)) (:entity-data m))
    (mongo/get-all
     (:entity-type (:entity-data m)))))

(defn update-entity-by-id!
  [m]
  {:pre [(:entity-type (:entity-data m)) (:id (:entity-data m))]}
  (mongo/update-obj-by-id!
     (:entity-type (:entity-data m))
     (:id (:entity-data m))
     (:entity-data m)))

(defn delete-entity-by-id!
  [m]
  {:pre [(:entity-type (:entity-data m)) (:id (:entity-data m))]}
  (debug "Removing entity " (:id (:entity-data m)))
  (debug "Execution completed!" (mongo/remove-obj-by-id!
                                 (:entity-type (:entity-data m))
                                 (:id (:entity-data m)))))
