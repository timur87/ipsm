(ns de.uni-stuttgart.iaas.ipsm.persistence.service
  (:require [taoensso.timbre :as timbre]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protocols]
            [de.uni-stuttgart.iaas.ipsm.persistence.core :as c]
            [puppetlabs.trapperkeeper.core :as trapperkeeper]))

(timbre/refer-timbre)


(trapperkeeper/defservice Persistor
  protocols/PersistenceProtocol
  []
  (init [this context]
        (info "Initializing persistence service")
        context)
  (start [this context]
         (info "Starting persistence service")
         context)
  (stop [this context]
        (info "Shutting down persistence service")
        context)
  (add-entity! [this entity]
               (c/add-entity! {:entity-data entity}))
  (get-entity [this entity] (c/get-entity {:entity-data entity}))
  (get-all-entities [this entity] (c/get-all-entities {:entity-data entity}))
  (update-entity-by-id! [this entity] (c/update-entity-by-id! {:entity-data entity}))
  (delete-entity-by-id! [this entity] (c/delete-entity-by-id! {:entity-data entity})))
