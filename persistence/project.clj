(def ks-version "1.0.0")
(def tk-version "1.5.2")

(defproject de.uni-stuttgart.iaas.ipsm/persistence "0.0.1-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0-alpha13"]
                 [com.novemberain/monger "3.1.0"]
                 [environ "1.0.0"]
                 [com.taoensso/timbre "4.7.4"]
                 [de.uni-stuttgart.iaas.ipsm/commons "0.0.2-SNAPSHOT"]
                 [org.clojure/tools.logging "0.3.1"]
                 [puppetlabs/trapperkeeper ~tk-version]
                 [de.uni-stuttgart.iaas.ipsm/protocols "0.5.3-SNAPSHOT"]]

  :profiles {:dev {:source-paths ["dev"]
                   :dependencies [[puppetlabs/trapperkeeper ~tk-version :classifier "test" :scope "test"]
                                  [expectations "2.0.9"]
                                  [org.clojure/tools.namespace "0.2.11"]]}}

  :repl-options {:init-ns user}
  :source-paths ["src/main/clj"]
  :uberjar-name "persistence.jar"
  :resource-paths ["src/main/resources"]
  :aliases {"tk" ["trampoline" "run" "--config" "dev-resources/config.conf"]}
  :main puppetlabs.trapperkeeper.main)
