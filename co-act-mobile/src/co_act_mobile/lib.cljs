(ns co-act-mobile.lib
  (:require [reagent.core :as r]
            [cemerick.url :refer [url url-encode]]))
; react-native
(defonce ReactNative (js/require "react-native"))

(defn alert [title]
  (.alert (.-Alert ReactNative) title))

(defn encode-twice [value]
  (url-encode (url-encode value)))

(defn format-url
  ([resource] (str
                "/"
                (encode-twice (:namespace resource))
                "/"
                (encode-twice (:id resource))))
  ([namespace id] (str
                    "/"
                    (encode-twice namespace)
                    "/"
                    (encode-twice id))))

(defn find-source-id
  "Find source node id in parsed xml"
  [v]
  (some #(if (= :SourceElement (:tag %))
          (-> % :attributes :ref)) v))

(defn find-target-id
  "Find target node id in parsed xml"
  [v]
  (some #(if (= :TargetElement (:tag %))
          (-> % :attributes :ref)) v))

(defn judge-color
  "Return the corresponding color for correlation coefficient"
  [v]
  (cond
    (> v 0.5) "green"
    (> v 0) "yellow"
    (> v -0.5) "orange"
    (> v -1) "red"))

;react-native-router-flux
;(defonce ReactNativeRouterFlux (js/require "react-native-router-flux"))
;(defonce Drawer (r/adapt-react-class (js/require "react-native-drawer")))
;
;(defonce Navigation (r/adapt-react-class (aget ReactNativeRouterFlux "Navigation")))
;React Class
;(defonce Scene (r/adapt-react-class (aget ReactNativeRouterFlux "Scene")))
;(defonce Router (r/adapt-react-class (aget ReactNativeRouterFlux "Router")))
;(defonce DefaultRenderer (r/adapt-react-class (aget ReactNativeRouterFlux "DefaultRenderer")))
;
;(defonce Actions (aget ReactNativeRouterFlux "Actions"))