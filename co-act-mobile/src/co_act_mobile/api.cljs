(ns co-act-mobile.api
  (:require [cljs-time.core :as t]
            [cljs-time.coerce :as tc]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [reagent.core :as r]
            [clojure.string :as string]
            [goog.string :as gs]
            [co-act-mobile.android.lib :as lib]
            [tubax.core :as tubax]
            ))

;-- Helper Function ------------------------------------------
(defn error-handler
  "Log out the error to console"
  [err]
  (.log js/console err))

;-- Basic CRUD request ------------------------------------------
(defn api-post
  "Api POST request"
  [url token body success-handler error-handler]
  (-> (js/fetch url (clj->js (cond->
                               {:method "POST"
                                :headers (cond->
                                           {"Accept" "application/json"
                                            "Content-Type" "application/json"}
                                           (some? token)
                                           (assoc "Authorization" (str "Bearer " token)))}
                               (some? body)
                               (assoc :body (js/JSON.stringify (clj->js body))))))
      (.then (fn [resp]
               (if (not (nil? resp))
                 (let [ok (.-ok resp)
                       handle-fn (fn [data]
                                   (if ok
                                     (success-handler data)
                                     (error-handler data)))]
                   (-> (.json resp)
                       (.then handle-fn))))))
      (.catch (fn [error]
                (prn error)
                (error-handler error)))))

(defn api-delete
  "Api DELETE request"
  [url token success-handler error-handler]
  (-> (js/fetch url (clj->js {:method "DELETE"
                              :headers {"Accept" "application/json"
                                        "Content-Type" "application/json"
                                        "Authorization" (str "Bearer " token)}}))
      (.then (fn [resp]
               (if (not (nil? resp))
                 (let [ok (.-ok resp)
                       handle-fn (fn [data]
                                   (if ok
                                     (success-handler data)
                                     (error-handler data)))]
                   (-> (.json resp)
                       (.then handle-fn))))))
      (.catch (fn [error]
                (prn error)
                (error-handler error)))))

(defn api-get
  "Api GET request for json files."
  [url accept success-handler error-handler]
  (println "api-get url " (str @(subscribe [:static/host]) url))
  (-> (js/fetch (str @(subscribe [:static/host]) url) (clj->js {:method "GET"
                                                                :headers {
                                                                          "Content-Type" "application/json"
                                                                          "Accept" (str "application/" accept)
                                                                          }}))
      (.then (fn [resp]
               (if (not (nil? resp))
                 (let [ok (.-ok resp)
                       handle-fn (fn [data]
                                   (if ok
                                     (success-handler data)
                                     (error-handler data)))]
                   (-> (.json resp)
                       (.then handle-fn))))))
      (.catch (fn [error]
                (error-handler error)))))

(defn api-get-url
  "Api GET request for json files."
  [url accept success-handler error-handler]
  (-> (js/fetch url (clj->js {:method "GET"
                              :headers {
                                        "Content-Type" "application/json"
                                        "Accept" (str "application/" accept)
                                        }}))
      (.then (fn [resp]
               (if (not (nil? resp))
                 (let [ok (.-ok resp)
                       handle-fn (fn [data]
                                   (if ok
                                     (success-handler data)
                                     (error-handler data)))]
                   (-> (.json resp)
                       (.then handle-fn))))))
      (.catch (fn [error]
                (error-handler error)))))

(defn xml-get
  "Api GET requst for xml files"
  [url success-handler error-handler]
  ;(println "xml-get url " (str @(subscribe [:static/host]) url))
  (-> (js/fetch (str @(subscribe [:static/host]) url) (clj->js {:method "GET"
                                                                :headers {"Content-Type" "application/json"
                                                                          "Accept" "application/xml"}}))
      (.then (fn [resp]
               (if (not (nil? resp))
                 (let [ok (.-ok resp)
                       handle-fn (fn [data]
                                   (if ok
                                     (success-handler data)
                                     (error-handler data)))]
                   (-> (.text resp)
                       (.then handle-fn))))))
      (.catch (fn [error]
                (error-handler error)))))

;-- High level function ------------------------------------------

(defn get-list
  "Get json list"
  [element callback]
  (api-get element "json"
           (fn [res]
             ;(println "get-list success with" (lib/api-vec2map (js->clj res)))
             (callback (lib/api-vec2map (js->clj res))))
           (fn [error]
             (error-handler error))))

(defn parse-nodes
  "parser the node from xml format"
  [xml]
  (let [content (:content xml)
        resource-model {:attributes {}}
        result (reduce (fn [resource-model node]
                         (cond
                           (= (:tag node) :NodeTemplate) (assoc-in resource-model [:nodes (keyword (-> node :attributes :id))]  node)
                           (= (:tag node) :RelationshipTemplate) (assoc-in resource-model [:relationships (keyword (-> node :attributes :id))]  node)
                           )
                         ) {} content)]
    result))

(defn get-bitcoin
  "Get the price of Bitcoin"
  [callback]
  (api-get-url "https://blockchain.info/ticker" "json"
           (fn [res]
             (println "get-list success with"  (str "Buy: " (aget res "USD" "buy") " Sell:" (aget res "USD" "sell")))
             (callback (str "Buy: " (aget res "USD" "buy") " Sell: " (aget res "USD" "sell"))))
           (fn [error]
             (error-handler error))
               ))


(defn get-service
  "Get a service template"
  [element callback]
  (xml-get element
           (fn [res]
             (let [result (clojure.string/replace res "tosca:" "")
                   $ (.load lib/cheerio result (clj->js {:xmlMode true}))
                   topo  ($ "ServiceTemplate")
                   topo-html (.html topo)
                   topo-vec (tubax/xml->clj topo-html)
                   topo-map (parse-nodes topo-vec)]
               (callback  (cond-> topo-map
                                  (some?(.attr topo "id")) (assoc-in [:attributes :id] (.attr topo "id"))
                                  (some?(.attr topo "name")) (assoc-in [:attributes :name] (.attr topo "name"))
                                  (some?(.attr topo "targetNamespace")) (assoc-in [:attributes :target-namespace] (.attr topo "targetNamespace"))))))
           (fn [error]
             ;(println "get-single error with")
             (error-handler error))))

(defn get-node
  "Get a node"
  [element callback]
  (xml-get element
           (fn [res]
             (let [result (clojure.string/replace res "tosca:" "")
                   $ (.load lib/cheerio result (clj->js {:xmlMode true}))
                   topo  ($ "Definitions")
                   topo-html (.html topo)
                   topo-vec (tubax/xml->clj topo-html)
                   topo-map (parse-nodes topo-vec)]
               (callback  (cond-> topo-map
                                  (some?(.attr topo "id")) (assoc-in [:attributes :id] (.attr topo "id"))
                                  (some?(.attr topo "name")) (assoc-in [:attributes :name] (.attr topo "name"))
                                  (some?(.attr topo "targetNamespace")) (assoc-in [:attributes :target-namespace] (.attr topo "targetNamespace"))))))
           (fn [error]
             (error-handler error))))

(defn get-relationship
  "Get a relationship"
  [element callback]
  (xml-get element
           (fn [res]
             (let [result (clojure.string/replace res "tosca:" "")
                   $ (.load lib/cheerio result (clj->js {:xmlMode true}))
                   topo  ($ "ServiceTemplate")
                   topo-html (.html topo)
                   topo-vec (tubax/xml->clj topo-html)
                   topo-map (parse-nodes topo-vec)]
               (callback  (cond-> topo-map
                                  (some?(.attr topo "id")) (assoc-in [:attributes :id] (.attr topo "id"))
                                  (some?(.attr topo "name")) (assoc-in [:attributes :name] (.attr topo "name"))
                                  (some?(.attr topo "targetNamespace")) (assoc-in [:attributes :target-namespace] (.attr topo "targetNamespace"))))))
           (fn [error]
             (error-handler error))))

;-- Reserve Function to interact with remote ------------------------------------------
(defn get-entity
  [group-keyword entity-id entity-map callback]
  (api-get (str (@(subscribe [:static/host])) "/v1/" (name group-keyword) "/" entity-id)
           nil
           (fn [res]
             (callback res))
           (fn [error]
             (error-handler error))))

(defn instantiate
  [group-keyword entity-id entity-map callback]
  (api-post (str (@(subscribe [:static/host])) "/v1/instantiate" (name group-keyword) "/" entity-id)
            nil
            entity-map
            (fn [res]
              (callback res))
            (fn [error]
              (error-handler error))))

(defn extract
  [group-keyword entity-id entity-map callback]
  (api-post (str (@(subscribe [:static/host])) "/v1/extract" (name group-keyword) "/" entity-id)
            nil
            entity-map
            (fn [res]
              (callback res))
            (fn [error]
              (error-handler error))))

(defn api-subscribe
  [group-keyword entity-id  callback]
  (api-get (str @(subscribe [:static/host]) "/v1/subscribe" (name group-keyword) "/" entity-id)
           nil
           (fn [res]
             (callback res))
           (fn [error]
             (error-handler error))))

(defn api-unsubscribe
  [group-keyword entity-id  callback]
  (api-get (str @(subscribe [:static/host]) "/v1/unsubscribe" (name group-keyword) "/" entity-id)
           nil
           (fn [res]
             (callback res))
           (fn [error]
             (error-handler error))))

