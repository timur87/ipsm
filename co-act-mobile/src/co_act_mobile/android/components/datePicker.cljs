(ns co-act-mobile.android.components.datePicker
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.ui :as ui]))

(def ReactNative (js/require "react-native"))
(def DatePicker (.-DatePickerAndroid ReactNative))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "row"
                     :align-items "stretch"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 3}
            :text   {:font-size (-> common :text :small)
                     :font-weight "bold"
                     :color (:icon color)
                     :flex 1}
            :selected-text {:flex 2
                            :font-size (-> common :text :small)
                            :text-align "center"}})

;spec with coll-of with 5 args is not supported yet
;(s/def ::dateCol (s/coll-of number? :kind vector? :count 3 :distinct true ))
(s/def ::dateCol (s/* number?))

(defn date-picker-component
  "date picker component"
  [props]
  {:pre [(s/valid? ::dateCol (:value props))
         (s/valid? string? (:text props))
         (s/valid? fn? (:on-change props))]}
  (let [nowMil (.now js/Date)
        now (js/Date. nowMil)
        date-vec (if (some? (:value props))
                   (:value props)
                   (vector (+ (.getYear now) 1900) (+ (.getMonth now) 1) (.getDate now)))]
    (fn [props]
      [ui/view {:style (-> style :view)}
       [ui/text {:style (-> style :text)} (:text props)]
       [ui/text {:style (-> style :selected-text)} (str (nth date-vec 2) "/"
                                                        (nth date-vec 1) "/"
                                                        (nth date-vec 0))]
       [ui/button {:outer-style {:width (-> common :icon :width)}
                   :inner-style {:color (:icon color)}
                   :on-press (fn []
                               (.then (.open DatePicker  (clj->js {:date (js/Date.
                                                                           ;year
                                                                           (nth date-vec 0)
                                                                           ;month
                                                                           (- (nth date-vec 1) 1)
                                                                           ;day must plus 1 , it maybe js/Date format
                                                                           (nth date-vec 2) )}))
                                      ;resolve function for promise
                                      #(if (= "dateSetAction" (.-action %))
                                        ((:on-change props) (vector (.-year %) (+ (.-month %) 1) (.-day %) )))))}
        [ui/m-icons {:key (str "date-range")  :name "date-range" :size (-> common :icon :size)}]]]
      )))