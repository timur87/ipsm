(ns co-act-mobile.android.components.getLocation
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.android.ui :as ui]))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "column"
                     :align-items "stretch"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 10}
            :title   {:font-size (-> common :component :small)
                      :color (:icon color)
                      :flex 3}
            :touchable {:view   {:padding 0
                                 :flex 7
                                 :flex-direction "row"
                                 :align-items "center"
                                 :justify-content "space-between"}
                        :text   {:flex 1
                                 :font-size (-> common :text :middle)}
                        :delete {:outer-style {:width (-> common :icon :width)}
                                 :inner-style {:color (:icon color) }}
                        }})

(defn get-location-component
  "A component retriving user location"
  [props]
  (let [state (if (nil? (:state props)) false (:state props))]
    (fn [props]
      )))