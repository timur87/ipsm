(ns co-act-mobile.android.components.selector
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [cljs.spec :as s]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "row"
                     :align-items "center"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 3}
            :text   {:font-size (-> common :text :small)
                     :font-weight "bold"
                     :color (:icon color)
                     :flex 1}
            :picker {:flex 2
                     :margin-left 100}
            })

(s/def ::seq-of-strings (s/* string?))
(defn selector-component
  "Value selector component"
  [props]
  {:pre [(s/valid? ::seq-of-strings (:options props))
         (s/valid? number? (:selected-value props))
         (s/valid? string? (:text props))
         (s/valid? fn? (:on-select props))]}
  (let [changed (atom false)]
    (fn [props]
      [ui/view {:style (-> style :view)}
       [ui/text  {:style (-> style :text)} (:text props)]
       [ui/picker {:style (-> style :picker)
                   :selected-value (:selected-value props)
                   :on-value-change (:on-select props)
                   :mode  "dropdown"}
        (for [item (:options props)]
          (let [index (.indexOf (:options props) item)
                color (if (s/valid? ::seq-of-strings (:options-color props))
                        (nth (:options-color props) index)
                        "black")]
            ;item style only work for ios, different style control
            ; -> https://facebook.github.io/react-native/docs/picker.html
            ^{:key item}[ui/picker-item {:label item :color color :value index}]))]])))