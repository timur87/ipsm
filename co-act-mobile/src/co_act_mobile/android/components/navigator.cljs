(ns co-act-mobile.android.components.navigator
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.android.lib :as lib]
            [co-act-mobile.android.ui :as ui]))

(def style {:container {:position "absolute"
                        :top 0
                        :left 0
                        :right 0
                        :bottom 0
                        :height 400
                        :width 400
                        :justifyContent "flex-end"
                        :alignItems "center"}
            :map {:position "absolute"
                  :top 0
                  :left 0
                  :right 0
                  :bottom 0}
            })
(defn navigator-component
  "map component for navigation"
  [props]
  (let [state (if (nil? (:state props)) false (:state props))
        location (->
                   (.geocodePosition lib/geocoder (clj->js {:lat 37.78825
                                                            :lng -122.4324}))
                   (.then #(->
                            (js->clj %)
                            (nth 0)
                            (get "formattedAddress"))))
        ]
    (fn [props]
      [ui/view {:style (-> style :container)}
       [ui/map-component  {:initialRegion{:latitude 37.78825
                                          :longitude -122.4324
                                          :latitudeDelta 0.0922
                                          :longitudeDelta 0.0421}
                           :style (-> style :map)}]]
      )))