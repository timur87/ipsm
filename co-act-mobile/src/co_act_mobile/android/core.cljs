(ns co-act-mobile.android.core
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.android.ui :as ui]
            [co-act-mobile.lib :as lib]
            [co-act-mobile.containers.index :as containers]
            [co-act-mobile.style :refer [color common]]
            [co-act-mobile.android.entity.index :as entities]
            [co-act-mobile.android.components.index :as components]))

(def router (merge {:list containers/listView
                    :pick containers/pick
                    :resources containers/resources
                    :nodes containers/node
                    :relationships containers/relationship
                    :add-node containers/add-node
                    :recommendation containers/recommendation
                    :add-relationship containers/add-relationship}
                   ;merge entity map
                   entities/entitiesMap))

(def pullDrawer (fn [_ gestureState]
                  (let [dx (aget gestureState "dx")
                        ;dy (aget gestureState "dy")
                        ;opened (subscribe [:drawer/opened])
                        ]
                    (cond
                      ;close drawer
                      (> dx 20)  (dispatch [:drawer/pull false])
                      ;open drawer
                      (< dx -20) (dispatch [:drawer/pull true])
                      ))))

(defn set-notification
  "handle notificaion logic goes here"
  []
  (.configure ui/notification (clj->js {:onRegister #(.log js/console "token is" %)
                                        :onNotification #(.log js/console "notification is" %)
                                        :popInitialNotification true
                                        :requestPermissions true})))

(defn local-notification
  "create a local notification"
  []
  (.localNotification ui/notification (clj->js {:title  "Intention Updated."
                                                :ticker "My Notification Ticker" ; (optional)
                                                :autoCancel false ; (optional) default: true
                                                :number 10
                                                :color "green"
                                                :bigText "James updated intention definition : Product Development"
                                                :subText "Achieving strategy is selected"
                                                :message "Co-Act-Mobile"})))

(defn sync-resource
  "Sync the resource list when application start"
  []
  (dispatch [:api/fetch-resources]))

(def responder (.create ui/pan-responder
                        (clj->js {:onStartShouldSetPanResponder #(= (count (aget %1 "nativeEvent" "touches")) 1)
                                  ;:onStartShouldSetPanResponderCapture #(true)
                                  ;:onMoveShouldSetPanResponder #(true)
                                  ;:onMoveShouldSetPanResponderCapture #(true)
                                  ;:onPanResponderGrant #(.log js/console "gesture start")
                                  ;:onPanResponderMove #(.log js/console "gesture moving")
                                  :onPanResponderRelease pullDrawer})))

(defn renderMain
  "Main page render regading to different tenplates"
  [props]
  (let [current (subscribe [:nav/current])]
    [ui/view {:style {:margin      0
                      :flex 1
                      :padding-left 10
                      :padding-right 10
                      :background-color (-> color :background)
                      :align-items "stretch"
                      :margin-top (+ (.-HEIGHT ui/navigation-header-comp) 5)}
              }
     [(get router (:group @current))]]))

(defn nav-title
  "Navigation title render"
  [props]
  (let [current (subscribe [:nav/current])]
    [ui/header-title {:style {:flex 1
                              :align-items "center"
                              :justify-content "center"}}
     (:title @current)]))

(defn nav-right
  "Navigation right part render: drawer toggle butotn"
  [props]
  [ui/button {:inner-style {:margin 10
                            :color (-> color :icon)}
              :outer-style {:flex 1
                            :elevation 5
                            :justify-content "center"}
              :on-press #(dispatch [:drawer/pull])
              }
   [ui/m-icons {:key "menu" :name "menu" :size 20}]])

(defn header
  "Header render for title and right part"
  [props]
  [ui/navigation-header
   (assoc
     (js->clj props)
     :render-title-component #(r/as-element (nav-title %))
     :render-right-component #(r/as-element (nav-right %)))])

(defn app-root
  "The entrance of all the application"
  []
  (set-notification)
  ;(fake-notification)
  (sync-resource)

  (let [nav (subscribe [:nav/state])
        animate-value (:close-value (subscribe [:drawer]))
        animate-final (ui/animated-value. animate-value)]
    (dispatch [:drawer/init animate-final])
    (fn []
      [ui/view (merge {:style {:flex 1
                               :flexDirection "row"
                               :flex-wrap "nowrap"}}
                      (js->clj (aget responder "panHandlers")))
       [ui/animated-view {:style {:transform [{:translateX animate-final}]
                                  :overflow "visible"
                                  :flex 1}}
        [ui/card-stack {:on-navigate      #(dispatch [:nav/pop nil])
                        :render-overlay   #(r/as-element (header %))
                        :navigation-state @nav
                        :style            {:flex 1}
                        :render-scene     #(r/as-element  (renderMain %))}]]
       [components/drawer]])))

(defn init
  "Init function runs before rendering"
  []
  (dispatch-sync [:initialize-db])
  (.registerComponent ui/app-registry "coActMobile" #(r/reactify-component app-root)))


