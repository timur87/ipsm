(ns co-act-mobile.android.entity.strategy
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.android.components.index :as components]
            [co-act-mobile.android.ui :as ui]))

(defn strategy-entity
  "Strategy defnition entity"
  []
  (fn []
    (let [strategy-map (subscribe [:nav/current-entity])
          target-intention (if (some? (:target-intention @strategy-map))
                             (subscribe [:data/find [:intention-definitions (:target-intention @strategy-map)]]))]
      [ui/view {:flex 1
                :flex-direction "column"
                :justify-content "center"}
       [ui/scroll {:margin-bottom (-> common :component :margin-bottom)
                   :showsVerticalScrollIndicator false}

        [components/small-title "Name"]
        [components/text-input {:target-property [:interactive-entity-definition
                                                  :identifiable-entity-definition
                                                  :entity-identity
                                                  :name]
                                :size :small
                                :entity @strategy-map}]

        (if (some? (:target-intention @strategy-map))
          [ui/view
           [components/small-title "Convert the strategy to informal procss"]
           [components/convert {:target-intention  @target-intention}]])

        [components/add-button {:text "Target namespace"
                                :entity @strategy-map
                                :add-name :target-namespace
                                :add-group [:interactive-entity-definition
                                            :identifiable-entity-definition
                                            :entity-identity
                                            :target-namespace]
                                :multi false
                                :require true
                                :display-property :namespace}]

        [components/entity-link {:group :namespaces
                                 :name [:interactive-entity-definition
                                        :identifiable-entity-definition
                                        :entity-identity
                                        :target-namespace]
                                 :require true
                                 :display [:namespace]
                                 :entity @strategy-map}]
        [components/line]
        [components/privilege]

        [components/line]
        [components/add-button {:text "Target intention"
                                :entity @strategy-map
                                :add-name :target-intention
                                :add-group :intention-definitions
                                ;:filter :link
                                :multi false
                                ;:display-property :namespace
                                }]

        [components/entity-link {:group :intention-definitions
                                 :name :target-intention
                                 :display [:interactive-entity-definition
                                           :identifiable-entity-definition
                                           :entity-identity
                                           :name]
                                 :entity @strategy-map}]
        [components/line]


        [components/add-button {:entity @strategy-map
                                :add-name :contained-intentions
                                :add-group :intention-definitions
                                :text "Contained intentions" }]

        [components/entity-links {:group :intention-definitions
                                  :name :contained-intentions
                                  :entity @strategy-map  }]
        [components/line]

        [components/small-title "Instances"]
        [components/instance-links {:group :strategy-instances}]
        [components/line]
        [components/add-button {:entity @strategy-map
                                :add-name :implementations
                                :add-group :process-definitions
                                :text "Operational processes"}]

        [components/entity-links {:group :informal-process-definitions
                                  :name :implementations
                                  :entity @strategy-map}]
        [components/line]]

       [components/bar
        [components/subscribe]
        [components/save]
        [components/revert]
        [components/init]]]
      )))