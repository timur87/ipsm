(ns co-act-mobile.android.entity.processDefinition
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.android.components.index :as components]
            [co-act-mobile.android.ui :as ui]))

(defn process-definition-entity
  "Informal process defnition entity"
  []
  (let [process-map (subscribe [:nav/current-entity])
        contained-resources (subscribe [:data/find-group [(:resources @process-map) :resources]])]
        ;(println "log out process definition " )
        (fn []
          [ui/view {:flex 1
                    :flex-direction "column"
                    :justify-content "center"}
           [ui/scroll {:margin-bottom (-> common :component :margin-bottom)
                       :showsVerticalScrollIndicator false}

            [components/small-title "Name"]
            [components/text-input {:target-property [:interactive-entity-definition
                                                      :identifiable-entity-definition
                                                      :entity-identity
                                                      :name]
                                    :size :small
                                    :entity @process-map}]

            [components/add-button {:text "Target namespace"
                                    :entity @process-map
                                    :add-name [:interactive-entity-definition
                                               :identifiable-entity-definition
                                               :entity-identity
                                               :target-namespace]
                                    :add-group :namespaces
                                    :require true
                                    :multi false
                                    :display-property :namespace}]

            [components/entity-link {:group :namespaces
                                     :name [:interactive-entity-definition
                                            :identifiable-entity-definition
                                            :entity-identity
                                            :target-namespace]
                                     :require true
                                     :display :namespace
                                     :entity @process-map}]
            [components/line]
            [components/privilege]

            [components/line]
            [components/add-button {:text "Resource Model"
                                    :multi false
                                    :disable-redirect true
                                    :display-property :id
                                    :entity @process-map
                                    :add-name [:resource-model :id]
                                    :add-group :service-templates}]
            [components/resource-model {:group :service-templates
                                        :name :resource-model
                                        :id (get-in @process-map [:resource-model :id])}]

            [components/line]
            [components/add-button {:text "Target intention"
                                    :entity @process-map
                                    :add-name :target-intention
                                    :add-group :intention-definitions
                                    :multi false
                                    }]

            [components/entity-link {:group :intention-definitions
                                     :name :target-intention
                                     :display [:interactive-entity-definition
                                               :identifiable-entity-definition
                                               :entity-identity
                                               :name]
                                     :entity @process-map}]
            [components/line]
            [components/small-title "Instances"]
            [components/instance-links {:group :informal-process-instances}]]
           [components/bar
            [components/subscribe]
            [components/save]
            [components/revert]
            [components/init]
            [components/recommendation]
            ]]
          )))