(ns co-act-mobile.android.entity.interactions
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.android.components.index :as components]
            [co-act-mobile.android.ui :as ui]))

(defn interactions-entity
  "interaction entity"
  []
  (let [interaction-map (subscribe [:nav/current-entity])
        target (subscribe [:data/find [(:group @interaction-map) (:target @interaction-map)]])
        date  (.toString (js/Date. (:time @interaction-map)))
        ]
    ;(println "log out interaction " @interaction-map)
    (fn []
      [ui/scroll {:style{:flex 1
                         :flex-direction "column"}
                  :contentContainerStyle {:justify-content "center"}
                  :showsVerticalScrollIndicator false}
       [components/line]

       [components/single-link {:title "Action"
                                :display-text (:action @interaction-map)}]
       [components/line]
       [components/single-link {:title "Editor"
                                :display-text (:source @interaction-map)
                                :font-size :middle
                                ;:on-press #()
                                }]

       [components/single-link {:title "Target"
                                :display-text (:name @target)
                                :on-press #(dispatch [:nav/push-entity [(:group @interaction-map) @target]])}]

       [components/single-link {:title "Time"
                                :display-text date}]
       [components/line]
       [components/snapshot]
       ])))