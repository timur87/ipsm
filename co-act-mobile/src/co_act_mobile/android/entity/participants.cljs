(ns co-act-mobile.android.entity.participants
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.android.ui :as ui]
            [co-act-mobile.android.lib :as lib]
            [co-act-mobile.android.components.index :as components ]))

(defn participants-entity
  "Participant entity"
  [props & children]
  (let [participant-map (subscribe [:nav/current-entity])]
    ;(println "log out interaction " lib/communicator)
    (fn []
      [ui/view {:flex 1
                :flex-direction "column"
                :justify-content "center"}
       [ui/scroll {:margin-bottom (-> common :component :margin-bottom)
                   :showsVerticalScrollIndicator false}

        [components/add-button {:text "Target namespace"
                                :entity @participant-map
                                :add-name :target-namespace
                                :add-group :namespaces
                                :require true
                                :multi false
                                :display-property :namespace}]
        [components/entity-link {:group :namespaces
                                 :require true
                                 :name :target-namespace
                                 :display :namespace
                                 :entity @participant-map}]
        [components/line]

        [components/single-link {:title "Name"
                                 :display-text (:name @participant-map)
                                 :font-size :middle}]

        [components/line]
        [components/single-link {:title "Email"
                                 :display-text (:email @participant-map)
                                 :on-press #(.email lib/communicator
                                                    ;to /String Array
                                                    (clj->js [(:email @participant-map)])
                                                    ;cc /String Array
                                                    (clj->js nil)
                                                    ;bcc /String Array
                                                    (clj->js nil)
                                                    ;subject /String
                                                    "notification from co-act-mobile"
                                                    ;body /String
                                                    (clj->js nil))}]
        [components/line]
        [components/single-link {:title "Phone"
                                 :display-text (:phone @participant-map)}]
        [components/phone {:number (:phone @participant-map)}]
        [components/line]
        [components/single-link {:title "Website"
                                 :display-text (:web @participant-map)
                                 :on-press #(.web lib/communicator (:web @participant-map))}]]
       [components/bar
        [components/subscribe]
        [components/save]
        [components/revert]]]
      )))