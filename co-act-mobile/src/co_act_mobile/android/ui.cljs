(ns co-act-mobile.android.ui
  (:require [reagent.core :as r]
            [co-act-mobile.components.button :refer [button-component]]))

;;;;This file list the minor UI component mainly from react native

(defonce ReactNative (js/require "react-native"))

(defonce app-registry
         (.-AppRegistry ReactNative))

;;;; Basic views --------------------------------------

(defonce touchable-highlight (r/adapt-react-class (.-TouchableHighlight ReactNative)))
(defonce touchable-opacity (r/adapt-react-class (.-TouchableOpacity ReactNative)))
(defonce view (r/adapt-react-class (.-View ReactNative)))
(defonce list-view (r/adapt-react-class (.-ListView ReactNative)))
(defonce scroll (r/adapt-react-class (.-ScrollView ReactNative)))
(defonce image (r/adapt-react-class (.-Image ReactNative)))
(defonce progress-bar (r/adapt-react-class (.-ProgressBarAndroid ReactNative)))
(defonce text (r/adapt-react-class (.-Text ReactNative)))
(defonce input (r/adapt-react-class (.-TextInput ReactNative)))
(defonce touchable (r/adapt-react-class (.-TouchableWithoutFeedback ReactNative)))
(defonce linking (r/adapt-react-class (.-Linking ReactNative)))
(defonce pan-responder  (.-PanResponder ReactNative))
(defonce dimensions (.-Dimensions ReactNative))
(def animated (.-Animated ReactNative))
(def animated-value (.-Value animated))
(def animated-view (r/adapt-react-class (.-View animated)))
(defonce picker (r/adapt-react-class (.-Picker ReactNative)))
(defonce picker-item (r/adapt-react-class (.-Item (.-Picker ReactNative))))
(def button button-component)

;;;; Router Element --------------------------------------

(def card-stack (r/adapt-react-class (.-CardStack (.-NavigationExperimental ReactNative))))
(def navigation-header-comp (.-Header (.-NavigationExperimental ReactNative)))
(def navigation-header (r/adapt-react-class navigation-header-comp))
(def header-title (r/adapt-react-class (.-Title (.-Header (.-NavigationExperimental ReactNative)))))
;(def tab-bar (r/adapt-react-class (.-TabBarIOS ReactNative)))
;(def tab-bar-item (r/adapt-react-class (.-TabBarIOS.Item ReactNative)))

;;;; External Element --------------------------------------

(def font-icons (r/adapt-react-class (js/require "react-native-vector-icons/FontAwesome")))
(def m-icons (r/adapt-react-class (js/require "react-native-vector-icons/MaterialIcons")))
(def map-component (r/adapt-react-class (js/require "react-native-maps")))
(def notification (js/require "react-native-push-notification"))