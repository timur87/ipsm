(ns co-act-mobile.components.singleLink
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [cljs.spec :as s]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

;if specified the on-press property it is a link, else it is just a text display
;font-size is an another optional property, default is small
(def style {:view   {:padding (-> common :component :padding)
                     :flex 3
                     :flex-direction "row"
                     :align-items "center"
                     :justify-content "space-between"}
            :title  {:font-size (-> common :text :small)
                     :font-weight "bold"
                     :color (:icon color)
                     :flex 1}
            :text   {:flex 2
                     :text-align "center"
                     :font-size (-> common :text :small)}
            })

(defn singleLink-component
  "A single text link with text title in single line"
  [props]
  {:pre [(s/valid? string? (:title props))
         (s/valid? string? (:display-text props))
         ;(s/valid? fn? (:on-delete props))
         ]}
  (let [text (or (:title props) "default value")
        display-text (:display-text props)]
    (fn [props]
      (if (s/valid? fn? (:on-press props))
        [ui/touchable-opacity {:activeOpacity (-> common :button :opacity)
                               :on-press (:on-press props)}
         [ui/view {:style (-> style :view)}
          [ui/text {:style (-> style :title)} text]
          (if-let [size (:font-size props)]
            [ui/text {:style (merge (-> style :text)
                                    {:font-size (-> common :text size)})}
             display-text]
            [ui/text {:style (-> style :text)} display-text]
            )]]
        [ui/view {:style (-> style :view)}
         [ui/text {:style (-> style :title)} text]
         (if-let [size (:font-size props)]
           [ui/text {:style (merge (-> style :text)
                                   {:font-size (-> common :text size)})}
            display-text]
           [ui/text {:style (-> style :text)} display-text])]))))