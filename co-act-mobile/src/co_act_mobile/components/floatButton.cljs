(ns co-act-mobile.components.floatButton
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.ui :as ui]))

(def style {:view {:background-color "white"
                   :border-color "white"
                   :border-width 1
                   :height 80
                   :width 80
                   :border-radius 40
                   :align-items "center"
                   :justify-content "center"
                   :position "absolute"
                   :bottom 20
                   :right 20
                   :elevation 10
                   }
            :text {:font-size 40
                   :color "cornflowerblue"}})

(defn float-button-component
  "Float adding button"
  [props]
  (let [current (subscribe [:nav/current])]
    (fn [props]
      [ui/touchable-highlight {:underlay-color "cadetblue"
                               :style (-> style :view)
                               :on-press #(dispatch [:data/new (:key @current)])}
       [ui/text {:style (-> style :text)} "+"]]
      )))