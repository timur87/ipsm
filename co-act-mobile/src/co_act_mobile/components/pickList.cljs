(ns co-act-mobile.components.pickList
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.ui :as ui]))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "column"
                     :align-items "stretch"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 8}
            :touchable {:view   {:padding 0
                                 :flex 7
                                 :flex-direction "row"
                                 :align-items "center"
                                 :justify-content "space-between"}
                        :text   {:flex 1
                                 :font-size (-> common :text :small)}
                        :delete {:outer-style {:width (-> common :icon :width)}
                                 :inner-style {:color (:icon color) }}
                        }})
(s/def ::seq-of-strings (s/* string?))

(defn pickList-component
  "A list for picking items indicated by checkbox"
  [props]
  {:pre [
         (s/valid? keyword? (:group props))
         (s/valid? ::seq-of-strings (:entity-ids props))
         (s/valid? ::seq-of-strings (:picked-ids props))
         (s/valid? some? (:picked-name props))
         ;;;; ensure every id in picked-list is contained in entity-list
         (s/valid? #(every?
                     (fn[picked-id]
                       (some (fn[entity-id] (=  entity-id picked-id)) (:entity-ids props))) %)
                   (:picked-ids props))
         (s/valid? boolean? (:multi props))
         ]}
  (let [picked-atom (atom false)
        picked-name (if(false? (vector? (:picked-name props)))
                      (conj [] (:picked-name props))
                      (:picked-name props))]
    (fn [props]
      [ui/view {:style {:flex 1}}
       (doall (for [entity-id (:entity-ids props)]
                (let [entity (subscribe [:data/find [(:group props) entity-id]])
                      picked (:picked-ids props)
                      picked? (some #(= entity-id %) picked)]
                  ^{:key entity-id}
                  [ui/view {:style (-> style :view)}
                   [ui/touchable-opacity {:activeOpacity (-> common :button :opacity)
                                          :on-press #(dispatch [:nav/push-entity [(:group props) @entity]])}
                    [ui/view {:style (-> style :touchable :view)}
                     [ui/button (merge (-> style :touchable :delete)
                                       {:on-press (fn[] (let []
                                                          (do
                                                            (if picked?
                                                              ;remove
                                                              (dispatch [:nav/cache [picked-name (remove #(= entity-id %) picked)]])
                                                              ;add
                                                              (if (or (:multi props) (empty? picked))
                                                                (dispatch [:nav/cache [picked-name (conj picked entity-id)]])
                                                                ;if it is single select
                                                                (dispatch [:nav/cache [picked-name [entity-id]]]))))))})
                      (if picked?
                        [ui/m-icons {:key (str "checkbox" (:id @entity))  :name "check-box" :size (-> common :icon :size)}]
                        [ui/m-icons {:key (str "checkbox" (:id @entity))  :name "check-box-outline-blank" :size (-> common :icon :size)}])]
                     [ui/text {:style (-> style :touchable :text)} (get-in @entity [:interactive-entity-definition
                                                                                    :identifiable-entity-definition
                                                                                    :entity-identity
                                                                                    :name]) ]

                     ;following code reserved for create deletable list -------------------------

                     ;[ui/button (merge (-> style :touchable :delete)
                     ;                  {:on-press (fn[](do
                     ;                                    ;(println "new list is" (remove #(= entity-id %) (:entity-ids props)))
                     ;                                    (dispatch [:nav/cache {(:name props) (remove #(= entity-id %) (:entity-ids props))}])))})
                     ; [ui/m-icons {:key (str "cancel" (:id @entity))
                     ;              :name (-> common :icon-name :delete)
                     ;              :size (-> common :icon :size)}]]
                     ]]])))])))