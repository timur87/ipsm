(ns co-act-mobile.components.entityLinks
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [cljs.spec :as s]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "column"
                     :align-items "stretch"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 8}
            :touchable {:view   {:padding 0
                                 :flex 7
                                 :flex-direction "row"
                                 :align-items "center"
                                 :justify-content "space-between"}
                        :text   {:flex 1
                                 :font-size (-> common :text :small)}
                        :delete {:outer-style {:width (-> common :icon :width)}
                                 :inner-style {:color "grey"}}
                        }})

(defn entityLinks-component [props]
  "A list for clickable entities"
  {:pre [(s/valid? keyword? (:group props))
         (s/valid? some? (:entity props))
         (s/valid? some? (:name props))]}
  (fn [props]
    (let [attribute-name (if(false? (vector? (:name props)))
                           (conj [] (:name props))
                           (:name props))
          entityIds (get-in(:entity props) attribute-name)
          display (or (:display-vec props) [:interactive-entity-definition
                                        :identifiable-entity-definition
                                        :entity-identity
                                        :name])]
      [ui/view {:style {:flex 1}}
       (doall (for [entity-id entityIds]
                (let [entity (subscribe [:data/find [(:group props) entity-id]])]
                  ^{:key entity-id}
                  [ui/view {:style (-> style :view)}
                   [ui/touchable-opacity {:activeOpacity (-> common :button :opacity)
                                          :on-press #(dispatch [:nav/push-entity [(:group props) @entity]])}
                    [ui/view {:style (-> style :touchable :view)}
                     [ui/text {:style (-> style :touchable :text)} (get-in @entity display)]
                     [ui/button (merge (-> style :touchable :delete)
                                       {:on-press (fn[](do
                                                         ;(println "new list is" (remove #(= entity-id %) entityIds))
                                                         (dispatch [:nav/cache [attribute-name (remove #(= entity-id %) entityIds)]])))})
                      [ui/m-icons {:key (str "cancel" (:id @entity))
                                   :name (-> common :icon-name :delete)
                                   :size (-> common :icon :size-fix)
                                   }]]]]])))])))