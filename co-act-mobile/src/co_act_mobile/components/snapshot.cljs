(ns co-act-mobile.components.snapshot
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.ui :as ui]))

(def style {:view {:flex 1
                   :justify-content "center"
                   :align-items "center"}
            :button {:outer-style {:width 250
                                   :flex 1
                                   :margin 10
                                   }
                     :inner-style {:color (:icon color)
                                   :text-align "center"
                                   :font-size (-> common :text :middle)}
                     :group-style {:border-width 0
                                   :border-radius 5
                                   :justifyContent "center"
                                   :margin 10}}
            :text {:fontFamily  ".HelveticaNeueInterface-MediumP4"
                   :font-size (-> common :text :small)
                   :margin 5
                   :fontWeight  "bold"
                   :textAlign   "center"}})

(defn snapshot-component
  "Snapshot button in interaction"
  [props & children]
  (let [current (subscribe [:nav/current-entity])
        snapshot (:snapshot-entity @current)]
    (fn [props & children]
      [ui/view {:style (-> style :view)}
       [ui/button (merge (-> style :button) {:on-press #(dispatch [:nav/push-snapshot [(:group @current) snapshot]])})
        ^{:key "save-button-text"}[ui/text {:style (merge (-> style :text) (-> style :color :save))} "View Snapshot" ]
        [ui/m-icons {:color (-> color :icon)
                     :key "forward"
                     :name "forward"
                     :size (-> common :icon :size)}]]])))