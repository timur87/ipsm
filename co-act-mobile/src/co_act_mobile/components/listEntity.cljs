(ns co-act-mobile.components.listEntity
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

;This component define the display of entities in the list
;With combination of list.js, it offer generic list creation.

(def style {
            :view {:flexDirection  "row"
                   :justifyContent "center"
                   :padding-left 2
                   :padding-right 2
                   :alignItems "center"}
            :text {:padding-top 2
                   :text-align "center"
                   :padding-bottom 2
                   }
            })

(defn intention [intention-map]
  (fn [intention-map]
    [ui/view (-> style :view)
     [ui/view {:style {:flex 1}}
      [ui/text {:style (-> style :text)
                :ellipsize-mode "tail"
                :number-of-lines 1}(get-in intention-map [:interactive-entity-definition
                                                                         :identifiable-entity-definition
                                                                         :entity-identity
                                                                         :name])]]]))

(defn strategy [strategy-map]
  (fn []
    [ui/view (-> style :view)
     [ui/view {:style {:flex 1}}
      [ui/text {:style (-> style :text)
                :ellipsize-mode "tail"
                :number-of-lines 1}(get-in strategy-map [:interactive-entity-definition
                                                          :identifiable-entity-definition
                                                          :entity-identity
                                                          :name])]]]))

(defn context [context-map]
  ;(let [state (if (nil? (:state props)) false (:state props))]
  (fn []
    [ui/view (-> style :view)
     [ui/view {:style {:flex 1}}
      [ui/text {:style (-> style :text)
                :ellipsize-mode "tail"
                :number-of-lines 1}(get-in context-map [:interactive-entity-definition
                                                          :identifiable-entity-definition
                                                          :entity-identity
                                                          :name])]]]))

(defn capability [capability-map]
  ;(let [state (if (nil? (:state props)) false (:state props))]
  (fn []
    [ui/view (-> style :view)
     [ui/view {:style {:flex 1}}
      [ui/text {:style (-> style :text)
                :ellipsize-mode "tail"
                :number-of-lines 1}(get-in capability-map [:interactive-entity-definition
                                                          :identifiable-entity-definition
                                                          :entity-identity
                                                          :name])]]]))

(defn process-definition [definition-map]
  ;(let [state (if (nil? (:state props)) false (:state props))]
  (fn []
    [ui/view (-> style :view)
     [ui/view {:style {:flex 1}}
      [ui/text {:style (-> style :text)
                :ellipsize-mode "tail"
                :number-of-lines 1}(get-in definition-map [:interactive-entity-definition
                                                          :identifiable-entity-definition
                                                          :entity-identity
                                                          :name])]]]))




(defn interaction [interaction-map]
  ;(let [state (if (nil? (:state props)) false (:state props))]
  (fn []
    [ui/view {:flexDirection  "row"
              :justifyContent "center"
              :alignItems "center"}
     ;[ui/text (:state capability-map)]
     [ui/text (str (:source interaction-map) " ")]
     [ui/text (:action interaction-map)]
     ;[ui/m-icons {:key (str "icon" (:key instance-map)) :name "assignment-turned-in" :size 20}]
     ]
    ))

(defn participant [participant-map]
  ;(let [state (if (nil? (:state props)) false (:state props))]
  (fn []
    [ui/view (-> style :view)
     [ui/view {:style {:flex 1}}
      [ui/text {:style (-> style :text)
                :ellipsize-mode "tail"
                :number-of-lines 1}(:name participant-map)]]]))

(defn name-space [namespace-map]
  ;(let [state (if (nil? (:state props)) false (:state props))]
  (fn []
    [ui/view {:flexDirection  "column"
              :justifyContent "center"
              :alignItems "center"}
     ;[ui/text (:state capability-map)]
     [ui/text (:name namespace-map)]
     [ui/text {:style (-> style :text)}(:namespace namespace-map)]
     ;[ui/m-icons {:key (str "icon" (:key instance-map)) :name "assignment-turned-in" :size 20}]
     ]
    ))

(defn group [group-map]
  (fn []
    [ui/view (-> style :view)
     [ui/text {:style (-> style :text)}(:name group-map)]]))


(def entities-map {:intention-definitions intention
                   :strategy-definitions strategy
                   :context-definitions context
                   :capability-definitions capability
                   :informal-process-definitions process-definition
                   :interactions interaction
                   :participants participant
                   :namespaces name-space
                   :groups group})

;;;; defmulti defmethod