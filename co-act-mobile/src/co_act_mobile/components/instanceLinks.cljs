(ns co-act-mobile.components.instanceLinks
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [cljs.spec :as s]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "column"
                     :align-items "stretch"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 8}
            :touchable {:view   {:padding 0
                                 :flex 7
                                 :flex-direction "row"
                                 :align-items "center"
                                 :justify-content "space-between"}
                        :text   {:flex 1
                                 :font-size (-> common :text :small)}
                        :delete {:outer-style {:width (-> common :icon :width)}
                                 :inner-style {:color "grey"}}
                        }})

(defn instance-links-component [props]
  "A list for instance descriptors"
  {:pre [(s/valid? keyword? (:group props))]}
  (fn [props]
    (let [current (subscribe [:nav/current-entity])
          instances (vec (vals (:instances @current)))]
      [ui/view {:style {:flex 1}}
       (doall (for [instance instances]
                  ^{:key (:id instance)}
                  [ui/view {:style (-> style :view)}
                   [ui/touchable-opacity {:activeOpacity (-> common :button :opacity)
                                          :on-press #(dispatch [:nav/push-entity [(:group props) instance]])}
                    [ui/view {:style (-> style :touchable :view)}
                     [ui/text {:style (-> style :touchable :text)} (get-in instance [:identifiable-entity-definition
                                                                                     :entity-identity
                                                                                     :name])]
                     [ui/button (merge (-> style :touchable :delete)
                                       {:on-press (fn[](do
                                                         ;(println "new list is" (remove #(= entity-id %) entityIds))
                                                         (dispatch [:nav/cache [:instances (dissoc instance (:id instance))]])))})
                      [ui/m-icons {:key (str "cancel" (:id instance))
                                   :name (-> common :icon-name :delete)
                                   :size (-> common :icon :size-fix)
                                   }]]]]]))])))