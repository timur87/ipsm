(ns co-act-mobile.components.subscribe
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.lib :as lib]
            [co-act-mobile.ui :as ui]))

(def style {:container {:flex-direction "column"
                        :align-items "center"}
            :button {:outer-style {:width (-> common :icon :width)
                                   :margin-bottom (-> common :component :margin-compen)
                                   }
                     :inner-style {:color (:icon color)
                                   :text-align "center"
                                   :font-size (-> common :text :middle)}
                     :group-style {:border-width 1
                                   :border-radius 5
                                   :justifyContent "center"
                                   :margin 10}}
            :text {:fontFamily  ".HelveticaNeueInterface-MediumP4"
                   :font-size (-> common :text :small)
                   :margin 5
                   :color (-> common :text :subscribe)
                   :fontWeight  "bold"
                   :textAlign   "center"}
            :icon {:font-size (-> common :text :small)
                   :margin 5
                   :fontWeight  "bold"
                   :textAlign   "center"}
            :string {:unsubscribe "Unsubscribe"
                     :subscribe "Subscribe"}
            :disable-text {:fontFamily  ".HelveticaNeueInterface-MediumP4"
                           :font-size (-> common :text :small)
                           :margin 5
                           :color (-> common :text :subscribe)
                           :fontWeight  "bold"
                           :textAlign   "center"}
            :disable-icon {:font-size (-> common :text :small)
                           :margin 5
                           :fontWeight  "bold"
                           :textAlign   "center"}
            :disable-string {:definition "definition"
                             :instance "instance"}
            })

(defn subscribe-component
  "Bar component : subscribe button"
  [props]
  (fn [prop]
    (if-let [login @(subscribe [:user/login])]
      (let [list (subscribe [:user/subscribe])
            entity (subscribe [:nav/current-entity])
            group (subscribe [:nav/current-group])
            subscribed? @(subscribe[:user/subscribe?])
            ]

         (if subscribed?
           ;if subscribed
           [ui/view {:style (-> style :container)}
            [ui/button (merge (-> style :button) {:on-press #(dispatch [:user/unsubscribe (:id @entity)])})
            ;^{:key "unsubscribe-button-text"} [ui/text {:style (-> style :text)} (-> style
            ;                                                                       :string :unsubscribe)]
            [ui/m-icons {:color (-> color :disable)
                         :key   "visibility-off"
                         :name  "visibility-off"
                         :size  (-> common :icon :size)}]]
            [ui/text "Unsubscribe"]]

           ;if not subscribed
           [ui/view {:style (-> style :container)}
            [ui/button (merge (-> style :button) {:on-press #(dispatch [:user/subscribe [@group @entity]])})
            ;^{:key "subscribe-button-text"} [ui/text {:style (-> style :text)} (-> style
            ;                                                                       :string :subscribe)]
            [ui/m-icons {:color (-> color :icon)
                         :key   "visibility"
                         :name  "visibility"
                         :size  (-> common :icon :size)}]]
            [ui/text "Subscribe"]]
           )
         ))))