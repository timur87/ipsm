(ns co-act-mobile.components.addButton
  (:require [reagent.core :as r :refer [atom]]
            [cljs.spec :as s]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "row"
                     :align-items "center"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 10}
            :text   {:font-size (-> common :text :small)
                     :color (:icon color)
                     :font-weight "bold"
                     :flex 8}
            :button {:outer-style {:width (-> common :icon :width)}
                     :inner-style {:color (:icon color)}}})

; multi is option property, default is true
; add-group is the group name of the target element
; add-name is the name of property to be pick in current element
(defn addButton-component
  "A add button for add component id into list, which redirect user to a pick list page"
  [props]
  {:pre [(s/valid? string? (:text props))
         ]}
  (let [text (or (:text props) "default value")
        picked-name (if(false? (vector? (:add-name props)))
                      (conj [] (:add-name props))
                      (:add-name props))
        multi? (if (nil? (:multi props))
                 true
                 (:multi props))
        display-property (if (nil? (:display-property props))
                           [:interactive-entity-definition
                            :identifiable-entity-definition
                            :entity-identity
                            :name]
                           (if(false? (vector? (:display-property props)))
                             (conj [] (:display-property props))
                             (:display-property props)))]
    (fn [props]
      [ui/view {:style (get-in style [:view])}
       [ui/text {:style (get-in style [:text])} text]
       [ui/button (merge  (get-in style [:button])
                          {:on-press (if (some? (:on-press props))
                                       (:on-press props)
                                       #(dispatch [:nav/push {:key (keyword (str (:id (:entity props)) "-pick"))
                                                              :group :pick
                                                              :title (str "Pick " (name (:add-group props)))
                                                              :pick-data {:group (:add-group props)
                                                                          :picked-ids (get-in (:entity props) picked-name)
                                                                          :picked-name picked-name
                                                                          :multi multi?
                                                                          :require (:require props)
                                                                          :filter (:filter props)
                                                                          :disable-redirect (:disable-redirect props)
                                                                          :display-property display-property}}]))})
        [ui/m-icons {:key (str "add-circle" text)  :name "add" :size (-> common :icon-name :size)}]]
       ])))