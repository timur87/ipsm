(ns co-act-mobile.components.resourceRelationships
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [cljs.spec :as s]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "column"
                     :align-items "stretch"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 8}
            :touchable {:view   {:padding 0
                                 :flex 7
                                 :flex-direction "row"
                                 :align-items "center"
                                 :justify-content "space-between"}
                        :text   {:flex 1
                                 :font-size (-> common :text :small)}
                        :delete {:outer-style {:width (-> common :icon :width)}
                                 :inner-style {:color "grey"}}
                        }})

(defn resource-relationships-component
  "Resource relationship button"
  [props]
  {:pre []}
  (fn [props]
    (let [definitions (subscribe [:nav/resource-definitions])
          attribute-name (if(false? (vector? (:name props)))
                           (conj [] (:name props))
                           (:name props))
          ]
      (if (some? @definitions)
        [ui/view {:style {:flex 1}}
         (doall (for [entity (vec (vals (:relationships @definitions)))]
                  (let [id (-> entity :attributes :id)]
                    ^{:key id}
                    [ui/view {:style (-> style :view)}
                     [ui/touchable-opacity {:activeOpacity (-> common :button :opacity)
                                            :on-press #(dispatch [:nav/push {:key           (keyword (str id "-resource-relationship"))
                                                                             :group         :relationships
                                                                             :title         "Relationship model element"
                                                                             :resource-id   (keyword id)
                                                                             }])}
                      [ui/view {:style (-> style :touchable :view)}
                       [ui/text {:style (-> style :touchable :text)} (-> entity :attributes :name)]
                       [ui/button (merge (-> style :touchable :delete)
                                         {:on-press #(dispatch [:resource/delete-relationship (keyword id)])})
                        [ui/m-icons {:key (str "cancel" (:id entity))
                                     :name (-> common :icon-name :delete)
                                     :size (-> common :icon :size-fix)}]]]]])))]))))