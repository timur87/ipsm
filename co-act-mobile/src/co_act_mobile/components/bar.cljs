(ns co-act-mobile.components.bar
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.ui :as ui]))

(def style {:opacity 0.2
            :container {:position "absolute"
                        :background-color "white"
                        :padding-bottom 2
                        :padding-top 2
                        :margin-left -10
                        :margin-right -10
                        :elevation 2
                        :left 0
                        :right 0
                        :bottom 0}
            :view {:flexDirection  "row"
                   :flex 1
                   :justifyContent "space-around"
                   :alignItems     "center"}
            :button {}
            })

(defn index-of
  "Getting index of a value in vector"
  [vector value]
  (keep-indexed (fn [[idx item]] (if (= value item) idx)) vector))

(defn bar-component
  "A navigation bar which display on the bottom of screen"
  [& children]
  (let [length (count children)
        snapshot? (subscribe [:nav/snapshot?])]
    (fn [& children]
      (case length
        0 [ui/text "haha"]
        1 (get children 0)
        (if (false? @snapshot?)[ui/view {:style (-> style :container)}
                                [ui/view {:style (-> style :view)}
                                 (doall (map-indexed (fn[index child]
                                                       [ui/view {:key (str "barElement" index)
                                                                 :flex (/ 1 (count children))}
                                                        child]
                                                       )  children))
                                 ]])))))