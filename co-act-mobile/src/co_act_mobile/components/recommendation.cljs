(ns co-act-mobile.components.recommendation
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [cljs.spec :as s]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

(def style {:container {:flex-direction "column"
                        :align-items "center"}
            :button {:outer-style {:width (-> common :icon :width)
                                   :margin-bottom (-> common :component :margin-compen)
                                   }
                     :inner-style {:color (:icon color)
                                   :text-align "center"
                                   :font-size (-> common :text :middle)}
                     :group-style {:border-width 1
                                   :border-radius 5
                                   :justifyContent "center"
                                   :margin 10}}})

(s/def ::seq-of-strings (s/* string?))
(defn recommendation-component
  "Bar component : recommendation generating button"
  []
  (let [entity (subscribe [:nav/current-entity])]
    (fn []
      [ui/view {:style (-> style :container)}
       [ui/button (merge (-> style :button) {:on-press #(dispatch [:nav/push {:key (keyword (str (:id @entity) "-pick"))
                                                                              :group :recommendation
                                                                              :title (str "Generate recommendations" )
                                                                              }])})
        ;^{:key "save-button-text"}[ui/text {:style (merge (-> style :text) (-> style :color :save))} "save" ]
        [ui/m-icons {:color (-> color :init)
                     :key "call-merge"
                     :name "call-merge"
                     :size (-> common :icon :size)}]]
       [ui/text "Recommendation"]])))