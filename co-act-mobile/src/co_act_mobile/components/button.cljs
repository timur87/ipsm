(ns co-act-mobile.components.button
  (:require [reagent.core :as r :refer [atom]]
            [co-act-mobile.subs]
            [co-act-mobile.style :refer [common color]]
            ))

(def ReactNative (js/require "react-native"))
(def styleSheet (r/adapt-react-class (.-StyleSheet ReactNative)))
(def view (r/adapt-react-class (.-View ReactNative)))
(def touchableOpacity (r/adapt-react-class (.-TouchableOpacity ReactNative)))
(def text (r/adapt-react-class (.-Text ReactNative)))

(def ^:const system-opacity 0.2)

(defn render-group [props]
  (merge {:flexDirection  "row"
          :flex 1
          :elevation 3
          :background-color (-> color :background)
          :justifyContent "space-around"
          :alignItems     "center"} (:group-style props)))

(defn render-style [props]
  (merge {:color (if (:disabled props) "#dcdcdc" "#009900" )
          :fontFamily  ".HelveticaNeueInterface-MediumP4"
          :font-size  15
          :margin 5
          :fontWeight  "bold"
          :textAlign   "center"} (:inner-style props)))

(defn- render-touchable-opacity [props]
  (merge {} (:outer-style props)))

(defn button-component
  "A hybrid button component"
  [props & children]
  (let [disable (if (nil? (:disable props)) false (:disable props))]
    (fn [props & children]
      (case (count children)
        0 nil
        1 [touchableOpacity {:style (render-touchable-opacity props)
                             :elevation 5
                             :activeOpacity (if (:disable props) 1 system-opacity)
                             :on-press (:on-press props)}
           [text {:style (render-style props)} children ]]
        ;else rendered as group but it need to use correct props here
        [touchableOpacity {:style (render-touchable-opacity props)
                           :activeOpacity (if (:disable props) 1 system-opacity)
                           :on-press (:on-press props)}
         [view {:style (render-group props)} children]]
        ))))