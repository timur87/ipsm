(ns co-act-mobile.spec
  (:require [cljs.spec :as s]
            ;[de.uni-stuttgart.iaas.ipsm.ipe.model.schema :as ss]
    ;[taoensso.timbre :as timbre]
            )
  )

(enable-console-print!)
;(timbre/refer-timbre)

;;;; Single entities
(s/def ::entity-type keyword?)
(s/def ::name string?)
(s/def ::target-namespace string?)
;(s/def ::definition-language #?(:clj #(instance? URI  %)
;                                 :cljs string?))
(s/def ::value string?)
(s/def ::element string?)
(s/def ::type string?)
(s/def ::required string?)
(s/def ::start-time string?)
(s/def ::end-time string?)
(s/def ::instance-state string?)
(s/def ::instance-uri string?)
(s/def ::source-model string?)
(s/def ::type-ref string?)
(s/def ::parent-instance string?)
(s/def ::id string?)
(s/def ::abstract boolean?)
(s/def ::final boolean?)
(s/def ::priority (s/and int? #(<= 10 %) #(>= 0 %)))
(s/def ::due string?)
(s/def ::participant-ref string?)
(s/def ::participant-type string?)
(s/def ::process-id string?)
(s/def ::operation-type string?)
(s/def ::preconditioner-relationship (s/coll-of ::relationship-template))
(s/def ::preconditioner-relationships (s/keys :req-un [::preconditioner-relationship]))
(s/def ::dependency (s/coll-of ::node-template))
(s/def ::dependencies (s/keys :req-un [::dependency]))

(s/def ::abstract #{"yes" "no"})
(s/def ::final #{"yes" "no"})
(s/def ::required #{"yes" "no"})
(s/def ::derived-from string?)
(s/def ::properties-definition (s/keys :opt-un [::element ::type]))
(s/def ::tag (s/coll-of (s/keys :opt-un [::name ::value])))
(s/def ::tags (s/keys :req-un [::tag]))
(s/def ::pattern string?)
(s/def ::reference string?)

(s/def ::include (s/keys :req-un [::pattern]))
(s/def ::exclude (s/keys :req-un [::pattern]))

(s/def ::extensible-elements (s/keys :opt-un [::any
                                              ::documentation
                                              ::import
                                              ::other-attributes]))

(s/def ::parameter (s/merge ::extensible-elements (s/keys :req-un [::name
                                                                   ::type
                                                                   ::required])))

(s/def ::input-parameter (s/coll-of ::parameter))
(s/def ::output-parameter (s/coll-of ::parameter))

(s/def ::input-parameters (s/keys :req-un [::input-parameter
                                           ::entity-type]))

(s/def ::output-parameters (s/keys :req-un [::output-parameter
                                            ::entity-type]))

(s/def ::single-operation (s/merge ::extensible-elements
                                   (s/keys :opt-un [::input-parameters
                                                    ::output-parameters]
                                           :req-un [::name])))

(s/def ::operation (s/coll-of (s/merge ::extensible-elements
                                       (s/keys :opt-un [::input-parameters
                                                        ::output-parameters]
                                               :req-un [::name]))))

(s/def ::interface (s/keys :opt-un [::operation
                                    ::name]
                           :req-un [::entity-type]))

(s/def ::interfaces (s/keys :req-un [::interface]))
(s/def ::source-interfaces (s/keys :req-un [::interface]))
(s/def ::target-interfaces (s/keys :req-un [::interface]))

(s/def ::tosca-entity-type (s/merge ::extensible-elements (s/keys :req-un [::name
                                                                           ::target-namespace]
                                                                  :opt-un [::tags
                                                                           ::abstract
                                                                           ::final
                                                                           ::properties-definition
                                                                           ::derived-from])))

(s/def ::node-type (s/merge ::tosca-entity-type
                            (s/keys :opt-un [::req-unuirement-definitions
                                             ::capability-definitions
                                             ::instance-states
                                             ::interfaces]
                                    :req-un [::entity-type])))

(s/def ::valid-source (s/coll-of ::type-ref))
(s/def ::valid-target (s/coll-of ::type-ref))

(s/def ::relationship-type (s/merge ::tosca-entity-type
                                    :opt-un [::valid-target
                                             ::valid-source
                                             ::instance-states
                                             ::capability-definitions
                                             ::source-interfaces
                                             ::target-interfaces]
                                    :req-un [::entity-type]))

(s/def ::tosca-artifact-type (s/merge ::tosca-entity-type (s/keys :req-un [::entity-type])))

(s/def ::artifact-reference (s/keys :req-un [::reference ::entity-type]
                                    ::opt-un [::include ::exclude]))

(s/def ::tosca-entity-template (s/merge
                                 ::extensible-elements
                                 (s/keys :req-un [::id
                                                  ::type
                                                  ::entity-type]
                                         :opt-un [::properties
                                                  ::property-constraints])))



(s/def ::min-instances number?)
(s/def ::max-instances string?)

(s/def ::source-element (s/keys :ref [::ref]))
(s/def ::target-element (s/keys :ref [::ref]))

(s/def ::node-template (s/merge ::tosca-entity-template
                                (s/keys :opt-un [::requirements
                                                 ::capabilities
                                                 ::policies
                                                 ::deployment-artifacts
                                                 ::name
                                                 ::min-instances
                                                 ::max-instances]
                                        :req-un [::entity-type])))

(s/def ::relationship-template (s/merge ::tosca-entity-template
                                        (s/keys :opt-un [::relationship-constraints
                                                         ::capabilities
                                                         ::policies
                                                         ::deployment-artifacts
                                                         ::name
                                                         ::min-instances
                                                         ::max-instances]
                                                :req-un [::entity-type
                                                         ::source-element
                                                         ::target-element])))

(s/def ::single-deployment-artifact (s/keys :req-un [::name
                                                     ::artifact-type
                                                     ::artifact-ref
                                                     ::entity-type]))

(s/def ::deployment-artifact (s/coll-of ::single-deployment-artifact))

(s/def ::deployment-artifacts (s/keys :req-un [::deployment-artifact]))


(s/def ::implementation-artifact (s/coll-of (s/keys :req-un [::interface-name
                                                             ::operation-name
                                                             ::artifact-type
                                                             ::artifact-ref
                                                             ::entity-type])))

(s/def ::implementation-artifacts (s/keys :req-un [::implementation-artificat]))

(s/def ::node-type-implementation (s/keys :req-un [::name
                                                   ::target-namespace
                                                   ::entity-type]
                                          :opt-un [::abstract
                                                   ::final
                                                   ::tags
                                                   ::derived-from
                                                   ::required-container-features
                                                   ::implementation-artifacts
                                                   ::deployment-artifacts]))

(s/def ::operation-coverage (s/keys :opt-un [::name
                                             :str/interface
                                             ::operation-target]))

(s/def ::artifact-template ::tosca-entity-template)

(s/def ::execution-environment-integrator (s/keys :req-un [::id
                                                           ::name
                                                           ::resource-specific-type-groups
                                                           ::domain-specific-type-groups
                                                           ::uri
                                                           ::entity-type]
                                                  ::opt-un [::artifact-type
                                                            ::artifact-templates]))




(s/def ::target-resource ::node-template)

(s/def ::operation-message (s/keys :req-un [::process-id
                                            ::operation-type
                                            ::target-resource
                                            ::entity-type]
                                   ::opt-un [::dependencies
                                             ::informal-process-definition
                                             ::preconditioner-relationships
                                             ::instance-state
                                             ::instance-location
                                             ::error-definition]))

(s/def ::imports (s/coll-of (s/keys :req-un [::namespace
                                             ::location
                                             ::imported-type])))

(s/def :dm/types (s/coll-of ::node-type
                            ::relationship-type
                            ::node-type-implementation
                            ::artifact-type
                            ::artifact-template))

(s/def ::domain-manager-message (s/keys :req-un [:dm/types]
                                        :opt-un [::imports
                                                 ::target-namespace
                                                 ::id]))



(s/def ::instance-descriptors (s/coll-of ::instance-descriptor))
(s/def ::initializable-entity-definition (s/keys :req-un [::identifiable-entity-definition]
                                                 :opt-un [::instance-descriptors]))
(s/def ::participant (s/keys :req-un [::participant-ref]
                             :opt-un [::participant-type]))
(s/def ::participant-list (s/coll-of ::participant))
(s/def ::interactive-entity-definition (s/keys :req-un [::identifiable-entity-definition]
                                               :opt-un [::participant-list]))
(s/def ::interactive-initializable-entity-definition (s/keys :req-un [::initializable-entity-definition]
                                                             :opt-un [::participant-list]))
(s/def ::intention-definition (s/keys :req-un [::interactive-entity-definition]
                                      :opt-un [::related-intentions
                                               ::achieving-strategies
                                               ::initial-contexts
                                               ::final-contexts
                                               ::priority
                                               ::due]))
(s/def ::organizational-capability (s/coll-of string?))
(s/def ::organizational-capabilities (s/keys :req-un [::organizational-capability]))

(s/def ::required-intention (s/coll-of string?))
(s/def ::required-intentions (s/keys :req-un [::organizational-capability]))

(s/def ::operational-process (s/coll-of string?))
(s/def ::operational-processes (s/keys :req-un [::organizational-capability]))

(s/def ::strategy-definition (s/keys :req-un [::interactive-initializable-entity-definition]
                                     :opt-un [::required-intentions
                                              ::organizational-capabilities
                                              ::operational-processes]))

(s/def ::strategy-definitions (s/coll-of ::strategy-definition))


(s/def ::context-definition
  (s/merge ::interactive-entity-definition (s/keys :opt-un-un [::contained-contexts])))

(s/def ::context-definitions (s/coll-of ::context-definition))

(s/def ::capability-type number?)
(s/def ::providing-resources (s/coll-of string?))
(s/def ::desired-resources (s/coll-of string?))
(s/def ::capability-definition
  (s/merge ::interactive-entity-definition
           (s/keys :opt-un [
                            ::capability-type
                            ::providing-resources
                            ::desired-resources])))
(s/def ::capability-definitions (s/map-of keyword? ::capability-definition))


(s/def ::informal-process-definition
  (s/merge ::interactive-entity-definition
           (s/keys :opt-un [::target-intention
                            ::required-intentions
                            ::organizational-capabilities
                            ::resource-model])))

(s/def ::informal-process-definitions (s/map-of keyword? ::informal-process-definition))

(s/def ::instance-descriptor
  (s/merge ::identifiable-entity-definition
           (s/keys :opt-un [::start-time
                            ::end-time
                            ::instance-state
                            ::instance-uri
                            ::source-model
                            ::parent-instance])))



(s/def ::StringOrNil (s/or :null nil?
                           :string string?))

(s/def ::text string?)
(s/def ::group keyword?)
;(s/def ::id string?)
(s/def ::username string?)
(s/def ::icon string?)
(s/def ::size number?)
(s/def ::key keyword?)
(s/def ::title string?)
(s/def ::index number?)
(s/def ::string-coll (s/coll-of string?))
(s/def ::time number?)
(s/def ::target string?)
(s/def ::source string?)
(s/def ::action string?)
;(s/def ::name string?)
(s/def ::updatedAt (s/coll-of number?))
(s/def ::createdAt (s/coll-of number?))
(s/def ::target-note-id string?)
(s/def ::contextual-parameters some?)
(s/def ::editor string?)
(s/def ::snapshot-entity some?)
(s/def ::reader ::string-coll)
(s/def ::writer ::string-coll)
(s/def ::executor ::string-coll)
(s/def ::administrator ::string-coll)
(s/def ::phone string?)
(s/def ::email string?)
(s/def ::web string?)
;(s/def ::start-time (s/coll-of number?))
;(s/def ::end-time (s/coll-of number?))
(s/def ::instance-state-id  number?)
;(s/def ::instance-uri string?)
;(s/def ::source-model string?)
;(s/def ::parent-instance string?)
(s/def ::color string?)
(s/def ::link some?)
;;;;=== main-list
(s/def ::menu-item (s/keys :req-un [::text ::group ::icon ::color]))
(s/def ::main-list (s/coll-of ::menu-item))

;;;;=== login
(s/def ::login boolean?)

;;;;=== user
;(s/def ::subscribe-item (s/keys :req-un [:id :group]))
(s/def ::subscribe (s/map-of keyword? (s/keys :req-un [::id ::group])))
(s/def ::user (s/keys :req-un [::id ::username ::subscribe]))

;=== title-map
(s/def ::title-map (s/map-of keyword? string?))

;=== drawer
(s/def ::opened boolean?)
(s/def ::open-value number?)
(s/def ::close-value number?)
(s/def ::open-value-drawer number?)
(s/def ::close-value-drawer number?)
(s/def ::duration number?)
;=== ui/animated-value.
;(s/def ::animate-value some?)
;(s/def ::animate-value-drawer some?)
(s/def ::drawer (s/keys :req-un [::size ::opened ::open-value ::close-value ::open-value-drawer ::close-value-drawer ::duration]))

;=== static
(s/def ::capability-types ::string-coll)
(s/def ::priority-level ::string-coll)
(s/def ::priority-color ::string-coll)
(s/def ::priority-color-code ::string-coll)
(s/def ::host string?)
(s/def ::instance-state-map (s/map-of keyword? ::string-coll))
(s/def ::privilege-level ::string-coll)
(s/def ::static (s/keys :req-un [::capability-types
                                 ::priority-level
                                 ::priority-color
                                 ::priority-color-code
                                 ::host
                                 ::instance-state-map
                                 ::privilege-level ]))

;=== strings
(s/def ::strings (s/map-of keyword? string?))
;=== nav
(s/def ::navigation-state (s/keys  :req-un [::key ::group ::title]
                                   :opt-un [::entity-data ::pick-data ::resource-data ::resource-id ::requesting]
                                   ))
(s/def ::children ::navigation-state)
(s/def ::navigation-states (s/keys :req-un [::key ::children ::index]
                                   :opt-un []))

;;;;=== target-instance-group
(s/def ::target-instance-group (s/map-of keyword? keyword?))

;;;;=== data
(s/def ::definition-content some?)
;(s/def ::definition-language some?)
(s/def ::properties some?)
(s/def ::participant-list some?)
(s/def ::target-namespace string?)
(s/def ::entity-definition (s/keys :req-un [::definition-content
                                            ::definition-language]
                                   :opt-un [::properties]))
(s/def ::entity-definitions (s/coll-of ::entity-definition))
(s/def ::mobile-entity-definition (s/keys :opt-un [::entity-definitions
                                                   ::entity-identity]))
(s/def ::entity-identity (s/keys :req-un [::name
                                          ::target-namespace]))
(s/def ::identifiable-entity-definition (s/keys :opt-un [::entity-identity
                                                         ::entity-definitions]))
(s/def ::interactive-entity-definition (s/keys :req-un [::identifiable-entity-definition]
                                               :opt-un [::participant-list]))

(s/def ::initial-contexts ::string-coll)
(s/def ::final-contexts ::string-coll)
(s/def ::priority number?)
(s/def ::due (s/coll-of number?))
(s/def ::related-intentions ::string-coll)
(s/def ::organizational-capabilities ::string-coll)
(s/def ::achieving-strategies ::string-coll)
(s/def ::intention-definition
           (s/keys :req-un [::interactive-entity-definition]
             :opt-un [
                            ::related-intentions
                            ::achieving-strategies
                            ::initial-contexts
                            ::final-contexts
                            ::priority
                            ::due
                            ::organizational-capabilities
                            ]))
(s/def ::intention-definitions (s/map-of keyword? ::intention-definition))
(s/def ::target-intention string?)
(s/def ::required-intentions ::string-coll)
(s/def ::operational-processes ::string-coll)

(s/def ::strategy-definition
           (s/keys
             :req-un [::interactive-entity-definition]
             :opt-un [::target-intention
                            ::required-intentions
                            ::operational-processes
                            ::priority
                            ::due
                            ]))
(s/def ::strategy-definitions (s/map-of keyword? ::strategy-definition))

(s/def ::contained-contexts ::string-coll)
(s/def ::context-definition
           (s/keys :req-un [::interactive-entity-definition]
             :opt-un [
                            ::contained-contexts
                            ]))
(s/def ::context-definitions (s/map-of keyword? ::context-definition))

(s/def ::capability-type number?)
(s/def ::providing-resources ::string-coll)
(s/def ::desired-resources ::string-coll)
(s/def ::capability-definition
           (s/keys :req-un [::interactive-entity-definition]
             :opt-un [::interactive-entity-definition
                            ::capability-type
                            ::providing-resources
                            ::desired-resources
                            ]))
(s/def ::capability-definitions (s/map-of keyword? ::capability-definition))

(s/def ::ref string?)
(s/def ::definitions some?)
(s/def ::resource-model-uri string?)
(s/def ::resource-model (s/keys :opt-un [::definitions
                                         ::target-namespace
                                         ::ref
                                         ::resource-model-uri]))
(s/def ::instances (s/map-of keyword? ::instance-descriptor))
(s/def ::instance-descriptor
  (s/keys :req-un [::identifiable-entity-definition]
          :opt-un [::start-time
                   ::end-time
                   ::instance-state-id
                   ::instance-uri
                   ::source-model
                   ::parent-instance
                   ]))
(s/def ::informal-process-definition
           (s/keys :req-un [::interactive-entity-definition]
             :opt-un [::target-intention
                            ::required-intentions
                            ::organizational-capabilities
                            ::resource-model
                      ::instances
                            ]))
(s/def ::informal-process-definitions (s/map-of keyword? ::informal-process-definition))


;;;; === mobile entity definition
(s/def ::participant   (s/merge ::mobile-entity-definition
                                (s/keys :opt-un [
                                                 ::phone
                                                 ::email
                                                 ::web
                                                 ])))
(s/def ::participants (s/map-of keyword? ::participant))
(s/def ::note
  (s/keys :req-un [::id]
          :opt-un [::createdAt
                   ::updatedAt
                   ::editor
                   ::target-note-id
                   ::contextual-parameters
                   ]))
(s/def ::note-list (s/coll-of ::note))
(s/def ::privilege-management-panel (s/merge ::mobile-entity-definition
                                             (s/keys :opt-un [
                                                              ::reader
                                                              ::writer
                                                              ::executor
                                                              ::administrator
                                                              ])))
(s/def ::group-definition   (s/merge ::mobile-entity-definition
                                     (s/keys :opt-un [::participants
                                                      ::note-list
                                                      ::privilege-management-panel
                                                      ])))
(s/def ::groups (s/map-of keyword? ::group-definition))
(s/def ::namespace-definition (s/keys :req-un [::id ::name ::namespace]
                                      :opt-un [::link]))
(s/def ::namespaces (s/map-of keyword? ::namespace-definition))

(s/def ::interaction (s/keys :req-un [::target ::source ::action ::id ::group ::time]
                             :opt-un [::snapshot-entity]))
(s/def ::interactions (s/map-of keyword? ::interaction))

(s/def ::node-type some?)
(s/def ::service-template some?)
(s/def ::relationship-type some?)
(s/def ::node-type-instance some?)
(s/def ::service-template-instance some?)
(s/def ::relationship-type-instance some?)
(s/def ::node-types (s/map-of keyword? ::node-type))
(s/def ::service-templates (s/map-of keyword? ::service-template))
(s/def ::relationship-type-instances (s/map-of keyword? ::relationship-type-instance))
(s/def ::node-type-instances (s/map-of keyword? ::node-type-instance))
(s/def ::service-template-instances (s/map-of keyword? ::service-template-instance))
(s/def ::relationship-types (s/map-of keyword? ::relationship-type))
(s/def ::resource-models (s/keys :req-un [::node-types ::service-templates ::relationship-types]))
(s/def ::data (s/keys :req-un [
                               ::interactions
                               ::intention-definitions
                               ::strategy-definitions
                               ::context-definitions
                               ::capability-definitions
                               ::informal-process-definitions
                               ::participants
                               ::namespaces
                               ::groups
                               ::interactions
                               ::node-types
                               ::service-templates
                               ::relationship-types
                               ::node-type-instances
                               ::service-template-instances
                               ::relationship-type-instances
                               ]))

;;;; Single entities
(s/def ::entity-type keyword?)

(s/def ::db (s/keys :req-un [::data
                             ::nav
                             ::main-list
                             ::login
                             ::user
                             ::drawer
                             ::static
                             ::strings
                             ::title-map
                             ::target-instance-group]))
