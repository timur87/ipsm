(ns co-act-mobile.schema
  (:require [schema.core :as s :include-macros true]))


(def NavigationState
  {:key s/Keyword
   :group s/Keyword
   :title s/Str
   (s/optional-key :entity-data) s/Any
   (s/optional-key :pick-data) s/Any
   s/Keyword s/Any})

(def privilege-group
  {:reader [s/Str]
   :writer [s/Str]
   :executor [s/Str]
   :administrator [s/Str]
   })

(def PostsSchema
  {s/Keyword {:id s/Str
              :user-id s/Str
              :content s/Str}})

(def InstanceDescriptor
  {:instance-state-id  s/Int
   :definition s/Str
   :name s/Str
   :id s/Any
   :posts   PostsSchema
   :start-date [s/Num]
   :end-date [s/Num]
   :instance-uri s/Str
   :source-model s/Str
   (s/optional-key :parent-instance) s/Str
   :author s/Str})

(def NavigationParentState
  (dissoc
    (merge NavigationState
           {:index    s/Int
            :children [NavigationState]})
    :title
    :group))

(def EntityIdentity
  {:name s/Str
   :target-namespace s/Str})

(def IdentifiableEntityDefinition)

(def InteractiveInitializableEntityDefinition)

(def InteractiveEntityDefinition)

(def DeploymentDescriptor
  {:runnable-uri s/Any
   :execution-environment s/Any
   :runnable-name s/Str})


(def Participant
  {:participantRef s/Str
   :participantType s/Str})

;;;; schema of app-db
(def schema {:nav NavigationParentState
             :main-list [{:text s/Str
                          :group s/Str
                          :icon s/Str}
                         ]
             :login s/Bool
             :user {:id s/Str
                    :username s/Str
                    :subscribe {s/Keyword {:id s/Str
                                           :group s/Keyword}}}
             :title {s/Keyword s/Str}
             :target-instance-group {s/Keyword s/Keyword}
             :drawer {:size s/Num
                      :opened s/Bool
                      :open-value s/Int
                      :close-value s/Int
                      :duration s/Num
                      :animate-value s/Any
                      :open-value-drawer s/Int
                      :close-value-drawer s/Int
                      :animate-value-drawer s/Any
                      }
             :static {:capability-type [s/Str]
                      :priority-level [s/Str]
                      :priority-color [s/Str]
                      :priority-color-code [s/Str]
                      :instance-state {s/Keyword [s/Str]}
                      :privilege-level [s/Str]
                      }
             :strings {s/Keyword s/Str}
             :data {:interactions  {s/Keyword {:target  s/Str
                                               :source  s/Str
                                               :action  s/Str
                                               :id s/Str
                                               ;:content s/Str
                                               :group s/Keyword
                                               (s/optional-key :snapshot-entities) [s/Str]
                                               :time s/Num}}
                    :intention-definitions {s/Keyword {:state s/Int
                                       :definition s/Str
                                       :id s/Any
                                       :priority s/Int
                                       :due-date [s/Num]
                                       :name s/Str
                                       :strategy-definitions [s/Str]
                                       :active-strategy [s/Str]
                                            :instances [s/Str]
                                       (s/optional-key :reader) [s/Str]
                                       (s/optional-key :writer) [s/Str]
                                       (s/optional-key :executor) [s/Str]
                                       (s/optional-key :administrator) [s/Str]
                                       (s/optional-key :namespace) s/Any
                                       (s/optional-key :initial-contexts) [s/Str]
                                       (s/optional-key :final-contexts) [s/Str]
                                       (s/optional-key :alternative-final-contexts) [s/Str]
                                       :author s/Str}}
                    :strategy-definitions {s/Keyword {:state s/Int
                                            :definition s/Str
                                            :name s/Str
                                            :target-intention s/Str
                                            (s/optional-key :namespace) s/Any
                                            (s/optional-key :posts) s/Any
                                            :contained-intentions [s/Str]
                                            :instances [s/Str]
                                            :implementations [s/Str]
                                            :capability-definitions [s/Str]
                                            :id s/Any
                                            :author s/Str}}
                    :context-definitions {s/Keyword {:definition s/Str
                                          :name s/Str
                                          (s/optional-key :namespace) s/Any
                                          :id s/Any
                                          :contained-contexts [s/Str]
                                          :author s/Str}}
                    :capability-definitions {s/Keyword {:state s/Int
                                              :name s/Str
                                              :definition s/Str
                                              (s/optional-key :namespace) s/Any
                                              :id s/Any
                                              :instances [s/Str]
                                              :providing-resources [s/Str]
                                              :desired-resources [s/Str]
                                              :contained-capabilities [s/Str]
                                              :capability-type s/Int
                                              :author s/Str}}
                    :resources {s/Keyword {:id s/Str
                                           :definition s/Str
                                           :uri s/Str
                                           }
                                }
                    :informal-process-definitions {s/Keyword {:state s/Int
                                                     :definition s/Str
                                                     :target-intention s/Str
                                                     :name s/Str
                                                     :id s/Any
                                                     (s/optional-key :namespace) s/Any
                                                     :model s/Str
                                                     :resources [s/Str]
                                                    :instances [s/Str]
                                                     :author s/Str}}
                    :participants {s/Keyword {:name s/Str
                                              :web s/Str
                                              :id s/Str
                                              :phone s/Str
                                              :email s/Str}}
                    :namespaces {s/Keyword {:name s/Str
                                            :id s/Str
                                            :namespace s/Str
                                            :link s/Any}}
                    :informal-process-instances {s/Keyword (merge InstanceDescriptor {})}
                    :strategy-instances {s/Keyword (merge InstanceDescriptor {})}
                    :intention-instances {s/Keyword (merge InstanceDescriptor {})}
                    :context-instances {s/Keyword (merge InstanceDescriptor {})}
                    :capability-instances {s/Keyword(merge InstanceDescriptor {})}
                    :groups {s/Keyword {:name    s/Str
                                        :id      s/Str
                                        :members [s/Str]
                                        :posts   PostsSchema}}
                    }
             })