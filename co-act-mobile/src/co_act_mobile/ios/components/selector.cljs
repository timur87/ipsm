(ns co-act-mobile.components.ios.selector
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [cljs.spec :as s]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "row"
                     :align-items "center"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 3}
            :text   {:font-size (-> common :text :middle)
                     :text-align "center"
                     :color (:icon color)
                     :flex 1}
            :picker {:flex 2
                     :width 100}
            :text-container {:align-items "center"
                             :width 100
                             :flex 2}
            :selected-text {:font-size (-> common :text :middle)
                            :text-align "center"}
            })

(s/def ::seq-of-strings (s/* string?))
(defn selector-component [props]
  {:pre [(s/valid? ::seq-of-strings (:options props))
         (s/valid? string? (:text props))
         (s/valid? number? (:selected-value props))
         (s/valid? fn? (:on-select props))]}
  (let [changed (atom false)]
    (fn [props]
      [ui/view {:style (-> style :view)}
       [ui/text  {:style (-> style :text)} (:text props)]
       (if (true? @changed) ;if enable select menu
         [ui/picker {:style (-> style :picker)
                     :selected-value (:selected-value props)
                     :on-value-change (:on-select props)
                     :mode  "drop-down"
                     }
          (for [item (:options props)]
            (let [index (.indexOf (:options props) item)
                  color (if (s/valid? ::seq-of-strings (:options-color props))
                          (nth (:options-color props) index)
                          "black")]
              ;map is not supported at the moment
              ;item style only work for ios, different style control
              ; -> https://facebook.github.io/react-native/docs/picker.html
              ^{:key item}[ui/picker-item {:label item :color color :value index}]))]

         ;drop down button
         (let [index (or (:selected-value props) 0)]
           [ui/view {:style (-> style :text-container)}
           [ui/text {:style (merge (-> style :selected-text)
                                   {:color (if (s/valid? ::seq-of-strings (:options-color props))
                                             (nth (:options-color props) index)
                                             "black")})}
            (nth (:options props) index)]]))
       [ui/button  {:outer-style {:width (-> common :icon :width)}
                    :inner-style {:color (:icon color)}
                    :on-press #(reset! changed (not @changed))}
        (if (true? @changed)
          ;if enable select menu
          [ui/m-icons {:key (str "done")  :name "done" :size (-> common :icon :size)}]
          ;drop down button
          [ui/m-icons {:key (str "arrow-drop-down-circle-priority")  :name "arrow-drop-down-circle" :size (-> common :icon :size)}])]])))