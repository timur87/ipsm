(ns co-act-mobile.components.ios.datePicker
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.ui :as ui]))

(def ReactNative (js/require "react-native"))
(def DatePicker (r/adapt-react-class (.-DatePickerIOS ReactNative)))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "row"
                     :align-items "center"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 3}
            :text   {:font-size (-> common :text :middle)
                     :text-align "center"
                     :color (:icon color)
                     :flex 1}
            :picker {:flex 2}
            :selected-text {:flex 2
                            :font-size (-> common :text :middle)
                            :width 100
                            :text-align "center"}})

;spec with coll-of with 5 args is not supported yet
;(s/def ::dateCol (s/coll-of number? :kind vector? :count 3 :distinct true ))
(s/def ::dateCol (s/* number?))

(defn date-picker-component [props]
  {:pre [(s/valid? ::dateCol (:value props))
         (s/valid? string? (:text props))
         (s/valid? fn? (:on-change props))]}
  (let [changed (atom false)]
    (fn [props]
      [ui/view {:style (-> style :view)}
       (if (false? @changed)
       [ui/text {:style (-> style :text)} (:text props)])
       (if (true? @changed) ;if enable select menu
         [DatePicker {:style (-> style :picker)
                      :mode "date"
                      :date (js/Date.
                              ;year
                              (nth (:value props) 0)
                              ;month
                              (- (nth (:value props) 1) 1)
                              ;day
                              (nth (:value props) 2))
                      ;:onDateChange #(println (.getUTCDate %) )
                      :onDateChange #((:on-change props) (vector (+ (.getYear %) 1900) (+ (.getMonth %) 1) (.getDate %)))
                     }]

         ;drop down button
         [ui/text {:style (-> style :selected-text)} (str (nth (:value props) 2) "/"
                                                          (nth (:value props) 1) "/"
                                                          (nth (:value props) 0))])

       [ui/button  {:outer-style {:width (-> common :icon :width)}
                    :inner-style {:color (:icon color)}
                    :on-press #(reset! changed (not @changed))}
        (if (true? @changed)
          ;if enable select menu
          [ui/m-icons {:key (str "done")  :name "done" :size (-> common :icon :size)}]
          ;drop down button
          [ui/m-icons {:key (str "date-range")  :name "date-range" :size (-> common :icon :size)}])]]

                                        ;((:on-change props) (vector (.-year %) (.-month %) (.-day %))))))}
      )))