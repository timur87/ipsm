(ns co-act-mobile.entity.ios.context
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.components.index :as components]
            [co-act-mobile.ui :as ui]))

(defn context-entity []
  (let [conetxt-map (subscribe [:nav/current-entity])
        ]
    ;(println "log out context ")
    (fn []
      [ui/view {:flex 1
                :flex-direction "column"
                :justify-content "center"
                }
       [components/text-input {:target-property :name
                               :entity @conetxt-map
                               :size :big}]

       [components/small-title "definition"]
       [components/text-input {:target-property :definition
                               :size :small
                               :entity @conetxt-map}]


       [components/add-button {:text "contained contexts"
                               :on-add-click #() }]

       [components/entity-links {:group :context-definitions
                                 :name :contained-contexts
                                 :entity @conetxt-map  }]

       [components/line]

       [components/save]
       ])))