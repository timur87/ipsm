(ns co-act-mobile.entity.ios.interactions
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.components.index :as components]
            [co-act-mobile.ui :as ui]))

(defn interactions-entity []
  (let [interaction-map (subscribe [:nav/current-entity])
        target (subscribe [:data/find [(:group @interaction-map) (:target @interaction-map)]])
        date  (.toString (js/Date. (:time @interaction-map)))
        ]
    ;(println "log out interaction " @interaction-map)
    (fn []
      [ui/view {:flex 1
                :flex-direction "column"
                :justify-content "center"
                }
       [components/line]


       [components/single-link {:title "action:"
                                :display-text (:action @interaction-map)
                                ;:on-press #(dispatch)
                                }]


       [components/line]

       [components/single-link {:title "source:"
                                :display-text (:source @interaction-map)
                                :font-size :middle
                                ;:on-press #()
                                }]

       [components/single-link {:title "target:"
                                :display-text (:name @target)
                                :on-press #(dispatch [:nav/push-entity [(:group @interaction-map) @target]])}]


       [components/single-link {:title "time:"
                                :display-text date}]
       [components/line]
       ])))