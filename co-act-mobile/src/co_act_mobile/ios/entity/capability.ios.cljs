(ns co-act-mobile.entity.ios.capability
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.components.ios.index :as components]
            [co-act-mobile.ui :as ui]))

(defn capability-entity []
  (let [capability-map (subscribe [:nav/current-entity])
        capability-type (subscribe [:static/capability-types])
        ]
    ;(println "log out capability ")
    (fn []
      [ui/view {:flex 1
                :flex-direction "column"
                :justify-content "center"
                }

       [components/small-title "definition"]
       [components/text-input {:target-property :definition
                               :size :small
                               :entity @capability-map}]

       [components/line]

       [components/selector {:options @capability-type
                             :text "type"
                             :selected-value (:type @capability-map)
                             :on-select #(dispatch [:nav/cache {:type %}])}]

       [components/line]

       [components/add-button {:text "contained capablities"
                               :on-add-click #() }]

       (if (not-empty (:contained-capabilities @capability-map))
         [components/entity-links {:group :capability-definitions
                                   :name :contained-capabilities
                                   :entity @capability-map  }])

       [components/add-button {:text "Providing Resources"
                               :on-add-click #() }]

       [components/entity-links {:group :resources
                                 :name :providing-resources
                                 :entity @capability-map}]

       [components/add-button {:text "Desired Resources"
                               :on-add-click #() }]

       [components/entity-links {:group :resources
                                 :name :desired-resources
                                 :entity @capability-map}]

       [components/line]

       [components/save]
       ])))