(ns co-act-mobile.entity.ios.processInstance
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.components.ios.index :as components]
            [co-act-mobile.ui :as ui]))

(defn process-instance-entity []
  (let [instance-map (subscribe [:nav/current-entity])
        process-model (subscribe [:data/find [:process-definitions (:model @instance-map)]])
        instance-state (subscribe [:static/instance-state])
        parent-instance (if (some? (:parent-instance @instance-map))
                                 (subscribe [:data/find [:informal-process-instances (:parent-instance @instance-map)]]))
        ]
    ;(println "log out process definition ")
    (fn []
      [ui/view {:flex 1
                :flex-direction "column"
                :justify-content "center"
                }
       [components/text-input {:target-property :name
                               :entity @instance-map
                               :size :big}]

       [components/small-title "definition"]
       [components/text-input {:target-property :definition
                               :size :small
                               :entity @instance-map}]

       [components/line]

       [components/single-link {:title "Model:"
                                :display-text (:name @process-model)
                                :on-press #(dispatch [:nav/push-entity [:process-definitions @process-model]])}]

       [components/line]

       (if (and (some? parent-instance) (not-empty @parent-instance))
         [components/single-link {:title        "Parent :"
                                  :display-text (:name @parent-instance)
                                  :on-press     #(dispatch [:nav/push-entity [:informal-process-instances @parent-instance]])}])

       [components/single-link {:title        "URL :"
                                :font-size    :small
                                :display-text (:instance-uri @instance-map)
                                :on-press     #()}]

       [components/selector {:options @instance-state
                             :text "State"
                             :selected-value (:instance-state-id  @instance-map)
                             :on-select #(dispatch [:nav/cache {:instance-state-id  %}])}]

       [components/date-picker {:on-change #(dispatch [:nav/cache {:start-date %}])
                                :text "Start Date"
                                :value (:start-date @instance-map)}]

       [components/date-picker {:on-change #(dispatch [:nav/cache {:end-date %}])
                                :text "End Date"
                                :value (:end-date @instance-map)}]


       [components/save]
       ])))