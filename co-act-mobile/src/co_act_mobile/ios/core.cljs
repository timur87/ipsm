(ns co-act-mobile.ios.core
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]
            [co-act-mobile.lib :as lib]
            [co-act-mobile.containers.index :as containers]
            [co-act-mobile.entity.ios.index :as entities]
            [co-act-mobile.components.index :as components]))

(def router (merge {:list containers/listView}
                   ;merge entity map
                   entities/entitiesMap))

(def pullDrawer (fn [_ gestureState]
                  (let [dx (aget gestureState "dx")
                        ;dy (aget gestureState "dy")
                        ;opened (subscribe [:drawer/opened])
                        ]
                    (cond
                      ;close drawer
                      (> dx 20)  (dispatch [:drawer/pull false])
                      ;open drawer
                      (< dx -20) (dispatch [:drawer/pull true])
                      ))))

(def responder (.create ui/pan-responder
                        (clj->js {
                                  :onStartShouldSetPanResponder #(= (count (aget %1 "nativeEvent" "touches")) 1)
                                  ;:onStartShouldSetPanResponderCapture #(true)
                                  ;:onMoveShouldSetPanResponder #(true)
                                  ;:onMoveShouldSetPanResponderCapture #(true)
                                  ;:onPanResponderGrant #(.log js/console "gesture start")
                                  ;:onPanResponderMove #(.log js/console "gesture moving")
                                  :onPanResponderRelease pullDrawer
                                  })))

(defn renderMain [props]
  (let [current (subscribe [:nav/current])]
    [ui/scroll {:style {
                        :margin      0
                      :flex 1
                      :margin-top  (.-HEIGHT ui/navigation-header-comp)

                      }
                :contentContainerStyle {:align-items "stretch"}
                }
     [(get router (:group @current))]]))

(defn nav-title [props]
  (let [current (subscribe [:nav/current])]
    [ui/header-title (:title @current)]))

(defn nav-right [props]
  [ui/button {:inner-style {:margin 10}
              :on-press #(dispatch [:drawer/pull])
              }
   [ui/m-icons {:key "menu" :name "menu" :size 20}]])

(defn header
  [props]
  [ui/navigation-header
   (assoc
     (js->clj props)
     :render-title-component #(r/as-element (nav-title %))
     :render-right-component #(r/as-element (nav-right %)))])

(defn app-root []
  (let [nav (subscribe [:nav/state])
        animate-value (:close-value (subscribe [:drawer]))
        animate-final (ui/animated-value. animate-value)]
    (dispatch [:drawer/init animate-final])
    (fn []
      [ui/view (merge {:style {:flex 1
                               :flexDirection "row"
                               :flex-wrap "nowrap"}}
                      (js->clj (aget responder "panHandlers")))
       [ui/animated-view {:style {:transform [{:translateX animate-final}]
                                  :overflow "visible"
                                  :flex 1}}
        [ui/card-stack {:on-navigate      #(dispatch [:nav/pop nil])
                        :render-overlay   #(r/as-element (header %))
                        :navigation-state @nav
                        :style            {:flex 1}
                        :render-scene     #(r/as-element  (renderMain %))}]]
       [components/drawer]
       ])))

(defn init []
  (dispatch-sync [:initialize-db])
  (.registerComponent ui/app-registry "coActMobile" #(r/reactify-component app-root)))


