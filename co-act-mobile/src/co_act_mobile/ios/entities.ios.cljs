(ns co-act-mobile.entity.ios.index
  (:require [co-act-mobile.entity.ios.intention :refer [intention-entity]]
            [co-act-mobile.entity.ios.strategy :refer [strategy-entity]]
            [co-act-mobile.entity.ios.capability :refer [capability-entity]]
            [co-act-mobile.entity.ios.context :refer [context-entity]]
            [co-act-mobile.entity.ios.processDefinition :refer [process-definition-entity]]
            [co-act-mobile.entity.ios.processInstance :refer [process-instance-entity]]
            [co-act-mobile.entity.ios.interactions :refer [interactions-entity]]
            ))

(def entitiesMap {:intention-definitions intention-entity
                  :strategy-definitions strategy-entity
                  :capability-definitions capability-entity
                  :context-definitions context-entity
                  :process-definitions process-definition-entity
                  :informal-process-instances process-instance-entity
                  :interactions interactions-entity
                  })

(def intention intention-entity) ;only used in android core
(def strategy strategy-entity)
(def capability capability-entity)
(def context context-entity)
(def process-definition process-definition-entity)
(def process-instance process-instance-entity)
(def interactions interactions-entity)
