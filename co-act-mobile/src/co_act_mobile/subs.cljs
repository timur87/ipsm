(ns co-act-mobile.subs
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [re-frame.core :refer [register-sub subscribe]]
            [cljs.spec :as s]
            [co-act-mobile.lib :as lib]))

(register-sub
  :get-greeting
  (fn [db _]
    (reaction
      (get @db :greeting))))

;;;; -- Nav Subs -----------------------------------------------------------

;;;; Index in nav
(register-sub
  :nav/index
  (fn [db _]
    (reaction
      (get-in @db [:nav :index]))))

;;;; First key, home page
(register-sub
  :nav/first-key
  (fn [db _]
    (reaction
      (get-in @db [:nav :children 0 :key]))))

;;;; Whole nav state
(register-sub
  :nav/state
  (fn [db [sid, cid]]
    (reaction
      (get @db :nav))))

;;;; Current nav object
(register-sub
  :nav/current
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (reaction
        (get-in @db [:nav
                     :children
                     @index])))))

;;;; Current entity data
(register-sub
  :nav/current-entity
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (reaction
        (get-in @db [:nav
                     :children
                     @index
                     :entity-data])))))

;;;; Latest entity data
(register-sub
  :nav/latest-entity
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (reaction
        (get-in @db [:nav
                     :children
                     (- @index 1)
                     :entity-data])))))

;;;; Current resource data
(register-sub
  :nav/resource-data
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (reaction
        (get-in @db [:nav
                     :children
                     @index
                     :resource-data])))))

;;;; Resource data in last page
(register-sub
  :nav/last-resource-definitions
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (reaction
        (get-in @db [:nav
                     :children
                     (- @index 1)
                     :resource-data
                     :definitions])))))

;;;; A resource node data
(register-sub
  :nav/resource-node
  (fn [db _]
    (let [index (subscribe [:nav/index])
          id (subscribe[:nav/resource-id])]
      (reaction
        (get-in @db [:nav
                     :children
                     (- @index 1)
                     :resource-data
                     :definitions
                     :nodes
                     @id])))))

;;;; A resource relationship data
(register-sub
  :nav/resource-relationship
  (fn [db _]
    (let [index (subscribe [:nav/index])
          id (subscribe[:nav/resource-id])]
      (reaction
        (get-in @db [:nav
                     :children
                     (- @index 1)
                     :resource-data
                     :definitions
                     :relationships
                     @id])))))

;;;; Current resource id
(register-sub
  :nav/resource-id
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (reaction
        (get-in @db [:nav
                     :children
                     @index
                     :resource-id])))))

;;;; Definitions of current resource model
(register-sub
  :nav/resource-definitions
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (reaction
        (get-in @db [:nav
                     :children
                     @index
                     :resource-data
                     :definitions])))))

;;;; If app is now requsting server
(register-sub
  :nav/requesting
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (reaction
        (get-in @db [:nav
                     :children
                     @index
                     :requesting])))))

;;;; Current pick list data
(register-sub
  :nav/current-pick
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (reaction
        (get-in @db [:nav
                     :children
                     @index
                     :pick-data])))))

;;;; Current keyword of group
(register-sub
  :nav/current-group
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (reaction
        (get-in @db [:nav
                     :children
                     @index
                     :group])))))

;;;; If current in a snapshot page
(register-sub
  :nav/snapshot?
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (reaction
        (some? (get-in @db [:nav
                            :children
                            @index
                            :snapshot]))))))


;;;; -- UI Subs -----------------------------------------------------------

;;;; All the list meta information
(register-sub
  :main-list
  (fn [db _]
    (reaction
      (get-in @db [:main-list]))))

;;;; All the drawer information
(register-sub
  :drawer
  (fn [db _]
    (get-in @db [:drawer])))

;;;; If drawer is open?
(register-sub
  :drawer/opened
  (fn [db _]
    (get-in @db [:drawer :opened])))

;;;; Get target-intention for certain strategy id
(register-sub
  :target-intention
  (fn [db [_ strategy-id]]
    {:pre [(s/valid? string? strategy-id)]}
    (reaction
      (some
        (fn[intention]
          (when (some #(= strategy-id %) (:active-strategy intention))
            intention))
        (vec (vals (get-in @db [:data :intention-definitions]))))
      )))

;;;; Get multiple target intentions
(register-sub
  :target-intention-multi
  (fn [db [_ strategy-id]]
    {:pre [(s/valid? string? strategy-id)]}
    (reaction
      (let [intentions (vec (vals (get-in @db [:data :intention-definitions])))
            get-intention (fn [targets intention]
                            (let []
                              ;here we don't use active-strategy
                              (if (some #(= strategy-id %) (:strategy-definitions intention))
                                (conj targets intention)
                                targets)))
            resources (vec (distinct (reduce get-intention [] intentions)))
            ]
        resources)

      )))

;;;; Get related resources for current entity
(register-sub
  :related-resources
  (fn [db [_ intention]]
    (let [get-resource (fn [resources capability-id]
                         (let [capability (get-in @db [:data :capability-definitions (keyword capability-id)])]
                           (into resources (:desired-resources capability))))
          resources (vec (distinct (reduce get-resource [] (:organizational-capabilities intention))))]
      (reaction (mapv #(get-in @db [:data :service-templates (keyword %)]) resources))
      )))

;;;; Get prerequisite intentions for certain intention
(register-sub
  :related-intentions
  (fn [db [_ intention]]
    ; can't use pre : the nav will changed before the subscribe change!
    ;{:pre [(s/valid? some? intention)]}
    (let [activ-strategy  (if (not-empty (:active-strategy intention))
                            (get-in @db [:data :strategy-definitions (keyword (nth (:active-strategy intention) 0))]))
          ]
      (reaction
        (filterv
          (fn[intention]
            (some #(= (:id intention) %) (:contained-intentions activ-strategy)))
          (vec (vals (get-in @db [:data :intention-definitions]))))
        ))))

;;;; The new post data
(register-sub
  :ui/new-post
  (fn [db _]
    (reaction
      (get-in @db [:new-post]))))

;;;; -- Data Subs -----------------------------------------------------------

;;;; All the entity data
(register-sub
  :data
  (fn [db _]
    (reaction
      (get-in @db [:data]))))

;;;; Find a bunch of items in one group
(register-sub
  :data/find-group
  (fn [db [_ [search-array target-group]]]
    {:pre [(s/valid? (s/* string?) search-array)
           (s/valid? keyword? target-group)]}
    ; can't use pre : the nav will changed before the subscribe change!
    ;{:pre [(s/valid? some? intention)]}
    (reaction
      (filterv
        (fn[entity]
          (some #(= (:id entity) %) search-array))
        (vec (vals (get-in @db [:data target-group]))))
      )))


;;;; Find certain item in one group
(register-sub
  :data/find
  (fn [db [_ [group id]]]
    {:pre [(s/valid? string? id)
           (s/valid? keyword? group)]}
    (reaction
      (get-in @db [:data group (keyword id)]))))

;;;; Find entity by id
(register-sub
  :data/find-keyword
  (fn [db [_ [group id]]]
    {:pre [(s/valid? keyword? id)
           (s/valid? keyword? group)]}
    (reaction
      (get-in @db [:data group id]))))

;;;; Current user name
(register-sub
  :data/find-user-name
  (fn [db [_ id]]
    {:pre [(s/valid? string? id)]}
    (reaction
      (get-in @db [:data :participants (keyword id) :name]))))

;;;; A certain resource data by group and id
(register-sub
  :data/resource-definition
  (fn [db [_ [group id]]]
    {:pre [(s/valid? keyword? group)
           (s/valid? keyword? id)]}
    (let []
      (reaction
        (get-in @db [:data
                     group
                     id])))))

;;;; Return group of one kind of entity data
(register-sub
  :data/group
  (fn [db [_ group]]
    {:pre [(s/valid? keyword? group)
           ]}
    (let []
      (reaction
        (get-in @db [:data
                     group])))))

;;;; -- User Subs -----------------------------------------------------------

;;;; All User data
(register-sub
  :user
  (fn [db _]
    (reaction
      (get-in @db [:user]))))

;;;; If user has login?
(register-sub
  :user/login
  (fn [db _]
    (reaction
      (get-in @db [:login]))))

;;;; Subscribed entities
(register-sub
  :user/subscribe
  (fn [db _]
    (reaction
      (get-in @db [:user :subscribe]))))

;;;; If user subscribed current entity
(register-sub
  :user/subscribe?
  (fn [db [_ _]]
    (let [index (subscribe [:nav/index])
             entity(get-in @db [:nav
                               :children
                               @index
                               :entity-data])
             list (get-in @db [:user :subscribe])]
      (if (some? entity)
        (reaction (contains? list (keyword (:id entity))))
        (reaction false)
        )
      )))

;;;; Privilege information for current enitty
(register-sub
  :user/privilege
  (fn [db _]
    (reaction
      true)))

;;;; -- Static Subs -----------------------------------------------------------

;;;; All the strings
(register-sub
  :strings
  (fn [db _]
    (reaction
      (get-in @db [:strings]))))

;;;; Find target group of definition for instance
(register-sub
  :static/target-definition
  (fn [db [_ _]]
    (let [group (subscribe [:nav/current-group])]
      (reaction
        (get-in @db [:target-definition-map @group])))))

;;;; Priority levels vector
(register-sub
  :static/priority-level
  (fn [db _]
    (reaction
      (get-in @db [:static :priority-level]))))

;;;; Completion levels vector
(register-sub
  :static/completion-level
  (fn [db _]
    (reaction
      (get-in @db [:static :completion-level]))))

;;;; Priority level colors vector
(register-sub
  :static/priority-color
  (fn [db _]
    (reaction
      (get-in @db [:static :priority-color]))))

;;;; Capability types vector
(register-sub
  :static/capability-types
  (fn [db _]
    (reaction
      (get-in @db [:static :capability-types]))))

;;;; Instace state vector for certain kind of entity
(register-sub
  :static/instance-state
  (fn [db [_ [group]]]
    {:pre [(s/valid? keyword? group)]}
    ;(println "group is" group)
    (reaction
      (get-in @db [:static :instance-state-map group]))))

;;;; Privilege levels vector
(register-sub
  :static/privilege-level
  (fn [db _]
    (reaction
      (get-in @db [:static :privilege-level]))))

;;;; Entities vector which could be add by user
(register-sub
  :static/addable-list
  (fn [db _]
    (reaction
      (get-in @db [:static :addable-list]))))

;;;; Host server addrss
(register-sub
  :static/host
  (fn [db _]
    (reaction
      (get-in @db [:static :host]))))