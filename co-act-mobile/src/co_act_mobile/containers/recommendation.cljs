(ns co-act-mobile.containers.recommendation
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.android.ui :as ui]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.lib :as lib]
            [co-act-mobile.android.components.index :as components]
            ))

(def style {:view {:padding (-> common :component :padding)}
            :text {:font-size (-> common :text :small)
                   :color (:icon color)
                   :font-weight "bold"}})

(defn recommendation-container
  "Generate recommendation page"
  []
  (let [informal-process (subscribe [:nav/latest-entity])
        completion-level (subscribe [:static/completion-level])]
    ;(println "log out interaction " @interaction-map)
    (fn []
      [ui/scroll {:style{:flex 1
                         :flex-direction "column"}
                  :contentContainerStyle {:justify-content "center"}
                  :showsVerticalScrollIndicator false}

       [components/small-title "Name"]
       [components/text-input {:target-property [:interactive-entity-definition
                                                 :identifiable-entity-definition
                                                 :entity-identity
                                                 :name]
                               :size :small
                               :entity @informal-process}]
       [components/add-button {:text "Target namespace"
                               :entity @informal-process
                               :add-name [:interactive-entity-definition
                                          :identifiable-entity-definition
                                          :entity-identity
                                          :target-namespace]
                               :add-group :namespaces
                               :require true
                               :multi false
                               :display-property :namespace}]
       [components/entity-link {:group :namespaces
                                :name [:interactive-entity-definition
                                       :identifiable-entity-definition
                                       :entity-identity
                                       :target-namespace]
                                :require true
                                :display :namespace
                                :entity @informal-process}]
       [components/line]
       [components/selector {:options @completion-level
                             :text "Completion"
                             :selected-value 0
                             :on-select #()}]


       [components/line]
       [components/upload]
       ])))