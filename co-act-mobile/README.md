# Co-Act-Mobile
=============



## File System
=============

Android and iOS folder include files specifically used in certain platform, as Android and iOS have complete different user interface management and utilize variant controller logic in some components like {date picker} or {selector}. Though React Native uses some consistent UI components like {View} and {Image}, there is still several platform-specified components. For instance, a progress bar is named as {ProgressViewIOS} in iOS and {ProgressBarAndroid} in Android. In addition, the control logic is also different, thus we need to provide separate environments to manage files.

In our application, we have categorized these files and try to keep a consistent structure. Android and iOS folder specified the files only works in corresponding platform, other files under the root are used in both platforms as common required files. To be more specific:

* Components: include all the customized and highly reusable UI components in our application. Passed arguments customize the target component, for example, {entityLinks} is a component listing a bunch of entities. It lists the providing list in capability, it also list achieving strategies in intention definition, just by passing different {group} value and a {name} value. Group indicates the category of entity like resource and strategy definition, name is used to inform the property name, accordingly providing list and archiving strategies. Similarly, we have a privilege control component in every entity, it acts the same according to the entity page even without passing any arguments.

* Entity: contains the template for all the entities, according to entities' {group} attribute,our router populates the templates with their {content} value.

* Containers: refer to the templates which are used to render categorized pages other than entity pages, namely, {list} page for enumerating all the entity in one group, and {pick} list for showing enabled pick candidates of certain property. For instance, user clicks on the add button attached with {contained capabilities} attribute in a strategy definition, application will direct user to this pick page, and show all the possible capabilities to be select.

* core.cljs: is the entrance for the whole application, Android, iOS each owns a {core.cljs} file indexing other related files. The main control logic is also defined here.

* ui.cljs: index the official React Native components here, these native elements are the underlying parts for building our customized components. Indeed, they are the most smallest UI parts to compose our application. For example, view element is used frequently in our application to form a display block, just like {<div>} used in HTML programming, text input element is used in any place to let user change the text information.

* lib.cljs: offers helper method with different UI components. For example, the alert functions in Android and iOS have different arguments numbers.

* db.cljs: is the central place where we put all the application data including all static data and variant entity content.

* subs.cljs: lists all the subscribed paths which bind the UI elements and their corresponding value in database.

* handlers.cljs: includes the event handlers respond to user actions, they are the interfaces directly interact with UI components.

* scehma.cljs: defines the schema for our database, it offers an holistic view for the type of every element in database. Additionally, each handler makes change only after schema file validating the new value passed in.

* style.cljs: offers a central management for the styles we used in application, all the common used value like colour codes, and font sizes will be managed here, in order to keep a consistent user interface.

* api.cljs: contains the required helper functino to connect with remote server and parser the xml format respond to fit the application.

## Quick Start
=============

##### Building

clone the repository

```
$ git clone https://gitlab.com/hanwencheng/co-act-mobile
```

NOTICE: make sure use a later version node

install react-native

```
$ npm install -g react-native-cli
```

install npm dependencies

```
$ npm install
```

##### Prepare iOS

install watchman

```
$ brew install watchman
```

get xcode 7.0 or higher

xcode -> Open Developer Tool -> Simulator

After Simulator start, you may change the device by Hardware -> Device

use figwheel

```
$ re-natal use-figwheel

$ lein figwheel ios
```

start react-native server

`$ react-native start`

start the project on iOS

`$ react-native run-ios`

##### Prepare Android

There are more steps, but just the same as [official Guide](https://facebook.github.io/react-native/docs/getting-started.html)

After that

NOTICE : map the port (if the program is running on real device)
```
$ adb reverse tcp:8081 tcp:8081

$ adb reverse tcp:3449 tcp:3449
```
use figwheel

```
$ re-natal use-android-device <real|genymotion|avd>

$ re-natal use-android-device genymotion

$ re-natal use-figwheel

$ lein figwheel android

```

start react-native server

`$ react-native start`

start the project on Android

```
$ react-native run-android

$ react-native bundle -dev false --platform android --entry-file index.android.js --bundle-output ./android/app/build/intermediates/assets/debug/index.android.bundle --assets-dest ./android/app/build/intermediates/res/merged/debug
```


##### Testing! (In Android)


build production version

```
$ lein prod-build
```

generate android unsigned apk

1. Bundle debug build:

```
react-native bundle --dev false --platform android --entry-file index.android.js --bundle-output ./android/app/build/intermediates/assets/debug/index.android.bundle --assets-dest ./android/app/build/intermediates/res/merged/debug
```

2. Create debug build:

```
$ cd android
$ ./gradlew assembleDebug
```

## Dependencies Q & A
===================

In normal case, with the latest rnpm

```
npm install -g rnpm
```

dependencies could be easily linked by

```
$ rnpm link
```

If there is still problems, please refer to the following solutions.

* Please check latest version of [react-native-maps](https://github.com/airbnb/react-native-maps) if there is any related building problem, mostly it is caused by the compatibility of react native and this library. To be mentioned here we use 0.6.0 version.

* Check the latest version of [react-native-vector-icons](https://github.com/oblador/react-native-vector-icons) repository for install instructions, extra icons package need to be import to Android and iOS folders. Android icons file could be found under `android/app/src/main/assets/font`.

* Check the latest version of [react-native-push-notifications](https://github.com/zo0r/react-native-push-notification) about the detail install instruction.

* If there is a warning box of Warning: Failed propType: SceneView: prop type `sceneRendererProps` is invalid, it is the problem of react native 0.26.0, please add the following line to YellowBox block in `/node_modules/react-native/Libraries/ReactIOS/YellowBox.js`
```
console.ignoredYellowBox = ['Warning: Failed propType'];
```

## License
===================

This product includes software from the project "Supporting Organizational Goals with Mobile Apps" which was originally developed by Hanwen Cheng as a Master thesis at the Institute of Architecture of Application Systems (IAAS) of the University of Stuttgart from March to October 2016. For more information see http://www.iaas.uni-stuttgart.de



