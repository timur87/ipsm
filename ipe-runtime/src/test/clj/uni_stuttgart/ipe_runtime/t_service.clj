(ns uni-stuttgart.ipe-runtime.t-service
  (:require [midje.sweet :as t]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protocols]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
            [de.uni-stuttgart.iaas.ipsm.ipe-runtime.service :as s]))



;; (defonce app (user/go))
;; (def persistence (puppetlabs.trapperkeeper.app/get-service app :PersistenceProtocol))
;; (def ipe-runtime (puppetlabs.trapperkeeper.app/get-service app :IPERuntime))
;; (def process-model-id "8a6589a4be864fc7d041006b")



;; (def service-impl (s/get-ipe-runtime-service-instance {:persistence persistence :uri (constants/property :uri)}))


;; (def docker-process-example (slurp "../examples/ipe-models/tosca-based-informal-process-definition-sample.xml"))

;; (def docker-process-example-edn (slurp "../examples/ipe-models/tosca-based-informal-process-definition-sample.edn"))
;; (spit "../examples/ipe-models/tosca-based-informal-process-definition-sample.xml" (de.uni-stuttgart.iaas.ipsm.ipe.model.transformations/get-informal-process-definition-in-xml {:entity-data (read-string docker-process-example-edn)}))
;; (def docker-process-instance-example (slurp "../examples/ipe-models/tosca-based-informal-process-instance-sample.xml"))
;; (def docker-process-instance-engaged-example (slurp "../examples/ipe-models/tosca-based-informal-process-instance-engaged-sample.xml"))
;; (def docker-process-instance-removal-example (slurp "../examples/ipe-models/tosca-based-informal-process-instance-removal-sample.xml"))
;; (def docker-process-instance-addition-example (slurp "../examples/ipe-models/tosca-based-informal-process-instance-addition-sample.xml"))
;; (def process-id "test-process-id")


;;                                         ; (protocols/terminate-process-execution! ipe-runtime "2d36c2e4e1bdbb1c9ee5723a")







;; (t/facts "about ipe-runtime"
;;          (.createIPEInstance service-impl docker-process-example (de.uni-stuttgart.iaas.ipsm.utils.conversions/create-unique-id))
;;          ;; (.createIPEInstance service-impl "Invalid string" "process-id")
;;          (.engageResources service-impl docker-process-instance-example)
;;          )
;; (def curr-process-id (protocols/init-informal-process-model! ipe-runtime docker-process-example))



;; (print (protocols/get-process-instance ipe-runtime curr-process-id))
;; (protocols/list-running-processes ipe-runtime)

;; (->> docker-process-instance-removal-example
;;      (protocols/update-process-instance! ipe-runtime))

;; (->> docker-process-instance-addition-example
;;      (protocols/update-process-instance! ipe-runtime))

;; (->> (protocols/list-running-processes ipe-runtime)
;;      (mapv #(future (protocols/terminate-process-execution! ipe-runtime %))))


;; (print (de.uni-stuttgart.iaas.ipsm.ipe.model.transformations/get-map-of-xml-element-str ))
;; (protocols/terminate-process-execution! ipe-runtime curr-process-id)
;; (clojure.pprint/pprint (protocols/list-processes ipe-runtime))

;; (print (protocols/terminate-process-execution! ipe-runtime curr-process-id))


;; (.terminateInstanceSuccessfully service-impl (protocols/get-process-instance ipe-runtime curr-process-id))
