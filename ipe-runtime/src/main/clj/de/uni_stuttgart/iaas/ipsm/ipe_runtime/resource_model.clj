(ns de.uni-stuttgart.iaas.ipsm.ipe-runtime.resource-model
  (:require [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.resource-model :as resource-model]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
            [dire.core :as dire]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.transformations :as transformations]
            [de.uni-stuttgart.iaas.ipsm.utils.communications :as communications]
            [clj-http.client :as client]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protocols]
            [clojure.java.io :as io]
            [overtone.at-at :as at]
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                          logf tracef debugf infof warnf errorf fatalf reportf
                          spy logged-future with-log-level with-logging-config
                          sometimes)]))

(def ^:private in-memory-resource-models (atom {}))
(def ^:private in-memory-resource-models-lock (Object.))

(def resources-handler-path "/resources/")

;; caching
(defn- add-in-memory-entity-to-cache!
  [m]
  {:pre [(:id (:graph-data m))]}
  (debug "Updating cache with the following data" )
  (swap! in-memory-resource-models assoc-in [(:entity-type (:graph-data m)) (keyword (:id (:graph-data m)))] (:graph-data m)))

(defn- delete-entity-from-cache!
  [m]
  {:pre [(:id (:graph-data m)) (:entity-type (:graph-data m))]}
  (swap! in-memory-resource-models update-in [(:entity-type (:graph-data m))]
         #(dissoc % (keyword (:id (:graph-data m))))))

(defn- add-in-memory-entity-to-persistence!
  [m]
  {:pre [(:persistence m) (:graph-data m)]}
  ;(debug (:persistence m))
  (protocols/delete-entity-by-id! (:persistence m) (:graph-data m))
  (protocols/add-entity! (:persistence m) (:graph-data m)))

(defn- delete-entity-from-persistence!
  [m]
  {:pre [(:persistence m) (:graph-data m)]}
  (protocols/delete-entity-by-id! (:persistence m) (:graph-data m)))

(defn- remove-resource-model-from-cache-and-persistence!
  [m]
  (delete-entity-from-cache! m)
  (delete-entity-from-persistence! m))

(defn- get-graph-data-from-memory
  [m]
  {:pre [(:graph-data m) (:id (:graph-data m)) (:entity-type (:graph-data m))]}
  (let [target-key (keyword (:id (:graph-data m)))] ;; process state contains the key or document type first checks the cache then persistence store
    (if (and (get @in-memory-resource-models (:entity-type (:graph-data m)))
             (target-key (get @in-memory-resource-models (:entity-type (:graph-data m)))))
      (target-key (get @in-memory-resource-models (:entity-type (:graph-data m))))
      nil)))



(defn- get-graph-data-from-ds
  [m]
  {:pre [(:graph-data m) (:persistence m)]}
  (protocols/get-entity (:persistence m) (:graph-data m)))

(defn- get-graph-data-from-cache-or-persistence!
  [m]
  (let [cache-val (get-graph-data-from-memory m)]
    (if cache-val
      cache-val
      (let [ds-val (get-graph-data-from-ds m)]
        (if ds-val
          (let [ret-val (assoc m :graph-data ds-val)]
            (add-in-memory-entity-to-cache! m)
            ret-val)
          nil)))))

(defn- add-target-resource
  [m]
  {:pre [(:index m) (:graph-data m) (:acquirement-vector (:graph-data m))]}
  (assoc m :target-resource (get (:acquirement-vector (:graph-data m)) (:index m))))

(defn- add-nodes-relationships-map
  [m]
  (debug "Adding nodes and relationships..." )
  (assoc m :resources-relationships-map (resource-model/resource-graph->resources-relationships-map m)))

(defn- increment-index
  [m]
  {:pre [(:index m)]}
  (update-in m [:index] inc))

(defn- remove-resource-identified-by-index
  [acquirement-vector index]
  (vec (concat (subvec acquirement-vector 0 index) (subvec acquirement-vector (inc index) (count acquirement-vector)))))


(defn- update-acquirement-vector
  [m]
  {:pre [(:index m) (:entity-data m) (:acquirement-vector (:graph-data m))]}
  ;; (debug "Updating acquirement vector" (:acquirement-vector (:entity-data m)))
  ;; (debug "Index" (:index m))
  ;; (debug "After update" (:acquirement-vector (:entity-data (update-in m [:entity-data :acquirement-vector]
  ;;                                                                     #(remove-resource-identified-by-index % (:index m))))))
  (update-in m [:graph-data :acquirement-vector]
             #(remove-resource-identified-by-index % (:index m))))


(defn- add-new-initializing-resource
  [m]
  {:pre [(:target-resource m)]}
  (->> (assoc m :new-state :initializing)
       domain-language/update-resource-instance-state
       :target-resource
       (assoc m :new-resource)))

(defn- add-new-removed-resource
  [m]
  {:pre [(:target-resource m)]}
  (->> (assoc m :new-state :removed)
       domain-language/update-resource-instance-state
       :target-resource
       (assoc m :new-resource)))

(defn- update-resource-graph-with-initializing-resource
  [m]
  ;; (debug "Resource graph will be updated" m)
  (-> m
      add-new-initializing-resource
      resource-model/update-target-resource-in-graph))

(defn- persist-changes-in-cache-and-persistence!
  [m]
  (debug "Persisting changes in cache and persistence storage")
  (add-in-memory-entity-to-cache! m)
  (add-in-memory-entity-to-persistence! m))

(defn- add-resource-neighbours
  [m]
  (-> m
      resource-model/add-dependent-resources
      resource-model/add-inbound-relationships
      resource-model/add-outbound-relationships
      resource-model/add-preconditioner-relationships
      resource-model/add-postconditioner-relationships))

(defn- resource-initialized?
  [rm]
  (or
   (= ":engaged" (domain-language/get-resource-state rm))
   (= :engaged (domain-language/get-resource-state rm))))

(defn- resource-removed?
  [rm]
  (or
   (= ":removed" (domain-language/get-resource-state rm))
   (= :removed (domain-language/get-resource-state rm))))

(defn- resource-acquirement-complete?
  [rm]
  (or
   (resource-initialized? rm)
   (resource-removed? rm)))

(defn- return-true-if-all-are-true-or-empty
  [v]
  (case (count v)
    0 true
    1 (first v)
    (reduce
     #(and %1 %2)
     v)))


(defn- all-dependencies-are-initialized?
  [m]
  {:pre [(:dependencies m)]}
  (->> (:dependencies m)
       (mapv resource-initialized?)
       return-true-if-all-are-true-or-empty))

(defn- to-be-removed?
  [m]
  (or
   (= (domain-language/get-resource-state (:target-resource m)) ":to-be-removed")
   (= (domain-language/get-resource-state (:target-resource m)) :to-be-removed)))

(defn- engaged?
  [m]
  (or
   (= (domain-language/get-resource-state (:target-resource m)) ":engaged")
   (= (domain-language/get-resource-state (:target-resource m)) :engaged)))

(defn- get-callback-address
  [m]
  {:pre [(:process-id m) (:uri m)]}
  (if (.endsWith (str (:uri m)) "/")
    (str (str (:uri m)) resources-handler-path  (:process-id m))
    (str (str (:uri m)) "/" resources-handler-path  (:process-id m))))

(defn- get-resource-handler-path
  []
  (str resources-handler-path ":id"))


(defn- create-initialization-parts
  [m]
  {:pre [(:target-resource m)
         (:dependencies m)
         (:dependents m)
         (:postconditioner-relationship-definitions m)
         (:preconditioner-relationship-definitions m)
         (:operation-type m)]}
  (filterv
   #(:content %)
   [{:name (name :operation-request)
     :content (str (domain-language/operation-message
                    {:process-id (:process-id m)
                     :informal-process-instance (:entity-data m)
                     :target-resource (:target-resource m)
                     :dependencies {:dependency (:dependencies m)}
                     :operation-type (:operation-type m)
                     :preconditioner-relationships {:preconditioner-relationship (:preconditioner-relationship-definitions m)}}))}
    {:name (name :callback-address) :content (get-callback-address m)}]))

(defn- create-release-parts
  [m]
  {:pre [(:target-resource m)]}
  (filterv
   #(:content %)
   [{:name (name :operation-request)
     :content (str (domain-language/operation-message
                    {:process-id (:process-id m)
                     :informal-process-instance (:entity-data m)
                     :target-resource (:target-resource m)
                     :operation-type (constants/property :release-resource-operation-name)}))}
    {:name (name :callback-address) :content (get-callback-address m)}]))



(defn- add-request-body
  [m]
  {:pre [(:multipart m)]}
  (debug "Adding request body")
  (assoc m :request-body
         {:multipart (:multipart m)}))

(defn- add-initialization-multipart
  [m]
  (assoc m :multipart (create-initialization-parts m)))

(defn- add-release-multipart
  [m]
  (debug "Adding release parts")
  (assoc m :multipart (create-release-parts m)))

(defn- post-request!
  [m]
  {:pre [(:request-body m)]}
  (debug "Posting the request for the resource operation" (:operation-type m))
  (if (:recipient-address m)
    (client/post (:recipient-address m) (:request-body m))
    (warn "Operation on the resource" (:target-resource m) "is not supported" (:operation-type m))))

(defn- add-recipient-address
  [m]
  {:pre [(:matching-ee-integrator m)]}
  (debug "Matching eei" (:matching-ee-integrator m))
  (assoc m :recipient-address (:matching-ee-integrator m)))

(defn- submit-resource-initialization-request!
  [m]
  {:pre [(:target-resource m)]}
  (doto (-> m
            add-recipient-address
            add-initialization-multipart
            add-request-body)
    post-request!))


(defn- add-matching-ee-integrator
  [m]
  {:pre [(:persistence m) (:target-resource m)]}
  (debug "Adding matching iee")
  (->> m
       domain-language/add-execution-environment
       :matching-ee-integrator
       (assoc m :matching-ee-integrator)))

(defn- prepare-for-initialization
  [m]
  (-> m
      (assoc :operation-type (constants/property :acquire-resource-operation-name))
      add-matching-ee-integrator
      add-resource-neighbours
      update-acquirement-vector
      add-new-initializing-resource
      update-resource-graph-with-initializing-resource))

(defn- prepare-for-skip
  [m]
  (-> m
      update-acquirement-vector))

(defn- prepare-for-removal
  [m]
  (debug "Preparing for removal")
  (-> m
      (assoc :operation-type (constants/property :release-resource-operation-name))
      add-matching-ee-integrator
      update-acquirement-vector
      add-new-removed-resource
      resource-model/update-target-resource-in-graph))

(defn- initialize-resource!
  [m]
  (debug "Resource will be requested with index" (:index m))
  (doto m
    submit-resource-initialization-request!
    persist-changes-in-cache-and-persistence!))

(defn- add-lifecylce-interface-as-target-interface
  [m]
  {:post [(:target-interface %)]}
  (debug "Adding life-cycle interface as target interface")
  (assoc m :target-interface (constants/property :life-cycle-interface-uri)))


(defn- release-resource!
  [m]
  (debug "Releasing resource!" m)
  (doto (-> m
            (assoc :operation-type (constants/property :release-resource-operation-name))
            add-lifecylce-interface-as-target-interface
            add-matching-ee-integrator
            add-recipient-address
            add-release-multipart
            add-request-body)
    post-request!))

(declare iterate-through-acquriement-vector!)

(defn- start-initialization-or-proceed!
  [m]
  (cond
    (to-be-removed? m)
    (doto (prepare-for-removal m)
      release-resource!
      iterate-through-acquriement-vector!)
    (engaged? m)
    (doto (prepare-for-skip m)
      iterate-through-acquriement-vector!)
    (all-dependencies-are-initialized? m)
    (doto (prepare-for-initialization m)
      initialize-resource! ;; do not increment the index as one resource will be removed
      iterate-through-acquriement-vector!)
    :else
    (iterate-through-acquriement-vector! (increment-index m))))


(defn- resources-acquirement-complete?
  [rv]
  (debug "Checking if resources-acquirement-complete..." rv)
  (not (seq (remove
             resource-acquirement-complete?
             rv))))

(defn- update-resource-model-of-informal-process-instance
  [m]
  {:pre [(:entity-data (:graph-data m)) (:resources-relationships-map m)]}
  (->> {:entity-data (:entity-data (:graph-data m))
        :new-vals (or (into (:relationships (:resources-relationships-map m))
                            (:resources (:resources-relationships-map m)))
                      [])}
       (into m)
       domain-language/replace-resources-or-relationships-of-informal-process))

(defn complete-resource-acquirement!
  [m]
  {:pre [(:resource-model-init-succ-fn! m)]}
  (debug "Completing resource acquirement" )
  ((:resource-model-init-succ-fn! m) m))

(defn- iterate-through-acquriement-vector!
  [m]
  {:pre [(:index m) (:graph-data m) (:acquirement-vector (:graph-data m))]}
  (debug "Iterating throug" (:index m) (:acquirement-vector (:graph-data m)))
  (if (< (:index m) (count (:acquirement-vector (:graph-data m))))
    (doto (-> m
              add-target-resource
              resource-model/add-dependency-resources)
      start-initialization-or-proceed!)
    (let [iteration-res (some->> m
                                 add-nodes-relationships-map
                                 :resources-relationships-map
                                 :resources
                                 resources-acquirement-complete?
                                 (assoc m :acquirement-complete?)
                                 add-nodes-relationships-map
                                 update-resource-model-of-informal-process-instance)]
      (if iteration-res
          (complete-resource-acquirement! iteration-res)))))

(defn- initialize-next-resource!
  "Return next initializble resource, change its state and save it, or if not found return nil. Acquiers a lock"
  [m]
  {:pre [(:process-id m) (:persistence m)]}
  (debug "Initializing next resource " )
  (locking in-memory-resource-models-lock
    (let [fetched-map (get-graph-data-from-cache-or-persistence! m)]
      (-> m
          (assoc :graph-data fetched-map)
          (assoc :index 0)
          iterate-through-acquriement-vector!))))

(defn add-precondioner-relationships
  [m]
  (assoc
   m
   :preconditioner-relationship-definitions
   (vec
    (reduce
     #(= (:entity-type) :preconditioner-relationship-definition)
     (:relationships m)))))

(defn- get-precondioned-resources
  [v]
  (reduce
   #(= (:entity-type) :preconditioner-relationship-definition)
   v))


(defn- deployable-exists?
  [resource-map folder-path]
  ;; (debug "Existence of the following deployable will be controlled:" resource-map)
  ;; (debug "Root path is" folder-path)
  (let [deployable-path (:deployable-path (:entity-map (:deployment-descriptor (:entity-map resource-map))))]
    (if (and deployable-path (.exists (io/file (str folder-path "/" deployable-path))))
      true
      false)))


(defn- deployable-stream
  [resource-map folder-path]
  (let [deployable-path (:deployable-path (:entity-map (:deployment-descriptor (:entity-map resource-map))))]
    (if (and deployable-path (.exists (io/file (str folder-path "/" deployable-path))))
      (io/input-stream (io/file (str folder-path "/" deployable-path)))
      nil)))

(defn- bring-all-deployables-together
  [m]
  (assoc m :deployables (vec
                         (filter
                          #(deployable-exists? % (:root-folder-path m))
                          (concat [(:target-resource m)]
                                     (:dependencies m)
                                     (:dependents m)
                                     (:inbound-relationships m)
                                     (:outbound-relationships m))))))


(defn- create-entiries-for-deployables
  [m]
  (:pre [(:deployables m)])
  ;; (debug "Creating file entries for the following files" (:deployables m))
  (assoc m :file-entries (mapv
                          #(hash-map :name (:id (:entity-map %)) :content-type constants/deployable-media-type  :content (deployable-stream % (:root-folder-path m)))
                          (:deployables m))))



(defn- create-file-entries
  [m]
  (-> m
      bring-all-deployables-together
      create-entiries-for-deployables
      :file-entries))

;;

 ;; (defn- prepare-and-initialize-resource!
 ;;  [m]
 ;;  (doto (-> m
 ;;            prepare-resource-for-initialization)
 ;;    initialize-resource!))

;; (defn- initialize-resource-iteratively!
;;   [m]
;;   {:pre [(:acquirement-vector m) (vector? (:acquirement-vector m))] }
;;   (let [resource-count (count (:acquirement-vector m))]
;;     (loop [index 0]
;;       (if (< index resource-count)
;;         (prepare-and-initialize-resource! (-> m
;;                                               (assoc :index index)
;;                                               add-target-resource))))))
                                        ;

;; (defn- initialize-resources!
;;   [m]
;;   {:pre [(:resource-model m)]}
;;   (doto (-> m
;;             resource-model/resource-model->acquirement-vector)
;;     (initialize-resource-iteratively!)))


(defn- persist-resource-graph!
  [m]
  {:pre [(:persistence m) (:resource-model m)]}
  (protocols/add-entity! (:persistence m) (:resource-model m)))


(defn- update-entity-type
  [m]
  (assoc-in m [:graph-data :entity-type] :initializing-informal-process-instance))

(defn- add-process-state
  [m]
  (assoc-in m [:entity-data :instance-descriptor :instance-state] :initializing))

(defn- add-process-id
  [m]
  {:pre [(:process-id m)]
   :post [(:id (:graph-data %))]}
  (assoc-in m [:graph-data :id] (:process-id m)))

(defn- add-process-model
  [m]
  {:pre [(:entity-data m)]}
  (assoc-in m [:graph-data :entity-data] (:entity-data m)))





(defn initialize-resource-model!
  [m]
  (debug "Resources will be initialized for the map" )
  (doto (-> m
            resource-model/add-graph-for-informal-process
            resource-model/mark-additional-to-be-removed-resources
            resource-model/add-acquirement-vector
            add-lifecylce-interface-as-target-interface
            update-entity-type
            add-process-id
            add-process-model
            add-process-state)
    (persist-changes-in-cache-and-persistence!)
    (initialize-next-resource!))
  nil)



(defn- add-operation-type-from-message
  [m]
  (debug "Adding operation type" (:operation-message m))
  (if (some-> m :operation-message :operation-type)
    (assoc m :operation-type (some-> m :operation-message :operation-type))
    (error "Operation type was not specified")))

(defn- add-process-id-from-params
  [m]
  (if (or (:id (:params m)) (:process-id (:operation-message m)))
    (assoc m :process-id (or (:id (:params m)) (:process-id (:operation-message m))))
    (error "Missing process id!!!")))

(defn- add-request-params
  [m]
  {:pre [(:message m)]}
  ;; (debug "Adding request params" (:params (:request (:message m))))
  (-> m
      (assoc :params (:params (:request (:message m))))))

(defn- add-content-type
  [m]

  (->> m
       :message
       :request
       :content-type
       (assoc m :content-type)))

(defn- add-resource-instance-state
  [m]
  (if (:instance-state (:operation-message m))
    (assoc m :instance-state (:instance-state (:operation-message m)))
    (error "Resoruce state is not specified")))

(defn- add-resource-instance-location
  [m]
  (if (:instance-location (:operation-message m))
    (assoc m :instance-location (:instance-location (:operation-message m)))
    (do
      (error "Resource instance location is not specified")
      m)))

(defn- add-new-resource-from-parameters
  [m]
  {:pre [(:params m) (:content-type m)]}
  (if (:target-resource (:operation-message m))
    (case (:content-type m)
      "application/xml" (assoc m :new-resource (error "Not yet implemented"))
      "application/edn" (assoc m :new-resource (:target-resource (:operation-message m))))
    (error "resource-instance must be specified")))


(defn- add-resource-graph
  [m]
  ;; (debug "Adding resoruce graph" m)
  (assoc m :graph-data (get-graph-data-from-cache-or-persistence! m)))


(defn- add-target-resource-from-resource-id
  [m]
  (assoc m :target-resource (resource-model/get-target-from-resource-id m)))

(defn- add-resource-id-from-new-resource
  [m]
  {:pre [(:new-resource m)]}
  (-> m
      (assoc :target-resource-id (:id (:new-resource m)))
      (dissoc :params)))

(defn- filter-engaged-and-initializing-resources
  [v]
  (filterv
   #(or (= (domain-language/get-resource-state %) :engaged)
        (= (domain-language/get-resource-state %) ":engaged")
        (= (domain-language/get-resource-state %) ":initializing")
        (= (domain-language/get-resource-state %) :initializing))
   v))

(defn- filter-to-be-removed-resources
  [v]
  (filterv
   #(or (= (domain-language/get-resource-state %) :to-be-removed)
        (= (domain-language/get-resource-state %) ":to-be-removed"))
   v))




(defn- release-all-resources!
  [m]
  {:pre [(:resources-relationships-map m) (:resources (:resources-relationships-map m))]}
  (->> m
       :resources-relationships-map
       :resources
       filter-engaged-and-initializing-resources
       (mapv #(release-resource! (assoc m :target-resource %)))))

(defn release-resources-of-an-informal-process-instance!
  [m]
  (->> m
       domain-language/get-resources
       filter-engaged-and-initializing-resources
       (mapv #(release-resource! (assoc m :target-resource %)))))


(defn release-to-be-removed-resources!
  [m]
  (->> m
       domain-language/get-resources
       filter-to-be-removed-resources
       (mapv #(release-resource! (assoc m :target-resource %)))))

(defn- initialization-failed!
  [m]
  {:pre [(:resource-model-init-fail-fn! m)]}
  (debug "Executing failure procedure!!!")
  (locking in-memory-resource-models-lock
    (remove-resource-model-from-cache-and-persistence! m)
    (release-all-resources! m)
    (add-in-memory-entity-to-persistence! (assoc-in m [:graph-data :entity-type] :failed-process-instant))
    ((:resource-model-init-fail-fn! m) m )));; store it for logging purposes


(dire/with-handler!
  #'start-initialization-or-proceed!
  "Problem with the initialization. Fail it!"
  java.lang.Exception
  (fn [e m]
    (error "Resource with the following spec could not be initialized" (:target-resource m))
    (error "Exception during resource acquirement:" e)
    (-> m
        add-nodes-relationships-map
        (assoc :instance-state-description "There was an exception during the acquirement of" (:name (:target-resource m)) "please contanct your technical admin.")
        initialization-failed!)))


(defn- initialize-post-conditioner-relationships!
  [m]
  (debug "TODO Postconditioner relationships will be initialized!!!"))


(defn proceed-resource-initalization!
  [m]
  {:pre [(:new-resource m) (:resources (:resources-relationships-map m))]}
  (debug "Proceeding with initialization!!")
  (if (resource-initialized? (:new-resource m))
    (do
      (initialize-post-conditioner-relationships! m)
      (if (resources-acquirement-complete? (:resources (:resources-relationships-map m)))
        (complete-resource-acquirement! m)
        (initialize-next-resource! m)))
    (initialization-failed! m)))



(defn- add-empty-entity-data
  [m]
  {:pre [(:process-id m)]}
  (assoc m :graph-data {:id (:process-id m)}))

(defn- add-updated-instance-descriptor
  [m]
  {:pre [(:instance-descriptor m)]}
  (assoc m :new-instance-descriptor (assoc (:instance-descriptor m)
                                           :instance-location (:instance-location m)
                                           :instance-state (:instance-state m))))


(defn- update-new-resource
  [m]
  {:pre [(:instance-descriptor m)]}
  (->> m
       :new-resource
       (assoc m :target-resource)
       domain-language/update-resource-instance-descriptor
       :target-resource
       (assoc m :new-resource)))

(defn- add-instance-descriptor-of-new-resource
  [m]
  {:pre [(:new-resource m)]}
  (assoc m :instance-descriptor (domain-language/get-resource-instance-descriptor (:new-resource m))))

(defn- prepare-new-resource
  [m]
  (->> m
       add-new-resource-from-parameters
       add-resource-instance-state
       add-resource-instance-location
       add-instance-descriptor-of-new-resource
       add-updated-instance-descriptor
       update-new-resource
       :new-resource
       (assoc m :new-resource)))

(defn- prepare-contents-for-proceeding-acquirement
  [m]
  (-> m
      add-process-id-from-params
      add-content-type
      prepare-new-resource
      add-process-state
      add-resource-id-from-new-resource
      add-empty-entity-data
      update-entity-type))

(defn- make-decision-for-execution!
  [m]
  (if (and (:graph-data m)
           (= :initializing-informal-process-instance (:entity-type (:graph-data m))))
    (doto
        (locking in-memory-resource-models-lock
          (debug "State will be changed hopefully only once:")
          (doto
              (-> m
                  add-resource-graph
                  add-target-resource-from-resource-id
                  resource-model/update-target-resource-in-graph
                  add-nodes-relationships-map
                  update-resource-model-of-informal-process-instance)
            persist-changes-in-cache-and-persistence!))
      proceed-resource-initalization!)
    (debug "No process is being acquired with the specfied id" (:process-id m))))


(defmulti handle-callback! :operation-type)

(defmethod handle-callback! (constants/property :acquire-resource-operation-name)
  [m]
  (-> m
      prepare-contents-for-proceeding-acquirement
      make-decision-for-execution!))


(defn- add-operation-response
  [m]
  (if (:operation-response (:params m))
    (assoc m :operation-message (:operation-response (:params m)))
    (error "No operation response included in the message")))

(defn- handle-resource-operation-callback!-fn
  [m]
  {:pre [(:persistence m)]}
  (fn [request]
    (debug "Change status request has been recieved!!!")
    (-> m
        (assoc :message request)
        add-request-params
        add-operation-response
        add-operation-type-from-message
        handle-callback!)))


(defn get-route-configs
  [m]
  [{:resource-path (get-resource-handler-path)
    :available-media-types ["application/xml" "application/edn"]
    :allowed-methods [:post]
    :post! (handle-resource-operation-callback!-fn m)}])
