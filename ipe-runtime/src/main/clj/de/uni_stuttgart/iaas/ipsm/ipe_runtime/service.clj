(ns de.uni-stuttgart.iaas.ipsm.ipe-runtime.service
  (:require [de.uni-stuttgart.iaas.ipsm.protocols.core :as protocols]
            [de.uni-stuttgart.iaas.ipsm.ipe-runtime.core :as core]
            [de.uni-stuttgart.iaas.ipsm.utils.communications :as cm]
            [schema.core :as s]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as dl]
            [puppetlabs.trapperkeeper.core :as trapperkeeper]
            [puppetlabs.trapperkeeper.services :as services]
            [de.uni-stuttgart.iaas.ipsm.ipe-runtime.resource-model :as rmi]
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                          logf tracef debugf infof warnf errorf fatalf reportf
                          spy logged-future with-log-level with-logging-config
                          sometimes)])
  (:import
   [java.net URI URL]
   [java.util.zip ZipEntry ZipOutputStream]
   [javax.jws WebService WebMethod]))


(def initialization-data-schema
  {(s/required-key :service-suffix) s/Str
   (s/required-key :service-name) s/Str
   (s/required-key :ws-port) s/Int
   (s/required-key :ws-host) s/Str
   (s/required-key :uri) s/Str
   (s/required-key :executor-wsdl-client-url) s/Str
   (s/optional-key :war-path) s/Str
   (s/optional-key :war-download-uri) s/Str
   (s/optional-key :war-handler-path) s/Str})


(trapperkeeper/defservice IPERuntime
  protocols/IPERuntime
  [[:ConfigService get-config]
   [:WebserverService add-war-handler add-ring-handler]
   PersistenceProtocol]
  (init [this context]
        (info "Initializing" (:service-name (:ipe-runtime (get-config))))
        (info "Initializing" (:ipe-runtime (get-config)))
        (-> (get-config)
            :ipe-runtime
            (assoc :persistence (services/get-service this :PersistenceProtocol))
            (update-in [:uri] #(URI/create %))
            core/add-rest-resource-routes
            :routes
            (add-ring-handler ""))
        context)
  (start [this context]
         (info "Validating initialization data" (:service-name (:ipe-runtime (get-config))))
         (info "Starting" (:service-name (:ipe-runtime (get-config))))
         (doto (as-> (into (:ipe-runtime (get-config)) context) loc
                 (assoc loc :persistence (services/get-service this :PersistenceProtocol))
                 (update-in loc [:executor-wsdl-client-url] #(URL. %))
                 (update-in loc [:uri] #(URI/create %)))
           core/init-ws-service!)
         context)
  (stop [this context]
        (info "Shutting down" (:ipe-runtime (get-config)))
        (core/stop-services (:ipe-runtime (get-config)))
        context)
  (set-service-initials [_ m] (swap! (:ipe-runtime (get-config)) into m))
  (init-informal-process-model! [_ process-model]
                                (as-> (:ipe-runtime (get-config)) loc
                                  (assoc-in loc [:entity-data] process-model)
                                  (assoc loc :persistence (services/get-service _ :PersistenceProtocol))
                                  (core/initilize-informal-process! loc)))
  (init-informal-process-model! [_ process-model instance-id]
                                (as-> (:ipe-runtime (get-config)) loc
                                  (assoc-in loc [:entity-data] process-model)
                                  (assoc loc :process-id instance-id)
                                  (assoc loc :persistence (services/get-service _ :PersistenceProtocol))
                                  (core/initilize-informal-process! loc)))
  (terminate-process-execution! [_ process-id]
                                (core/terminate-process-execution!
                                 (as-> (:ipe-runtime (get-config)) loc
                                   (into loc {:state-description "Successfully finished"
                                              :instance-state :completed})
                                   (assoc-in loc [:process-id] process-id)
                                   (assoc loc :persistence (services/get-service _ :PersistenceProtocol)))))
  (get-process-instance [_ process-id]
                        (as-> (:ipe-runtime (get-config)) loc
                          (assoc-in loc [:process-id] process-id)
                          (assoc loc :persistence (services/get-service _ :PersistenceProtocol))
                          (core/get-process-instance loc)))
  (update-process-instance! [_ process-instance]
                            (as-> (:ipe-runtime (get-config)) loc
                              (assoc-in loc [:entity-data] process-instance)
                              (assoc loc :persistence (services/get-service _ :PersistenceProtocol))
                              (core/update-informal-process! loc)))
  (list-processes [_]
                  (as-> (:ipe-runtime (get-config))loc
                    (assoc loc :persistence (services/get-service _ :PersistenceProtocol))
                    (core/get-all-process-instances loc)))
  (list-running-processes [_]
                          (as-> (:ipe-runtime (get-config)) loc
                            (assoc loc :persistence (services/get-service _ :PersistenceProtocol))
                            (core/get-all-running-process-instances loc)))
  (list-terminated-processes [_]
                             (as-> (:ipe-runtime (get-config)) loc
                               (assoc loc :persistence (services/get-service _ :PersistenceProtocol))
                               (core/get-all-terminated-process-instances loc))))

(defn get-ipe-runtime-service-instance [m] (core/->IPERuntimeServiceImpl m))
