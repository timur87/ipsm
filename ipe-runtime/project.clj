(defproject de.uni-stuttgart.iaas.ipsm/ipe-runtime "0.1.0-SNAPSHOT"
  :description "Runtime service for IPE models"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0-alpha13"]
                 [clj-http "2.0.1"]
                 [compojure "1.4.0"]
                 [puppetlabs/trapperkeeper-webserver-jetty9 "1.5.11"]
                 [com.taoensso/timbre "4.7.4"]
                 [puppetlabs/trapperkeeper "1.5.2"]
                 [com.novemberain/langohr "3.4.1"]
                 [environ "1.0.0"]
                 [dire "0.5.3"]
                 [uni-stuttgart.ipsm/ipe-executor-process-client "0.0.1-SNAPSHOT"]
                 [overtone/at-at "1.2.0"]
                 [de.uni-stuttgart.iaas.ipsm/persistence "0.0.1-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/protocols "0.5.3-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/commons "0.0.2-SNAPSHOT"]
                 [org.apache.cxf/cxf-rt-frontend-jaxws "3.1.4"]
                 [org.apache.cxf/cxf-rt-transports-http "3.1.4"]
                 [org.apache.cxf/cxf-rt-bindings-soap "3.1.4"]]
  :repl-options {:init-ns user}
  :aliases {"tk" ["trampoline" "run" "--config" "dev-resources/config.conf"]}
  :main puppetlabs.trapperkeeper.main
  :source-paths ["src/main/clj"]
  :plugins [[lein-environ "1.0.0"]]
  :test-paths ["src/test/clj"]
  :resource-paths ["src/main/resources" "src/main/webapp"]
  :uberjar-merge-with {"META-INF/cxf/bus-extensions.txt" [slurp #(str %1 "\n" %2) spit]})
