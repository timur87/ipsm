(ns de.uni-stuttgart.iaas.ipsm.ipe-compiler.core
  (:require [taoensso.timbre :as timbre]
            [clj-http.client :as client]
            [clojure.java.io :as io]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
            [de.uni-stuttgart.iaas.ipsm.utils.communications :as communications]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language])
  (:import [de.uni_stuttgart.iaas.ipsm.protocols.services IPECompilerService]
           [javax.xml.ws Endpoint]
           [java.net URI]
           [java.util.zip ZipEntry ZipOutputStream]
           [javax.jws WebService WebMethod]))


(timbre/refer-timbre)

(def protocol-prefix "http://")
(def temp-dir-path "temp/")
(def endpoint (atom {:ws nil :rest nil}))
(def handler-path "dipeas/")



(defprotocol DeploymentDescriptorProcessorProtocol
  (get-deployable-metadata [deployment-descriptor] "Returns a map with the deployment descriptor meta-data")
  (retrieve-deployable [deployment-descriptor target-output-stream] "Returns a map with the deployment descriptor meta-data"))

(deftype HTTPDeplomentDescriptorProcessor []
  DeploymentDescriptorProcessorProtocol
  (get-deployable-metadata [deployment-descriptor] "Returns a map with the deployment descriptor meta-data")
  (retrieve-deployable [deployment-descriptor target-output-stream] "Retrieves deployable using the outputstream"))



(defn create-deployables-vector
  [m]
  (let [model (get-in m [:entity-data :owl])]
    (assoc m :deployables
           (as-> (domain-language/get-maps-for-owl-str model :resource-definition :relationship-definition) loc
             (concat (:relationship-definition loc) (:resource-definition loc))))))


(defn- convert-to-domain-specific-rep
  "Accepts a map in the format of {:entity-data {:owl ipe-model}}. And
  creates the correspding domain specific EDN representation."
  [m]
  (-> m
      (assoc-in [:entity-data :entity-type] :informal-process)
      ;; Here we call first as the get-maps return a vector of maps
      (assoc-in [:entity-data] (first
                                (:informal-process
                                 (domain-language/get-maps-for-owl-str
                                 (get-in m [:entity-data :owl]) :informal-process))))))

(defn- create-deployables
  "Accepts a map in the format of {:entity-data {:entity-map
  ipe-model}}. And extracts information of deployables for
  relationships and resources."
  [m]
  (assoc m :deployables {:resources (:resources (:entity-map (:entity-data m)))
                         :relationships (:relationships (:entity-map (:entity-data m)))}))




(defn- fetch-deployables!
  "Accepts a map in the format of {:entity-data {:entity-map
  ipe-model}}. And fetches deployables for relationships and
  resources."
  [m]
  (debug "Deployables will be fetched from the map" m)
  (do
    (communications/download-deployables! (get-in m [:entity-data :entity-map :resources]) (:dipea-directory m))
    (communications/download-deployables! (get-in m [:entity-data :entity-map :relationships]) (:dipea-directory m)))
  nil)

(defn- create-root-folder!
  "Accepts a map in the format of {:entity-data {:entity-map
  ipe-model}}. Creates a root folder using the process id."
  [m]
  (let [dipea-folder (io/file (:dipea-directory m))]
    (if-not (.exists (io/file handler-path))
      (.mkdir (io/file handler-path)))
    (if (.exists dipea-folder)
      (.delete dipea-folder))
    (if (.mkdir dipea-folder)
      (debug "Directory for DIPEA has been created!")
      (error "Root dir could not be created on the path" (.getAbsolutePath dipea-folder))))
  nil)

(defn- remove-root-folder!
  "Accepts a map in the format of {:entity-data {:entity-map
  ipe-model}}. Deletes a root folder using the process id."
  [m]
  (let [dipea-folder (io/file (:dipea-directory m))]
    (if (.exists dipea-folder)
      (.delete dipea-folder)))
  nil)

(defn- archieve-all!
  "Accepts a map in the format of {:entity-data {:entity-map
  ipe-model} :root-folder}. And archives the root folder as
  name.dipea"
  [m]
  (debug "Map at during archieving is:" m)
  (with-open [zip (ZipOutputStream. (io/output-stream (:dipea-file m)))]
    (doseq [f (file-seq (io/file (:dipea-directory m))) :when (.isFile f)]
      (.putNextEntry zip (doto (ZipEntry. (.getName f))
                           (.setSize (.getTotalSpace f))))
      (io/copy f zip)
      (.closeEntry zip))
    (.putNextEntry zip (ZipEntry. "DIPEModel.xml"))
    (io/file (:dipea-directory m))
    (io/copy (domain-language/get-owl-xml-for-maps (:entity-data m)) zip)))

(defn- append-deployables
  "Appends each deployable in the deployables vector with file path
  and name."
  [deployables]
  (debug "Deployables are" deployables)
  (let [appended-deployables (mapv
                              #(do
                                 (debug "Deployable map is" %)
                                 (assoc-in % [:entity-map :deployment-descriptor :entity-map :deployable-path] (:id (:entity-map %))))
                              deployables)]
    (debug appended-deployables)
    appended-deployables))

(defn- append-type-of-deployables
  [m type]
  (if (get-in m [:entity-data :entity-map type])
      (update-in m
             [:entity-data :entity-map type]
             #(append-deployables %))
      m))

(defn debug-map
  [m]
  (debug "Map is" m)
  m)

(defn- append-ipe-model
  "Accepts a map in the format of {:entity-data {:entity-map
  ipe-model} :deployables []}. Updates deployables from the list."
  [m]
  (debug "Before appending models" m)
  (-> m
      (append-type-of-deployables :resources)
      (debug-map)
      (append-type-of-deployables :relationships)))

(defn- generate-dipea!
  [m]
  "Creates a DIPEA from the respective domain specific representation."
  (do (create-root-folder! m)
      (fetch-deployables! m)
      (archieve-all! m)))

(defn- get-dipea
  [m]
  (io/input-stream (io/file (:dipea-file m))))

(defn- add-dipea-directory
  "Adds :dipea-directory key val pair to the given map."
  [m]
  (assoc m :dipea-directory (str handler-path (:id (:entity-map (:entity-data m))) "/")))

(defn- add-dipea-file
  "Adds :dipea-directory key val pair to the given map."
  [m]
  (assoc m :dipea-file (str handler-path (constants/create-dipea-file-name-for-ipe-process (:entity-data m)))))

(defn compile-ipe
  [ipe-model]
  "Receives an IPE ontology and provisions it"
  (debug "Compliation has been started...")
  (doto (-> (assoc-in {} [:entity-data :owl] ipe-model)
            convert-to-domain-specific-rep
            add-dipea-directory
            add-dipea-file
            append-ipe-model)
    generate-dipea!))


(defn create-process-deployable-uri
  [m]
  (str protocol-prefix (constants/property :rest-host) ":" (constants/property :rest-port) "/" handler-path (get-in m [:entity-data :entity-map :id])))

(defn- append-ipe-with-deployment-data
  [m]
  {:pre [(get-in m [:entity-data :entity-map])]
   :post [(get-in % [:entity-data :entity-map :deployment-descriptor]) (:return-model %)]}
  (as-> m loc
      (assoc-in loc [:entity-data :entity-map :deployment-descriptor]
                (domain-language/deployment-descriptor
                 (create-process-deployable-uri m)
                 (:target-ee (constants/property :target-ee))
                 (constants/create-dipea-file-name-for-ipe-process (:entity-data m))))
      (assoc loc :return-model (domain-language/get-owl-xml-for-maps (:entity-data loc)))))

(defn- return-updated-ipe-model
  [m]
  (-> m
      append-ipe-with-deployment-data
      :return-model))

(deftype ^{WebService {:endpointInterface
                       "de.uni_stuttgart.iaas.ipsm.protocols.services.IPECompilerService"
                       }}
    IPECompilerServiceImpl []
    IPECompilerService
    (compileIPE [this ipe-model]
      (-> (compile-ipe ipe-model)
          (return-updated-ipe-model))))

(defn- rest-handler-config
  []
  {:resource-config {:route-configs
                     [{:resource-path (str "/" handler-path ":id")
                       :allowed-methods [:get]
                       :available-media-types [constants/dipea-media-type
                                               "text/html"]
                       :handle-ok (communications/default-id-based-file-handler
                                    {:file-path handler-path
                                     :media-type constants/dipea-media-type
                                     :file-suffix constants/dipea-suffix})}]}})

(defn- init-rest-service
  [args &]
  {:pre [(constants/property :rest-port)
         (constants/property :rest-host)]}
  (when-not (nil? (:rest @endpoint))
     (.stop (:rest @endpoint)))
  (swap! endpoint assoc
         :rest (as-> (rest-handler-config) loc
                 (assoc loc :host (constants/property :rest-host))
                 (assoc loc :port (Integer. (re-find #"\d+" (constants/property :rest-port))))
                 (communications/add-routes loc)
                 (communications/run-server-detached loc))))


(defn- init-ws-service
  [args &]
  {:pre [(constants/property :ws-host)
         (constants/property :ws-port)]}
  (when-not (nil? (:ws @endpoint))
    (.stop (:ws @endpoint)))
  (swap! endpoint assoc
         :ws (Endpoint/publish (str protocol-prefix
                                    (constants/property :ws-host)
                                    ":"
                                    (constants/property :ws-port)
                                    "/compiler-service") (IPECompilerServiceImpl.))))




(defn init-service
  [args &]
  (do
    (init-ws-service args)
    (init-rest-service args)))

;; (init-rest-service nil)
;; (print (-> (compile-ipe (slurp "resources/docker-based-owl-exchange.xml"))
;;            return-updated-ipe-model))
