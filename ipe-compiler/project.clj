(defproject de.uni-stuttgart.iaas.ipsm/ipe-compiler "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [com.taoensso/timbre "4.7.4"]
                 [clj-http "2.0.1"]
                 [de.uni-stuttgart.iaas.ipsm/protocols "0.5.3-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/commons "0.0.2-SNAPSHOT"]
                 [org.apache.cxf/cxf-rt-frontend-jaxws "3.1.0"]
                 [org.apache.cxf/cxf-rt-transports-http "3.1.0"]
                 [org.apache.cxf/cxf-rt-transports-http-jetty "3.1.0"]]
  :source-paths ["src/main/clj"]
  :java-source-paths ["src/main/java"]
  :resource-paths ["src/main/resources"])
