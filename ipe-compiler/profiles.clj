{:dev {:env {:ws-host "localhost"
             :rest-host "localhost"
             :ws-port "8010"
             :rest-port "8011"}
       :dependencies [[org.clojure/tools.nrepl "0.2.7"]]
       :plugins [[lein-environ "1.0.0"]
                 [cider/cider-nrepl "0.10.0-SNAPSHOT"]]}}
